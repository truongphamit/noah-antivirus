# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\androidsdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

##loại bỏ các tập tin xử lý thừa
#-dontpreverify
##Chỉ định để đóng gói lại tất cả các tập tin lớp được đổi tên,
#-repackageclasses ''
##Chỉ định truy cập của các lớp và các thành viên lớp có thể được mở rộng trong khi biên dịch
#-allowaccessmodification
##Chỉ định tối ưu hóa
#-optimizations !code/simplification/arithmetic
#-keepattributes *Annotation*
##Giữ lại class
#-keep public class * extends android.app.Activity
#-keep public class * extends android.app.Application
#-keep public class * extends android.app.Service
#-keep public class * extends android.content.BroadcastReceiver
#-keep public class * extends android.content.ContentProvider
#-keep public class * extends android.content.ContentProvider
#-keep public class * extends android.view.View {
#    public <init>(android.content.Context);
#    public <init>(android.content.Context, android.util.AttributeSet);
#    public <init>(android.content.Context, android.util.AttributeSet, int);
#    public void set*(...);
#}
##Các class được bảo mật
#-keepclasseswithmembers class * {
#    public <init>(android.content.Context, android.util.AttributeSet);
#}
#-keepclasseswithmembers class * {
#    public <init>(android.content.Context, android.util.AttributeSet, int);
#}
#-keepclassmembers class * extends android.content.Context {
#   public void *(android.view.View);
#   public void *(android.view.MenuItem);
#}
#-keepclassmembers class * implements android.os.Parcelable {
#    static ** CREATOR;
#}
#-keepclassmembers class **.R$* {
#    public static <fields>;
#}
#-keepclassmembers class * {
#    @android.webkit.JavascriptInterface <methods>;
#}

-dontwarn org.joda.time.**
-dontwarn retrofit2.**
-dontwarn okio.**

#To remove debug logs:
-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** v(...);
    public static *** w(...);
}

#
##To avoid changing names of methods invoked on layout's onClick.
## Uncomment and add specific method names if using onClick on layouts
##-keepclassmembers class * {
## public void onClickButton(android.view.View);
##}

#Maintain java native methods
-keepclasseswithmembers class * {
    native <methods>;
}

##To maintain custom components names that are used on layouts XML:
-keep public class * extends android.view.View {
      public <init>(android.content.Context);
      public <init>(android.content.Context, android.util.AttributeSet);
      public <init>(android.content.Context, android.util.AttributeSet, int);
      public void set*(...);
}

#
#Keep the R
-keepclassmembers class **.R$* {
    public static <fields>;
}


####### ADDITIONAL OPTIONS NOT USED NORMALLY
#
##To keep callback calls. Uncomment if using any
##http://proguard.sourceforge.net/index.html#/manual/examples.html#callback
##-keep class mypackage.MyCallbackClass {
##   void myCallbackMethod(java.lang.String);
##}
#
##Uncomment if using Serializable
#-keepclassmembers class * implements java.io.Serializable {
#    private static final java.io.ObjectStreamField[] serialPersistentFields;
#    private void writeObject(java.io.ObjectOutputStream);
#    private void readObject(java.io.ObjectInputStream);
#    java.lang.Object writeReplace();
#    java.lang.Object readResolve();
#}

# eventbus
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# IPackageDataObserver
-keep class android.content.pm.** { *; }
-keep interface android.content.pm.** { *; }


-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService

#keep all classes that might be used in XML layouts
-keep public class * extends android.view.View
-keep public class * extends android.app.Fragment
-keep public class * extends android.support.v4.Fragment

# support-v4
-dontwarn android.support.v4.**
-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }

# support-v7
-dontwarn android.support.v7.**
-keep class android.support.v7.internal.** { *; }
-keep interface android.support.v7.internal.** { *; }

# appcombat
-keep public class * extends android.support.v7.app.ActionBarActivity { *; }

#keep all public and protected methods that could be used by java reflection
-keepclassmembernames class * {
    public protected <methods>;
}

-keepclassmembers class * extends android.content.Context {
    public void *(android.view.View);
    public void *(android.view.MenuItem);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
	public static final android.os.Parcelable$Creator *;
}

# fb
-keep class com.facebook.** { *; }

# google play service
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keep public class com.google.android.gms.* { public *; }
-dontwarn com.google.android.gms.**
