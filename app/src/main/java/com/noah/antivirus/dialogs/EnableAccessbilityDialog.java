package com.noah.antivirus.dialogs;

import android.app.Dialog;
import android.app.usage.UsageEvents;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.util.AllowAccessibilityReceiverEvent;
import com.noah.antivirus.util.TypeFaceUttils;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by truongpq on 18/11/2016.
 */

public class EnableAccessbilityDialog extends BottomSheetDialog {
    private Context context;
    private CallBack callBack;

    public void setCallBack(CallBack callBack) {
        this.callBack = callBack;
    }

    public interface CallBack {
        void execute();
    }

    public EnableAccessbilityDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accessibility_dialog);
        getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        TextView tv_title = (TextView) findViewById(R.id.tv_title);
        TextView tv_decription = (TextView) findViewById(R.id.tv_decription);
        TextView btn_not_now = (TextView) findViewById(R.id.btn_not_now);
        TextView btn_enable = (TextView) findViewById(R.id.btn_enable);
        TypeFaceUttils.setBold(context, tv_title);
        TypeFaceUttils.setNomal(context, tv_decription);
        TypeFaceUttils.setNomal(context, btn_not_now);
        TypeFaceUttils.setNomal(context, btn_enable);
        btn_enable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();

                EventBus.getDefault().post(new AllowAccessibilityReceiverEvent(true));

                context.startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                Dialog dialog = new Dialog(context);
                dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_guide_accessibility);
                dialog.show();
            }
        });

        btn_not_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (callBack != null) callBack.execute();
            }
        });
    }
}
