package com.noah.antivirus.fragment;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.noah.antivirus.activities.ResultActivity;
import com.noah.antivirus.activities.ScanningResultActivity;
import com.noah.antivirus.adapter.WarningAdapter;
import com.noah.antivirus.iface.IProblem;
import com.noah.antivirus.model.AppProblem;
import com.noah.antivirus.model.MenacesCacheSet;
import com.noah.antivirus.model.SystemProblem;
import com.noah.antivirus.model.UserWhiteList;
import com.noah.antivirus.util.ProblemsDataSetTools;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.util.Utils;
import com.noah.antivirus.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResloveProblemDetailsFragment extends Fragment {
    @BindView(R.id.ll_layout_for_app)
    LinearLayout ll_layout_for_app;

    @BindView(R.id.ll_layout_for_system)
    LinearLayout ll_layout_for_system;

    @BindView(R.id.bt_ignore_setting)
    ImageView bt_ignore_setting;

    @BindView(R.id.bt_open_setting)
    ImageView bt_open_setting;

    @BindView(R.id.bt_trust_app)
    ImageView bt_trust_app;

    @BindView(R.id.bt_uninstall_app)
    ImageView bt_uninstall_app;

    @BindView(R.id.iv_icon_app)
    ImageView iv_icon_app;

    @BindView(R.id.tv_app_name)
    TextView tv_app_name;

    @BindView(R.id.rv_warning_problem)
    RecyclerView rv_warning_problem;

    private ResultActivity activity;
    IProblem _problem = null;
    private boolean _uninstallingPackage = false;

    private void customFont() {
        TypeFaceUttils.setNomal(getActivity(), tv_app_name);
    }

    public ResloveProblemDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reslove_problem_details, container, false);
        ButterKnife.bind(this, view);
        customFont();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (ResultActivity) getActivity();
        if (activity.getMonitorShieldService() == null) {
            getActivity().getSupportFragmentManager().popBackStack();
            return;
        }

        _problem = activity.getComu();
        rv_warning_problem.setAdapter(new WarningAdapter(getActivity(), _problem));
        rv_warning_problem.setLayoutManager(new LinearLayoutManager(getActivity()));

        init();
    }

    @Override
    public void onResume() {
        super.onResume();
        initForResume();
    }

    private void init() {
        if (_problem.getType() == IProblem.ProblemType.AppProblem) {
            ll_layout_for_app.setVisibility(View.VISIBLE);
            ll_layout_for_system.setVisibility(View.GONE);

            final AppProblem appProblem = (AppProblem) _problem;


            bt_uninstall_app.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    _uninstallingPackage = true;
                    Uri uri = Uri.fromParts("package", appProblem.getPackageName(), null);
                    Intent it = new Intent(Intent.ACTION_DELETE, uri);
                    startActivity(it);
                }
            });

            bt_trust_app.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle)
                            .setTitle(getString(R.string.warning))
                            .setMessage(getString(R.string.dialog_trust_app))
                            .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    UserWhiteList userWhiteList = activity.getMonitorShieldService().getUserWhiteList();
                                    userWhiteList.addItem(appProblem);
                                    userWhiteList.writeToJSON();

                                    MenacesCacheSet menacesCacheSet = activity.getMonitorShieldService().getMenacesCacheSet();
                                    menacesCacheSet.removeItem(appProblem);
                                    menacesCacheSet.writeToJSON();
                                    bt_trust_app.setEnabled(false);

                                    sendResult(_problem);
                                }
                            }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();

                }
            });

            Drawable s = Utils.getIconFromPackage(appProblem.getPackageName(), getActivity());
            getActivity().setTitle(Utils.getAppNameFromPackage(getActivity(), appProblem.getPackageName()));
            iv_icon_app.setImageDrawable(s);

            tv_app_name.setText(Utils.getAppNameFromPackage(getActivity(), appProblem.getPackageName()));
        } else {
            ll_layout_for_app.setVisibility(View.GONE);
            ll_layout_for_system.setVisibility(View.VISIBLE);

            final SystemProblem systemProblem = (SystemProblem) _problem;

            Drawable s = systemProblem.getIcon(getActivity());
            iv_icon_app.setImageDrawable(s);
//            getActivity().setTitle(systemProblem.getTitle(getActivity()));
            tv_app_name.setText(systemProblem.getTitle(getActivity()));

            bt_open_setting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    systemProblem.doAction(getActivity());

                }
            });


            bt_ignore_setting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserWhiteList _userWhiteList = activity.getMonitorShieldService().getUserWhiteList();
                    _userWhiteList.addItem(_problem);
                    _userWhiteList.writeToJSON();

                    MenacesCacheSet menaceCacheSet = activity.getMonitorShieldService().getMenacesCacheSet();
                    menaceCacheSet.removeItem(_problem);
                    menaceCacheSet.writeToJSON();

                    sendResult(_problem);
                }
            });
        }
    }

    private void initForResume() {
        //Returned from an uninstallation
        if (_uninstallingPackage) {
            if (_problem != null) {
                final AppProblem appProblem = (AppProblem) _problem;

                if (!Utils.isPackageInstalled(getActivity(), appProblem.getPackageName())) {
                    MenacesCacheSet menacesCacheSet = activity.getMonitorShieldService().getMenacesCacheSet();
                    menacesCacheSet.removeItem(appProblem);
                    menacesCacheSet.writeToJSON();

                    sendResult(_problem);

                    _uninstallingPackage = false;
                }
            }
        } else if (_problem.getType() == IProblem.ProblemType.AppProblem) {
            //User could have deleted app from file sytem while in this screen.
            //Check if it exists. If not update menacesCacheSet
            MenacesCacheSet menacesCacheSet = activity.getMonitorShieldService().getMenacesCacheSet();
            final AppProblem appProblem = (AppProblem) _problem;
            if (!ProblemsDataSetTools.checkIfPackageInCollection(appProblem.getPackageName(), menacesCacheSet.getSet())) {
                //It is in menaces cacheset. Check if it is really in the system
                if (!Utils.isPackageInstalled(getActivity(), appProblem.getPackageName())) {
                    //If it isn't remove it
                    menacesCacheSet = activity.getMonitorShieldService().getMenacesCacheSet();
                    menacesCacheSet.removeItem(appProblem);
                    menacesCacheSet.writeToJSON();

                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        } else if (_problem.getType() == IProblem.ProblemType.SystemProblem) {
            MenacesCacheSet menacesCacheSet;
            final SystemProblem systemProblem = (SystemProblem) _problem;
            if (!systemProblem.problemExists(getActivity())) {
                //If it isn't remove it
                menacesCacheSet = activity.getMonitorShieldService().getMenacesCacheSet();
                menacesCacheSet.removeItem(systemProblem);
                menacesCacheSet.writeToJSON();

                sendResult(_problem);
            }
        }
    }

    private void sendResult(IProblem iProblem) {
        activity.refresh(iProblem);
        getActivity().getSupportFragmentManager().popBackStack();
    }
}
