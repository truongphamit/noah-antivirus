package com.noah.antivirus.fragment;


import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.activities.AppLockCreatePasswordActivity;
import com.noah.antivirus.activities.ResultAppLockCreatePasswordActivity;
import com.noah.antivirus.adapter.ResultAppLockApdater;
import com.noah.antivirus.base.BaseNavigationDrawerActivity;
import com.noah.antivirus.model.AppsLocked;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.util.Utils;
import com.noah.antivirus.activities.ScanningResultActivity;
import com.noah.antivirus.model.AppLock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResultAppLockFragment extends Fragment {
    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_decription)
    TextView tv_decription;

    @BindView(R.id.rv_app_lock)
    RecyclerView rv_app_lock;

    @BindView(R.id.tv_protect)
    TextView tv_protect;

    private ScanningResultActivity activity;
    private ResultAppLockApdater adapter;
    private List<AppLock> appLocks;
    private int protectApps;
    private AppsLocked appsLocked;

    private void customFont() {
        TypeFaceUttils.setNomal(getActivity(), tv_title);
        TypeFaceUttils.setNomal(getActivity(), tv_decription);
        TypeFaceUttils.setNomal(getActivity(), tv_protect);
    }


    public ResultAppLockFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result_app_lock, container, false);
        ButterKnife.bind(this, view);
        customFont();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (ScanningResultActivity) getActivity();
        activity.setBackground(R.drawable.settings_background);
        if (activity.getMonitorShieldService() == null) {
            getActivity().getSupportFragmentManager().popBackStack();
            activity.setBackground(R.drawable.background_danger);
            return;
        }

        appsLocked = new AppsLocked(getActivity());
        initView();
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.registerReceiver(allowAccessibilityService, new IntentFilter(BaseNavigationDrawerActivity.BROADCAST_ALLOW_ACCESSIBILITY));
    }

    @Override
    public void onStop() {
        super.onStop();
        activity.unregisterReceiver(allowAccessibilityService);
    }

    private void initView() {
        for (AppLock appLock : activity.getMonitorShieldService().getAppLock()) {
            if (appLock.isRecommend()) {
                ++protectApps;
                appLock.setLock(true);
            }
        }
        String number = "<font color='#cc0000'>" + protectApps + "</font>";
        String text = "<font color='#0066cc'> " + getResources().getString(R.string.apps_with_privacy_issues) + "</font>";
        tv_title.setText(Html.fromHtml(number + text));

        appLocks = activity.getMonitorShieldService().getAppLock();
        Collections.sort(appLocks);

        rv_app_lock.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_app_lock.setHasFixedSize(true);
        adapter = new ResultAppLockApdater(getActivity(), appLocks);
        rv_app_lock.setAdapter(adapter);
        updateProtectApps();

        adapter.setOnItemClickListener(new ResultAppLockApdater.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                CheckBox checkBox = (CheckBox) itemView;
                if (!checkBox.isChecked()) {
                    checkBox.setChecked(false);
                    appLocks.get(position).setLock(false);
                    --protectApps;
                } else {
                    checkBox.setChecked(true);
                    appLocks.get(position).setLock(true);
                    ++protectApps;
                }
                updateProtectApps();
            }
        });

        tv_protect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity().getSharedPreferences(AppLockCreatePasswordActivity.SHARED_PREFERENCES_NAME, 0).getString(AppLockCreatePasswordActivity.KEY_PASSWORD, null) != null) {
                    List<AppLock> remove = new ArrayList<>();
                    for (AppLock appLock : appLocks) {
                        if (appLock.isLock()) {
                            appsLocked.add(appLock);
                            remove.add(appLock);
                        }
                    }
                    appLocks.removeAll(remove);
                    getActivity().getSupportFragmentManager().popBackStack();
                    activity.setBackground(R.drawable.background_danger);
                    activity.refresh();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                        if (!Utils.isUsageAccessEnabled(getActivity())) {
                            Utils.openUsageAccessSetings(getActivity());
                            return;
                        }
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.canDrawOverlays(getActivity())) {
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getActivity().getPackageName()));
                            startActivity(intent);
                            return;
                        }
                    }

                    if (!Utils.isAccessibilitySettingsOn(getActivity())) {
                        startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));

                        Dialog dialog = new Dialog(getActivity());
                        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog.setContentView(R.layout.dialog_guide_accessibility);
                        dialog.show();
                        return;
                    }

                    // Create Password
                    startActivity(new Intent(getActivity(), ResultAppLockCreatePasswordActivity.class));
                }
            }
        });
    }

    private void updateProtectApps() {
        if (protectApps != 0) {
            tv_protect.setVisibility(View.VISIBLE);
            tv_protect.setText(getResources().getString(R.string.protect) + "(" + protectApps + ")");
        } else {
            tv_protect.setVisibility(View.GONE);
        }
    }

    public BroadcastReceiver allowAccessibilityService = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.getBooleanExtra(BaseNavigationDrawerActivity.SHOULD_CREATE_PASS, false)) {
                    startActivity(new Intent(activity, ResultAppLockCreatePasswordActivity.class));
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }
    };
}
