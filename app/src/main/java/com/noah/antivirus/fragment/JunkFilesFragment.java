package com.noah.antivirus.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.adapter.JunkFilesAdapter;
import com.noah.antivirus.util.Utils;
import com.noah.antivirus.activities.ScanningResultActivity;
import com.noah.antivirus.model.JunkOfApplication;
import com.noah.antivirus.util.TypeFaceUttils;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class JunkFilesFragment extends Fragment {
    @BindView(R.id.tv_junk_files_total)
    TextView tv_junk_files_total;

    @BindView(R.id.tv_total_found)
    TextView tv_total_found;

    @BindView(R.id.tv_mb)
    TextView tv_mb;

    @BindView(R.id.tv_suggested)
    TextView tv_suggested;

    @BindView(R.id.tv_title_total_found)
    TextView tv_title_total_found;

    @BindView(R.id.tv_total_mb)
    TextView tv_total_mb;

    @BindView(R.id.tv_boost)
    TextView tv_boost;

    @BindView(R.id.framelayout_boost)
    View framelayout_boost;

    @BindView(R.id.rv_cache)
    RecyclerView rv_cache;

    private ScanningResultActivity activity;
    private long junkFilesSize;
    private JunkFilesAdapter adapter;

    private void customFont() {
        TypeFaceUttils.setNomal(getActivity(), tv_junk_files_total);
        TypeFaceUttils.setNomal(getActivity(), tv_mb);
        TypeFaceUttils.setNomal(getActivity(), tv_suggested);
        TypeFaceUttils.setNomal(getActivity(), tv_title_total_found);
        TypeFaceUttils.setNomal(getActivity(), tv_total_found);
        TypeFaceUttils.setNomal(getActivity(), tv_total_mb);
        TypeFaceUttils.setNomal(getActivity(), tv_boost);
    }


    public JunkFilesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_junk_files, container, false);
        ButterKnife.bind(this, view);
        customFont();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (ScanningResultActivity) getActivity();
        if (activity.getMonitorShieldService() == null) {
            getActivity().getSupportFragmentManager().popBackStack();
            return;
        }

        initView();
    }

    private void initView() {
        for (JunkOfApplication junkOfApplication : Utils.junkOfApplications) {
            junkFilesSize += junkOfApplication.getCacheSize();
        }

        junkFilesSize = junkFilesSize / (1024 * 1024);
        tv_junk_files_total.setText(String.valueOf(junkFilesSize));
        tv_total_found.setText(String.valueOf(junkFilesSize));

        rv_cache.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_cache.setHasFixedSize(true);

        adapter = new JunkFilesAdapter(getActivity(), Utils.junkOfApplications);
        rv_cache.setAdapter(adapter);

        framelayout_boost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.clearCache(getActivity());
                getActivity().getSupportFragmentManager().popBackStack();
                activity.refresh();
            }
        });
    }
}
