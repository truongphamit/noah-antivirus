package com.noah.antivirus.fragment;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.adapter.ResultAdapter;
import com.noah.antivirus.iface.Communicator;
import com.noah.antivirus.iface.IProblem;
import com.noah.antivirus.iface.IResultItemSelectedListener;
import com.noah.antivirus.model.AppProblem;
import com.noah.antivirus.model.Application;
import com.noah.antivirus.model.MenacesCacheSet;
import com.noah.antivirus.model.SystemProblem;
import com.noah.antivirus.model.UserWhiteList;
import com.noah.antivirus.activities.ScanningResultActivity;
import com.noah.antivirus.util.TypeFaceUttils;

import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ApplicationFragment extends Fragment {

    @BindView(R.id.rv_scan_result)
    RecyclerView rv_scan_result;

    ResultAdapter adapter;

    @BindView(R.id.tv_num_of_issues)
    TextView tv_num_of_issues;

    @BindView(R.id.framelayout_skip_all)
    View framelayout_skip_all;

    @BindView(R.id.tv_skip_all)
    TextView tv_skip_all;

    private ScanningResultActivity activity;

    private void customFont() {
        TypeFaceUttils.setNomal(getActivity(), tv_num_of_issues);
        TypeFaceUttils.setNomal(getActivity(), tv_skip_all);
    }

    public ApplicationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_application, container, false);
        ButterKnife.bind(this, view);
        customFont();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (ScanningResultActivity) getActivity();
        if (activity.getMonitorShieldService() == null) {
            getActivity().getSupportFragmentManager().popBackStack();
            return;
        }

        tv_num_of_issues.setText(getResources().getString(R.string.found) + " " + activity.getMonitorShieldService().getMenacesCacheSet().getItemCount() + " " + getResources().getString(R.string.issues));

        adapter = new ResultAdapter(getActivity(), new ArrayList<>(activity.getMonitorShieldService().getMenacesCacheSet().getSet()));
        rv_scan_result.setAdapter(adapter);
        rv_scan_result.setLayoutManager(new StickyHeaderLayoutManager());


        adapter.setResultItemSelectedStateChangedListener(new IResultItemSelectedListener() {
            @Override
            public void onItemSelected(IProblem bpdw, ImageView iv_icon_app, Context c) {
                activity.setComu(bpdw);
                activity.slideInFragment(ScanningResultActivity.PROBLEM_DETAIL_FRAGMENT_TAG);
//                activity.shareElementFragment(ApplicationFragment.this, iv_icon_app);
            }
        });

        framelayout_skip_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setApp_skip_all(true);
                getActivity().getSupportFragmentManager().popBackStack();
                activity.refresh();
            }
        });
    }

    public void deleteItem(final IProblem iProblem, final int type) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MenacesCacheSet menacesCacheSet = activity.getMonitorShieldService().getMenacesCacheSet();
                switch (type) {
                    case Communicator.TYPE_TRUST_APP:
                        AppProblem appProblem = (AppProblem) iProblem;
                        UserWhiteList userWhiteList = activity.getMonitorShieldService().getUserWhiteList();
                        userWhiteList.addItem(appProblem);
                        userWhiteList.writeToJSON();

                        menacesCacheSet.removeItem(appProblem);
                        menacesCacheSet.writeToJSON();
                        adapter.remove(iProblem);
                        break;
                    case Communicator.TYPE_IGNORE_SETTING:
                        UserWhiteList _userWhiteList = activity.getMonitorShieldService().getUserWhiteList();
                        _userWhiteList.addItem(iProblem);
                        _userWhiteList.writeToJSON();

                        MenacesCacheSet menaceCacheSet = activity.getMonitorShieldService().getMenacesCacheSet();
                        menaceCacheSet.removeItem(iProblem);
                        menaceCacheSet.writeToJSON();
                        adapter.remove(iProblem);
                        break;
                    case Communicator.TYPE_UNINSTALL_APP:
                        AppProblem mAppProblem = (AppProblem) iProblem;
                        menacesCacheSet.removeItem(mAppProblem);
                        menacesCacheSet.writeToJSON();

                        Application application = new Application();
                        application.setPackageName(mAppProblem.getPackageName());
                        activity.getMonitorShieldService().getRunningApplications().remove(application);
                        break;
                    case Communicator.TYPE_REMOVE_SETTING:
                        SystemProblem systemProblem = (SystemProblem) iProblem;
                        menacesCacheSet.removeItem(systemProblem);
                        menacesCacheSet.writeToJSON();
                        adapter.remove(iProblem);
                        break;
                }

                tv_num_of_issues.setText(getResources().getString(R.string.found) + " " + activity.getMonitorShieldService().getMenacesCacheSet().getItemCount() + " " + getResources().getString(R.string.issues));
                if (activity.getMonitorShieldService().getMenacesCacheSet().getItemCount() == 0) {
                    getActivity().getSupportFragmentManager().popBackStack();
                    activity.refresh();
                }
            }
        }, 300);
    }
}
