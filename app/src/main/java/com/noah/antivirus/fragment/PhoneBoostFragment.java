package com.noah.antivirus.fragment;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.adapter.ApplicationsAdapter;
import com.noah.antivirus.dialogs.EnableAccessbilityDialog;
import com.noah.antivirus.model.Application;
import com.noah.antivirus.service.BoosterService;
import com.noah.antivirus.util.AllowAccessibilityReceiverEvent;
import com.noah.antivirus.util.Utils;
import com.noah.antivirus.activities.ScanningResultActivity;
import com.noah.antivirus.util.TypeFaceUttils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.gmariotti.recyclerview.itemanimator.SlideInOutRightItemAnimator;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhoneBoostFragment extends Fragment implements View.OnClickListener {
    public static String TAG = "PhoneBoostFragment";

    public static final String BROADCAST_ALLOW_ACCESSIBILITY = "BROADCAST_ALLOW_ACCESSIBILITY";
    public static final String SHOULD_BOOST = "SHOULD_BOOST";

    private List<Application> removeApps;

    @BindView(R.id.rv_application)
    RecyclerView rv_application;

    @BindView(R.id.tv_count_running_app)
    TextView tv_count_running_app;

    @BindView(R.id.tv_memory_boost)
    TextView tv_memory_boost;

    @BindView(R.id.framelayout_boost)
    View framelayout_boost;

    @BindView(R.id.tv_boost)
    TextView tv_boost;

    @BindView(R.id.tv_mb)
    TextView tv_mb;

    @BindView(R.id.tv_freeable)
    TextView tv_freeable;

    private ApplicationsAdapter adapter;
    private ScanningResultActivity activity;

    private int totalBoost;

    private void customFont() {
        TypeFaceUttils.setNomal(getActivity(), tv_memory_boost);
        TypeFaceUttils.setNomal(getActivity(), tv_mb);
        TypeFaceUttils.setNomal(getActivity(), tv_freeable);
        TypeFaceUttils.setNomal(getActivity(), tv_count_running_app);
        TypeFaceUttils.setNomal(getActivity(), tv_boost);
    }

    boolean bound = false;
    private BoosterService boosterService;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            bound = true;
            BoosterService.BoosterBinder binder = (BoosterService.BoosterBinder) service;
            boosterService = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            boosterService = null;
            bound = false;
        }
    };

    public PhoneBoostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().startService(new Intent(getActivity(), BoosterService.class));
        getActivity().bindService(new Intent(getActivity(), BoosterService.class), serviceConnection, Context.BIND_AUTO_CREATE);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_phone_boost, container, false);
        ButterKnife.bind(this, view);
        customFont();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (ScanningResultActivity) getActivity();
        if (activity.getMonitorShieldService() == null) {
            getActivity().getSupportFragmentManager().popBackStack();
            return;
        }

        initView();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
        if (bound && boosterService != null) {
            getActivity().unbindService(serviceConnection);
            bound = false;
        }
    }

    @Override
    public void onDestroy() {
        try {
            getContext().unregisterReceiver(allowAccessibilityService);
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAllowAccessibilityServiceEvent(AllowAccessibilityReceiverEvent event) {
        if (event.isRegister()) {
            getContext().registerReceiver(allowAccessibilityService, new IntentFilter(BROADCAST_ALLOW_ACCESSIBILITY));
        }
    }

    /**
     * event when user allow accessibility service and auto boost device
     */
    public BroadcastReceiver allowAccessibilityService = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                boosterService.addViewInWindowManager();

                if (intent.getBooleanExtra(SHOULD_BOOST, false)) {
                    activity.getMonitorShieldService().getRunningApplications().clear();
                    getActivity().getSupportFragmentManager().popBackStack();
                    activity.refresh();
                    activity.setShowAd(false);
                    boosterService.boost(removeApps);
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }
    };

    private void initView() {
        int total = 0;
        for (Application application : activity.getMonitorShieldService().getRunningApplications()) {
            total += application.getSize() / 1024;
        }

        tv_memory_boost.setText(String.valueOf(total));
        tv_count_running_app.setText(activity.getMonitorShieldService().getRunningApplications().size() + " " + getResources().getString(R.string.apps_running));
        rv_application.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_application.setHasFixedSize(true);
        adapter = new ApplicationsAdapter(getActivity(), activity.getMonitorShieldService().getRunningApplications());
        rv_application.setAdapter(adapter);
        rv_application.setItemAnimator(new SlideInOutRightItemAnimator(rv_application));

        for (Application application : activity.getMonitorShieldService().getRunningApplications()) {
            if (application.isChoose()) totalBoost += application.getSize() / 1024;
        }

        updateBoostView();
        adapter.setOnItemClickListener(new ApplicationsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                CheckBox checkBox = (CheckBox) itemView;
                if (!checkBox.isChecked()) {
                    checkBox.setChecked(false);

                    SharedPreferences sharedPreferences = getContext().getSharedPreferences(Utils.SHARE_PREFERENCE_APP_CHOSEN_ADVICE, Context.MODE_PRIVATE);
                    if (!sharedPreferences.contains(activity.getMonitorShieldService().getRunningApplications().get(position).getPackageName())) {
                        sharedPreferences.edit().putInt(activity.getMonitorShieldService().getRunningApplications().get(position).getPackageName(), 1).commit();

                    } else {
                        if (sharedPreferences.getInt(activity.getMonitorShieldService().getRunningApplications().get(position).getPackageName(), 0) < 4) {
                            int timeUncheck = sharedPreferences.getInt(activity.getMonitorShieldService().getRunningApplications().get(position).getPackageName(), 0);
                            timeUncheck++;
                            Log.d(TAG, "add app advice, " + activity.getMonitorShieldService().getRunningApplications().get(position).getPackageName() + " " + timeUncheck);
                            sharedPreferences.edit().putInt(activity.getMonitorShieldService().getRunningApplications().get(position).getPackageName(), timeUncheck).commit();
                        }
                    }

                    activity.getMonitorShieldService().getRunningApplications().get(position).setChoose(false);
                    totalBoost -= activity.getMonitorShieldService().getRunningApplications().get(position).getSize() / 1024;
                } else {
                    checkBox.setChecked(true);
                    activity.getMonitorShieldService().getRunningApplications().get(position).setChoose(true);
                    totalBoost += activity.getMonitorShieldService().getRunningApplications().get(position).getSize() / 1024;
                }
                updateBoostView();
            }
        });

        framelayout_boost.setOnClickListener(this);
    }

    private void updateBoostView() {
        if (totalBoost != 0) {
            tv_boost.setText(getResources().getString(R.string.boost) + " " + totalBoost + "MB");
            framelayout_boost.setVisibility(View.VISIBLE);
        } else {
            tv_boost.setText(getResources().getString(R.string.boost) + " " + totalBoost + "MB");
            framelayout_boost.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.framelayout_boost) {
            removeApps = new ArrayList<>();
            for (Application application : activity.getMonitorShieldService().getRunningApplications()) {
                if (application.isChoose()) {
                    removeApps.add(application);
                }
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Settings.canDrawOverlays(getActivity())) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse("package:" + getActivity().getPackageName()));
                        startActivity(intent);
                        return;
                    }
                }

                if (Utils.isAccessibilitySettingsOn(getActivity())) {
                    activity.getMonitorShieldService().getRunningApplications().clear();
                    getActivity().getSupportFragmentManager().popBackStack();
                    activity.refresh();
                    activity.setShowAd(false);
                    boosterService.boost(removeApps);
                } else {
                    EnableAccessbilityDialog dialog = new EnableAccessbilityDialog(getActivity());
                    dialog.setCallBack(new EnableAccessbilityDialog.CallBack() {
                        @Override
                        public void execute() {
                            new RemoveApp().execute(removeApps);
                        }
                    });
                    dialog.show();
                }
            } else {
                new RemoveApp().execute(removeApps);
            }
        }
    }

    class RemoveApp extends AsyncTask<List<Application>, Application, Void> {

        private int total;

        public RemoveApp() {
            super();
            total = totalBoost;
        }

        @Override
        protected Void doInBackground(List<Application>... params) {
            for (Application removeApp : params[0]) {
                Utils.killBackgroundProcesses(getActivity(), removeApp.getPackageName());
                publishProgress(removeApp);
                try {
                    Thread.sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Application... values) {
            super.onProgressUpdate(values);
            adapter.notifyItemRemoved(activity.getMonitorShieldService().getRunningApplications().indexOf(values[0]));
            activity.getMonitorShieldService().getRunningApplications().remove(values[0]);
            total -= values[0].getSize() / 1024;
            tv_memory_boost.setText(String.valueOf(total));
            tv_boost.setText(getResources().getString(R.string.boost) + " " + totalBoost + "MB");
            tv_count_running_app.setText(activity.getMonitorShieldService().getRunningApplications().size() + " " + getResources().getString(R.string.apps_running));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            activity.getMonitorShieldService().getRunningApplications().clear();
            getActivity().getSupportFragmentManager().popBackStack();
            activity.refresh();
        }
    }
}
