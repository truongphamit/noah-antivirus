package com.noah.antivirus.iface;

/**
 * Created by truongpq on 8/18/16.
 */
public interface ActivityStartingListener {
    void onActivityStarting(String packageName);
}
