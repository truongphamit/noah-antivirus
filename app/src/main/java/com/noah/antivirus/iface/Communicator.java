package com.noah.antivirus.iface;

/**
 * Created by truongpq on 26/09/2016.
 */

public interface Communicator {
    int TYPE_TRUST_APP = 1;
    int TYPE_IGNORE_SETTING = 2;
    int TYPE_UNINSTALL_APP = 3;
    int TYPE_REMOVE_SETTING = 4;
    void respond(IProblem iProblem , int type);
}
