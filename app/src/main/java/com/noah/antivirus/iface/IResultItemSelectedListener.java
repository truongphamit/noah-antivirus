package com.noah.antivirus.iface;

import android.content.Context;
import android.widget.ImageView;

/**
 * Created by Magic Frame on 19/01/2016.
 */
public interface IResultItemSelectedListener
{
    public void onItemSelected(IProblem bpdw, ImageView iv_icon_app, Context c);
}
