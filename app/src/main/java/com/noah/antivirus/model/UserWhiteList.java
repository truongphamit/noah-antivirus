package com.noah.antivirus.model;

import android.content.Context;

import com.noah.antivirus.iface.IProblem;

import java.util.Set;

/**
 * Created by hexdump on 22/01/16.
 */
public class UserWhiteList extends JSONDataSet<IProblem>
{
    public UserWhiteList(Context context)
    {
        super(context,"userwhitelist.json",new ProblemFactory());
    }

    public boolean checkIfSystemPackageInList(Class<?> type)
    {
        Set<IProblem> problems=getSet();

        SystemProblem problem=null;

        for(IProblem p : problems)
        {
            if(p.getType()== IProblem.ProblemType.SystemProblem)
            {
                problem=(SystemProblem) p;
                if(problem.getClass()==type)
                    return true;
            }
        }

        return false;
    }
}
