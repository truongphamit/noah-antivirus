package com.noah.antivirus.model;

import android.content.Context;

import com.noah.antivirus.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

/**
 * Created by truongpq on 8/18/16.
 */
public class AppsLocked {
    private String _filePath;
    private Hashtable<String, AppLock> appLocks;
    private Context context;

    public Hashtable<String, AppLock> getAppLocks() {
        return appLocks;
    }

    public AppsLocked(Context context) {
        this.context = context;
        _filePath = Utils.getInternalDataPath(context) + File.separatorChar + "apps_locked.json";
        appLocks = new Hashtable<>();

        //Generate file if it does not exist
        if (!Utils.existsFile(_filePath)) {
            try {
                Utils.writeTextFile(_filePath, "{\n" +
                        "  \"data\": [" +
                        "{\"package_name\":\"com.android.settings\"}" +
                        "  ]\n" +
                        "}\n");
            } catch (IOException ioEx) {
                ioEx.printStackTrace();
            }
            loadFromJson();
        } else
            loadFromJson();
    }

    public void loadFromJson() {
        try {
            String jsonFile = Utils.loadJSONFromFile(context, _filePath);
            JSONObject obj = new JSONObject(jsonFile);
            JSONArray appsArray = obj.getJSONArray("data");
            for (int i = 0; i < appsArray.length(); i++) {
                JSONObject app = appsArray.getJSONObject(i);
                String packageName = app.getString("package_name");
                appLocks.put(packageName, new AppLock(Utils.getAppNameFromPackage(context, packageName), packageName, true));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public synchronized void writeToJSON() {
        JSONObject jo;
        JSONArray jsonArray = new JSONArray();

        try {
            for (String key : appLocks.keySet()) {
                jo = new JSONObject();
                jo.put("package_name", appLocks.get(key).getPackageName());
                jsonArray.put(jo);
            }

            JSONObject rootObj = new JSONObject();
            rootObj.put("data", jsonArray);

            Utils.writeTextFile(_filePath, rootObj.toString());
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    public void add(AppLock appLock) {
        appLocks.put(appLock.getPackageName(), appLock);
        writeToJSON();
    }

    public void remove(AppLock appLock) {
        appLocks.remove(appLock.getPackageName());
        writeToJSON();
    }

    public boolean isAppLocked(String packageNAme) {
        return appLocks.containsKey(packageNAme);
    }
}
