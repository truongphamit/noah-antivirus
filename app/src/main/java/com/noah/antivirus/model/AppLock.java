package com.noah.antivirus.model;

import android.support.annotation.NonNull;

/**
 * Created by truongpq on 8/18/16.
 */
public class AppLock implements Comparable<AppLock>{
    private String name;
    private String packageName;
    private boolean isLock;
    private boolean recommend;

    public AppLock(String name, String packageName, boolean isLock) {
        this.name = name;
        this.packageName = packageName;
        this.isLock = isLock;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public boolean isLock() {
        return isLock;
    }

    public void setLock(boolean lock) {
        isLock = lock;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRecommend() {
        return recommend;
    }

    public void setRecommend(boolean recommend) {
        this.recommend = recommend;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof AppLock) return packageName.equals(((AppLock)o).getPackageName());
        return false;
    }

    @Override
    public int compareTo(@NonNull AppLock another) {
        if (recommend && another.isRecommend()) return name.compareTo(another.getName());
        if (recommend) return -1;
        if (another.isRecommend()) return 1;

        if (isLock && another.isLock) return name.compareTo(another.getName());
        if (isLock) return -1;
        if (another.isLock) return 1;

        return name.compareTo(another.getName());
    }
}
