package com.noah.antivirus.model;

import android.graphics.drawable.Drawable;

/**
 * Created by truongpq on 7/26/16.
 */
public class BrowserHistory {
    private String title;
    private String url;
    private Drawable icon;

    public BrowserHistory(String title, String url, Drawable icon) {
        this.title = title;
        this.url = url;
        this.icon = icon;
    }

    public Drawable getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof BrowserHistory) {
            BrowserHistory x = (BrowserHistory)o;
            return title.equals(((BrowserHistory) o).getTitle()) && url.equals(x.getUrl());
        }
        return false;
    }
}
