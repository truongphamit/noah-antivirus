package com.noah.antivirus.model;

import android.support.annotation.NonNull;

/**
 * Created by truongpq on 9/9/16.
 */
public class Image implements Comparable<Image>{
    private int id;
    private String name;
    private String appName;
    private String date;
    private String path;

    public Image() {

    }

    public Image(String name, String appName, String date, String path) {
        this.name = name;
        this.appName = appName;
        this.date = date;
        this.path = path;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public int compareTo(@NonNull Image another) {
        return date.compareTo(another.getDate());
    }
}
