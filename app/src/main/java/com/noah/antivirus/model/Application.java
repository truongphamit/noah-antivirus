package com.noah.antivirus.model;

import android.graphics.drawable.Drawable;

/**
 * Created by truongpq on 7/27/16.
 */
public class Application {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private int pid;
    private String packageName;
    private boolean isChoose;
    private Drawable icon;
    private long size;

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public Application() {}

    public Application(int pid, String packageName) {
        this.pid = pid;
        this.packageName = packageName;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public boolean isChoose() {
        return isChoose;
    }

    public void setChoose(boolean choose) {
        isChoose = choose;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Application) {
            if (packageName.equals(((Application) o).getPackageName())) return true;
        }
        return false;
    }
}
