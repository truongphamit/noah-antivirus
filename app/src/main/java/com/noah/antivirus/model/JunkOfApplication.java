package com.noah.antivirus.model;

/**
 * Created by truongpq on 8/9/16.
 */
public class JunkOfApplication implements Comparable<JunkOfApplication>{
    private String packageName;
    private long cacheSize;

    public JunkOfApplication(String packageName, long cacheSize) {
        this.packageName = packageName;
        this.cacheSize = cacheSize;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public long getCacheSize() {
        return cacheSize;
    }

    public void setCacheSize(long cacheSize) {
        this.cacheSize = cacheSize;
    }

    @Override
    public int compareTo(JunkOfApplication another) {
        return Long.valueOf(cacheSize).compareTo(another.getCacheSize());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JunkOfApplication) return (packageName.equals(((JunkOfApplication) obj).getPackageName()));
        return super.equals(obj);
    }
}
