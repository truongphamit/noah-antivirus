package com.noah.antivirus.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Build;
import android.util.Log;
import android.view.Surface;
import android.view.WindowManager;

import com.noah.antivirus.databases.ImagesDatabaseHelper;
import com.noah.antivirus.activities.AppLockCreatePasswordActivity;
import com.noah.antivirus.util.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by truongpq on 9/13/16.
 */
public class Selfie {
    public static final String KEY_THIEVES = "thieves";
    public static final String KEY_APP_THIEVES = "app_thieves";
    public final String APP_TAG = getClass().getName();
    private Camera camera;
    private Context context;
    private String pakageName;
    private SharedPreferences sharedPreferences;
    private ImagesDatabaseHelper imagesDatabaseHelper;


    public Selfie(Context context, String pakageName) {
        this.context = context;
        this.pakageName = pakageName;
        sharedPreferences = context.getSharedPreferences(AppLockCreatePasswordActivity.SHARED_PREFERENCES_NAME, 0);
        imagesDatabaseHelper = ImagesDatabaseHelper.getInstance(context);
    }

    public void takePhoto() {
        final String imagesPath = context.getFilesDir().getPath() + File.separator + "images";
        File imageFolder = new File(imagesPath);
        if (!imageFolder.exists()) {
            imageFolder.mkdirs();
        }

        int id = getFrontCameraId();
        if (id == -1) {
            Log.w(APP_TAG, "No front camera available");
            return;
        }
        try {
            Log.d(APP_TAG, "trying id " + id);
            camera = Camera.open(id);
            camera.setPreviewTexture(new SurfaceTexture(10));
            setCameraDisplayOrientation(context, id, camera);
            camera.startPreview();
            camera.autoFocus(null);
            camera.takePicture(null, null, new Camera.PictureCallback() {
                @Override
                public void onPictureTaken(byte[] data, Camera camera) {
                    Log.d(APP_TAG, "picturesaver!");
                    if (data == null) {
                        return;
                    }

                    SimpleDateFormat format = new SimpleDateFormat("yyyymmdd-hhmmss");
                    String date = format.format(new Date());
                    String file = date + ".jpg";

                    String filename = imagesPath + File.separator + file;

                    File pictureFile = new File(filename);
                    try {
                        FileOutputStream fos = new FileOutputStream(pictureFile);
                        Bitmap realImage = BitmapFactory.decodeByteArray(data, 0, data.length);
                        realImage = rotate(realImage, -90);
                        realImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        fos.close();
                    } catch (Exception error) {
                        Log.w(APP_TAG, "Could not save image");
                    }

                    Image image = new Image();
                    image.setName(file);
                    image.setAppName(Utils.getAppNameFromPackage(context, pakageName));
                    image.setPath(filename);
                    long id = imagesDatabaseHelper.add(image);
                    if (id != -1) {
                        sharedPreferences.edit().putBoolean(KEY_THIEVES, true).apply();
                        sharedPreferences.edit().putLong(KEY_APP_THIEVES, id).apply();
                    }

                    if (camera != null) {
                        camera.release();
                    }
                }
            });
        } catch (Exception e) {
            Log.e(APP_TAG, "Failed to take picture", e);
            close();
        }
    }

    public void close() {
        if (camera != null) {
            camera.release();
            camera = null;
        }
    }


    public static void setCameraDisplayOrientation(Context c, int cameraId, Camera camera) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        int rotation = wm.getDefaultDisplay().getRotation();
        int degrees = rotation == Surface.ROTATION_0 ? 0
                : rotation == Surface.ROTATION_90 ? 90
                : rotation == Surface.ROTATION_180 ? 180 : 270;

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

    public boolean hasFrontCamera() {
        return getFrontCameraId() != -1;
    }

    private int getFrontCameraId() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
            return -1;
        }
        if (!context.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA_FRONT)) {
            return -1;
        }
        Camera.CameraInfo ci = new Camera.CameraInfo();
        for (int id = 0; id < Camera.getNumberOfCameras(); id++) {
            Camera.getCameraInfo(id, ci);
            if (ci.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
                return id;
        }
        return -1;
    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        //       mtx.postRotate(degree);
        mtx.setRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }
}
