package com.noah.antivirus.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.noah.antivirus.model.Image;
import com.noah.antivirus.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by truongpq on 9/9/16.
 */
public class ImagesDatabaseHelper extends SQLiteOpenHelper {
    private static ImagesDatabaseHelper sInstance;

    private static final String DATABASE_NAME = "imagesDatabase";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_IMAGES = "images";

    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_APP_NAME = "appName";
    private static final String KEY_DATE = "date";
    private static final String KEY_PATH = "path";

    public static synchronized ImagesDatabaseHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new ImagesDatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    public ImagesDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_IMAGES +
                "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_NAME + " TEXT," +
                KEY_APP_NAME + " TEXT," +
                KEY_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP," +
                KEY_PATH + " TEXT" +
                ")";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            // Simplest implementation is to drop all old tables and recreate them
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGES);
            onCreate(db);
        }
    }

    public long add(Image image) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, image.getName());
        values.put(KEY_APP_NAME, image.getAppName());
        values.put(KEY_DATE, Utils.getDateTime());
        values.put(KEY_PATH, image.getPath());
        return db.insert(TABLE_IMAGES, null, values);
    }

    public List<Image> getAllImages() {
        List<Image> images = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_IMAGES;

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Image image = new Image();
                image.setId(cursor.getInt(0));
                image.setName(cursor.getString(1));
                image.setAppName(cursor.getString(2));
                image.setDate(cursor.getString(3));
                image.setPath(cursor.getString(4));
                images.add(image);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return images;
    }

    public Image findByID(long id) {
        Image image = new Image();
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_IMAGES + " WHERE " + KEY_ID + " = " + id, null);

        if (cursor.moveToFirst()) {
            do {
                image.setId(cursor.getInt(0));
                image.setName(cursor.getString(1));
                image.setAppName(cursor.getString(2));
                image.setDate(cursor.getString(3));
                image.setPath(cursor.getString(4));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return image;
    }

    public void delete(long id) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_IMAGES + " WHERE " + KEY_ID + " = " + id);
    }
}
