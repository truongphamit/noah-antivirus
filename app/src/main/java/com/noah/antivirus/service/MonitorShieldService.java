package com.noah.antivirus.service;

import android.app.ActivityManager;
import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;

import com.noah.antivirus.R;
import com.noah.antivirus.activities.MainActivity;
import com.noah.antivirus.iface.ActivityStartingListener;
import com.noah.antivirus.iface.IProblem;
import com.noah.antivirus.model.AppProblem;
import com.noah.antivirus.model.Application;
import com.noah.antivirus.model.MenacesCacheSet;
import com.noah.antivirus.model.PackageData;
import com.noah.antivirus.model.UserWhiteList;
import com.noah.antivirus.receiver.PackageBroadcastReceiver;
import com.noah.antivirus.util.ProblemsDataSetTools;
import com.noah.antivirus.util.Utils;
import com.noah.antivirus.activities.AppLockCreatePasswordActivity;
import com.noah.antivirus.iface.IPackageChangesListener;
import com.noah.antivirus.model.AppData;
import com.noah.antivirus.model.AppLock;
import com.noah.antivirus.model.PermissionData;
import com.noah.antivirus.model.Scanner;
import com.noah.antivirus.util.ActivityStartingHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

/**
 * Created by hexdump on 14/01/16.
 * Based on stack overflow link: http://stackoverflow.com/questions/20594936/communication-between-activity-and-service
 */
public class MonitorShieldService extends Service {
    final String _logTag = MonitorShieldService.class.getSimpleName();

    private final IBinder _binder = new MonitorShieldLocalBinder();

    PackageBroadcastReceiver _packageBroadcastReceiver;

    Set<PackageData> _whiteListPackages;

    public Set<PackageData> getWhiteListPackages() {
        return _whiteListPackages;
    }

    Set<PackageData> _blackListPackages;

    public Set<PackageData> getBlackListPackages() {
        return _blackListPackages;
    }

    Set<PackageData> _blackListActivities;

    public Set<PackageData> getBlackListActivities() {
        return _blackListActivities;
    }

    Set<PermissionData> _suspiciousPermissions;

    public Set<PermissionData> getSuspiciousPermissions() {
        return _suspiciousPermissions;
    }

    UserWhiteList _userWhiteList = null;

    public UserWhiteList getUserWhiteList() {
        return _userWhiteList;
    }

    MenacesCacheSet _menacesCacheSet = null;

    public MenacesCacheSet getMenacesCacheSet() {
        return _menacesCacheSet;
    }

    IClientInterface _clientInterface = null;

    public void registerClient(IClientInterface clientInterface) {
        _clientInterface = clientInterface;
    }

    List<Application> runningApplications;

    public List<Application> getRunningApplications() {
        return runningApplications;
    }

    List<AppLock> appLock;

    public List<AppLock> getAppLock() {
        return appLock;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    static int _currentNotificationId = 0;

    @Override
    public void onCreate() {
        super.onCreate();

        if (getSharedPreferences(AppLockCreatePasswordActivity.SHARED_PREFERENCES_NAME, 0).getBoolean(AppLockCreatePasswordActivity.KEY_APPLOCKER_SERVICE, true)) {
            startLockAppTask();
        }

        _packageBroadcastReceiver = new PackageBroadcastReceiver();
        _packageBroadcastReceiver.setPackageBroadcastListener(new IPackageChangesListener() {

            public void OnPackageAdded(Intent i) {
                if (getSharedPreferences("Settings", 0).getBoolean("auto_scan", true)) {
                    String packageName = i.getData().getSchemeSpecificPart();
                    scanApp(packageName);
                }
            }

            public void OnPackageRemoved(Intent intent) {
                //TODO: Add code to make ourselves sure that not installed apps will appear in userwhitelist or results fragment
                String packageName = intent.getData().getSchemeSpecificPart();
                ProblemsDataSetTools.removeAppProblemByPackage(_menacesCacheSet, packageName);
                _menacesCacheSet.writeToJSON();
                ProblemsDataSetTools.removeAppProblemByPackage(_userWhiteList, packageName);
                _userWhiteList.writeToJSON();
            }
        });

        IntentFilter packageFilter = new IntentFilter();
        packageFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        packageFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);

        packageFilter.addDataScheme("package");
        this.registerReceiver(_packageBroadcastReceiver, packageFilter);

        registerReceiver(forceStopBroadcast, new IntentFilter(MyAccessibilityService.BROADCAST_FORCE_STOP));

        _loadDataFiles();
        _loadData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(_packageBroadcastReceiver);
        unregisterReceiver(forceStopBroadcast);
        _packageBroadcastReceiver = null;

        if (lockAppTask != null) lockAppTask.cancel();
    }

    @Override
    public IBinder onBind(Intent i) {
        return _binder;
    }

    //returns the getInstance of the service
    public class MonitorShieldLocalBinder extends Binder {
        public MonitorShieldService getServiceInstance() {
            return MonitorShieldService.this;
        }
    }

    public interface IClientInterface {
        //Called when a menace is found by the watchdog
        void onMonitorFoundMenace(IProblem menace);

        //All packages to scan can be useful if the client wants to do for example some animation to cheat :P
        void onScanResult(List<PackageInfo> allPackages, Set<IProblem> menacesFound);
    }

    private void _loadDataFiles() {

        _whiteListPackages = new HashSet<>();
        _blackListPackages = new HashSet<>();
        _blackListActivities = new HashSet<>();
        _suspiciousPermissions = new HashSet<>();

        //Build/Load user list
        _userWhiteList = new UserWhiteList(this);
        //Build/Load MenaceCache list
        _menacesCacheSet = new MenacesCacheSet(this);

        //Load WhiteList
        try {
            String jsonFile = Utils.loadJSONFromAsset(this, "whiteList.json");
            JSONObject obj = new JSONObject(jsonFile);

            JSONArray m_jArry = obj.getJSONArray("data");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject temp = m_jArry.getJSONObject(i);
                PackageData pd = new PackageData(temp.getString("packageName"));
                _whiteListPackages.add(pd);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Load blackPackagesList
        try {
            String jsonFile = Utils.loadJSONFromAsset(this, "blackListPackages.json");
            JSONObject obj = new JSONObject(jsonFile);

            JSONArray m_jArry = obj.getJSONArray("data");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject temp = m_jArry.getJSONObject(i);
                PackageData pd = new PackageData(temp.getString("packageName"));
                _blackListPackages.add(pd);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Load blackActivitiesList
        try {
            String jsonFile = Utils.loadJSONFromAsset(this, "blackListActivities.json");
            JSONObject obj = new JSONObject(jsonFile);

            JSONArray m_jArry = obj.getJSONArray("data");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject temp = m_jArry.getJSONObject(i);
                PackageData pd = new PackageData(temp.getString("packageName"));
                _blackListActivities.add(pd);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Load permissions data
        try {
            String jsonFile = Utils.loadJSONFromAsset(this, "permissions.json");
            JSONObject obj = new JSONObject(jsonFile);

            JSONArray m_jArry = obj.getJSONArray("data");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject temp = m_jArry.getJSONObject(i);
                PermissionData pd = new PermissionData(temp.getString("permissionName"), temp.getInt("dangerous"));
                _suspiciousPermissions.add(pd);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void _loadData() {
        runningApplications = new ArrayList<>();
        // Kiểm tra thời gian lần cuối boost tới thời điểm hiện tại phải lớn hơn 5p
        if (Utils.getCurrentTime() - getSharedPreferences("Settings", 0).getLong(BoosterService.PREFERENCES_LAST_TIME_BOOST, 0) >= 5 * 60 * 1000) {
            runningApplications.addAll(Utils.getRunningApplications(this));
        }
        appLock = new ArrayList<>();
        appLock.addAll(Utils.getAppLock(this));
    }

    protected boolean _checkIfPackageInWhiteList(String packageName, Set<PackageData> whiteListPackages) {
        for (PackageData packageInfo : whiteListPackages) {
            String packageMask = packageInfo.getPackageName();
            if (Utils.stringMatchesMask(packageName, packageMask))
                return true;
        }

        return false;
    }

    public void scanFileSystem() {
        //Scan installed packages
        List<PackageInfo> allPackages = Utils.getApps(this, PackageManager.GET_ACTIVITIES | PackageManager.GET_PERMISSIONS);
        List<PackageInfo> nonSystemApps = Utils.getNonSystemApps(this, allPackages);

        //Packages with problems will be stored here
        Set<IProblem> tempBadResults = new HashSet<IProblem>();

        //Filter white listed apps
        List<PackageInfo> potentialBadApps = _removeWhiteListPackagesFromPackageList(nonSystemApps, _whiteListPackages);
        potentialBadApps = _removeWhiteListPackagesFromPackageList(potentialBadApps, ProblemsDataSetTools.getAppProblemsAsPackageDataList(_userWhiteList));

        Scanner.scanForBlackListedActivityApps(potentialBadApps, _blackListActivities, tempBadResults);
        Scanner.scanForSuspiciousPermissionsApps(potentialBadApps, _suspiciousPermissions, tempBadResults);
        Scanner.scanInstalledAppsFromGooglePlay(this, potentialBadApps, tempBadResults);
        Scanner.scanSystemProblems(this, _userWhiteList, tempBadResults);

        _menacesCacheSet.addItems(tempBadResults);
        _menacesCacheSet.writeToJSON();

        if (_clientInterface != null)
            _clientInterface.onScanResult(allPackages, tempBadResults);
    }

    public void scanApp(String packageName) {
        AppData appData = AppData.getInstance(this);

        Intent toExecuteIntent = new Intent(MonitorShieldService.this, MainActivity.class);

        Intent openAppIntent = getPackageManager().getLaunchIntentForPackage(packageName);

        String appName = Utils.getAppNameFromPackage(MonitorShieldService.this, packageName);

        boolean whiteListed = Scanner.isAppWhiteListed(packageName, _whiteListPackages);

        if (whiteListed) {
            Utils.notificatePush(MonitorShieldService.this, _currentNotificationId++, R.drawable.ic_noti_safe,
                    appName + " " + getString(R.string.trusted_message), appName, "App " + appName + " " + getString(R.string.trusted_by_app), openAppIntent);
        } else {
            PackageInfo pi;
            try {
                pi = Utils.getPackageInfo(this, packageName, PackageManager.GET_ACTIVITIES | PackageManager.GET_PERMISSIONS);
            } catch (PackageManager.NameNotFoundException ex) {
                pi = null;
            }

            if (pi != null) {
                AppProblem bpbr = new AppProblem(pi.packageName);
                List<ActivityInfo> recycleList = new ArrayList<ActivityInfo>();
                Scanner.scanForBlackListedActivityApp(pi, bpbr, _blackListActivities, recycleList);
                Scanner.scanForSuspiciousPermissionsApp(pi, bpbr, _suspiciousPermissions);
                Scanner.scanInstalledAppFromGooglePlay(this, bpbr);

                if (bpbr.isMenace()) {
                    //Do not scan if we haven't done any
                    if (appData.getFirstScanDone()) {
                        _menacesCacheSet.addItem(bpbr);
                        _menacesCacheSet.writeToJSON();
                    }

                    if (_clientInterface != null) {
                        _clientInterface.onMonitorFoundMenace(bpbr);
                    }

                    Utils.notificatePush(MonitorShieldService.this, _currentNotificationId++, R.drawable.ic_noti_problems,
                            appName + " " + getString(R.string.has_been_scanned), appName, getString(R.string.enter_to_solve_problems), toExecuteIntent);

                } else
                    Utils.notificatePush(MonitorShieldService.this, _currentNotificationId++, R.drawable.ic_noti_safe,
                            appName + " " + getString(R.string.is_secure), appName, getString(R.string.has_no_threats), openAppIntent);
            }
            //}
        }
    }

    protected List<PackageInfo> _removeWhiteListPackagesFromPackageList(List<PackageInfo> packagesToSearch, Set<? extends PackageData> whiteListPackages) {
        boolean found;

        List<PackageInfo> trimmedPackageList = new ArrayList<PackageInfo>(packagesToSearch);

        //Check against whitelist
        for (PackageData pd : whiteListPackages) {
            PackageInfo p = null;
            int index = 0;
            String mask = pd.getPackageName();
            found = false;

            while (!found && index < trimmedPackageList.size()) {
                p = trimmedPackageList.get(index);

                if (Utils.stringMatchesMask(p.packageName, mask))
                    trimmedPackageList.remove(index);
                else
                    ++index;
            }
        }

        return trimmedPackageList;
    }

    // Locked APP
    private TimerTask lockAppTask;
    private boolean isBoosterRunning = false;

    private BroadcastReceiver forceStopBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isBoosterRunning = intent.getBooleanExtra(MyAccessibilityService.FORCE_STOP, false);
        }
    };

    public void startLockAppTask() {
        if (lockAppTask != null) lockAppTask.cancel();
        lockAppTask = new LockAppTask(new ActivityStartingHandler(this));
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(lockAppTask, 0, 50);
    }

    public void stopLockAppTask() {
        if (lockAppTask != null) lockAppTask.cancel();
    }

    private class LockAppTask extends TimerTask {

        private ActivityStartingListener listener;

        public LockAppTask(ActivityStartingListener listener) {
            this.listener = listener;
        }

        @Override
        public void run() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                String topPackageName = null;
                UsageStatsManager mUsageStatsManager = (UsageStatsManager) getSystemService(USAGE_STATS_SERVICE);
                long time = System.currentTimeMillis();
                // We get usage stats for the last 10 seconds
                List<UsageStats> stats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 10, time);
                // Sort the stats by the last time used
                if (stats != null) {
                    SortedMap<Long, UsageStats> mySortedMap = new TreeMap<>();
                    for (UsageStats usageStats : stats) {
                        mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
                    }
                    if (!mySortedMap.isEmpty()) {
                        topPackageName = mySortedMap.get(mySortedMap.lastKey()).getPackageName();
                    }
                }

                if (listener != null) {
                    if (topPackageName != null) {
                        if (!isBoosterRunning) listener.onActivityStarting(topPackageName);
                    }
                }
            } else {
                ActivityManager am = (ActivityManager) getBaseContext().getSystemService(ACTIVITY_SERVICE);
                PackageManager pm = getBaseContext().getPackageManager();

                String mPackageName = null;
                if (Build.VERSION.SDK_INT > 20) {
                    List<ActivityManager.RunningAppProcessInfo> appProcesses = am.getRunningAppProcesses();
                    if (appProcesses != null)
                        if (appProcesses.size() >= 0) {
                            ActivityManager.RunningAppProcessInfo appProcess = appProcesses.get(0);
                            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                                mPackageName = appProcess.processName;
                            }
                        }
                } else {
                    List<ActivityManager.RunningTaskInfo> runningTaskInfos = am.getRunningTasks(1);
                    if (runningTaskInfos.size() > 0) {
                        mPackageName = runningTaskInfos.get(0).topActivity.getPackageName();
                    }
                }

                PackageInfo foregroundAppPackageInfo = null;
                try {
                    if (mPackageName != null) {
                        foregroundAppPackageInfo = pm.getPackageInfo(mPackageName, 0);
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    // TODO Auto-generated catch block
                    System.out.println("Exception in run method " + e);
                    e.printStackTrace();
                }

                if (listener != null) {
                    if (foregroundAppPackageInfo != null) {
                        String s = foregroundAppPackageInfo.packageName;
                        if (s != null) {
                            if (!isBoosterRunning) listener.onActivityStarting(s);
                        }
                    }
                }
            }
        }
    }
}
