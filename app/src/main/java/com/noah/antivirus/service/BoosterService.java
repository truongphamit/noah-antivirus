package com.noah.antivirus.service;

import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.activities.DoneBoostActivity;
import com.noah.antivirus.activities.PhoneBoostActivity;
import com.noah.antivirus.activities.SafeActivity;
import com.noah.antivirus.model.Application;
import com.noah.antivirus.util.Utils;

import java.util.List;

public class BoosterService extends Service {
    public String TAG = "BoosterService";

    public static final String BROADCAST_ACCESSIBILITY = "BROADCAST_ACCESSIBILITY";
    public static final String STOPPED = "STOPPED";
    public static final String PREFERENCES_LAST_TIME_BOOST = "LAST_TIME_BOOST";

    private WindowManager windowManager;
    private WindowManager.LayoutParams params;
    private LayoutInflater layoutInflater;
    private View view;
    private ImageView img_close;
    private ImageView img_app_icon;
    private ImageView img_bin;
    private TextView tv_progress;
    private AnimationSet animationset;
    private List<Application> applications;
    private int index;
    private int size;
    private boolean isAddViewInWindowManager = false;

    private Activity activity;

    private SharedPreferences settings;
    private int timeoutDestroyService = 10000;
    private long totalCleanSize;

    /**
     * the handler to make service stop self after 15s doesn't work
     */
    private Handler destroyHandler;


    public BoosterService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    public class BoosterBinder extends Binder {
        public BoosterService getService() {
            return BoosterService.this;
        }
    }

    private IBinder iBinder = new BoosterBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isAddViewInWindowManager = false;
        totalCleanSize = 0;

        settings = getSharedPreferences("Settings", 0);
        registerReceiver(accessibilityBroadcast, new IntentFilter(BROADCAST_ACCESSIBILITY));
        windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
                    WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                            | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                            | WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                            | View.SYSTEM_UI_FLAG_FULLSCREEN,
                    PixelFormat.TRANSLUCENT);
        } else {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
                    WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                            | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                            | View.SYSTEM_UI_FLAG_FULLSCREEN,
                    PixelFormat.TRANSLUCENT);
        }

        params.gravity = Gravity.TOP;
        params.screenOrientation = Configuration.ORIENTATION_PORTRAIT;

        destroyHandler = new Handler();

        layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.booster_layout, null);
        img_close = (ImageView) view.findViewById(R.id.img_close);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isServiceRunning(BoosterService.this, LockService.class)) {
                    stopService(new Intent(BoosterService.this, LockService.class));
                }

                if (activity != null) activity.finish();
                sendBroadcast(new Intent().setAction(MyAccessibilityService.BROADCAST_FORCE_STOP).putExtra(MyAccessibilityService.FORCE_STOP, false));
                sendBroadcast(new Intent().setAction(MyAccessibilityService.BROADCAST_BACK_TAP));

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        BoosterService.this.stopSelf();
                        destroyHandler.removeCallbacks(destroyServiceCallbacks);
                    }
                }, 400);
            }
        });

        img_app_icon = (ImageView) view.findViewById(R.id.img_app_icon);
        img_bin = (ImageView) view.findViewById(R.id.img_bin);
        tv_progress = (TextView) view.findViewById(R.id.tv_progress);

        View bg_animation_scan = view.findViewById(R.id.bg_animation_scan);
        Animation bgAnimationScan = AnimationUtils.loadAnimation(this, R.anim.bg_animation_scan);
        bg_animation_scan.setAnimation(bgAnimationScan);

        animationset = new AnimationSet(true);
        Animation scaleAnimation = new ScaleAnimation(1f, 0.0f, 1f, 0.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setStartOffset(250);
        scaleAnimation.setDuration(250);
        scaleAnimation.setFillAfter(true);
        Animation translateAnimation = new TranslateAnimation(img_app_icon.getLeft(), img_app_icon.getLeft(), img_app_icon.getTop(), img_app_icon.getTop() - 400);
        translateAnimation.setDuration(500);
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        animationset.addAnimation(scaleAnimation);
        animationset.addAnimation(translateAnimation);

    }

    private BroadcastReceiver accessibilityBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            img_app_icon.startAnimation(animationset);
            animationset.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    --index;
                    applications.remove(applications.size() - 1);
                    if (applications.isEmpty()) {
                        settings.edit().putLong(PREFERENCES_LAST_TIME_BOOST, Utils.getCurrentTime()).apply();
                        sendBroadcast(new Intent().setAction(MyAccessibilityService.BROADCAST_FORCE_STOP).putExtra(MyAccessibilityService.FORCE_STOP, false));

                        Log.d(TAG, "App boost is empty, stop service");

                        destroyHandler.removeCallbacks(destroyServiceCallbacks);
                        BoosterService.this.stopSelf();
                    } else {
                        img_app_icon.setImageDrawable(Utils.getIconFromPackage(applications.get(index).getPackageName(), BoosterService.this));
                        tv_progress.setText("Boosting " + (size - index) + "/" + size);

                        Log.d(TAG, "start app: " + applications.get(applications.size() - 1).getPackageName());
                        startActivity(Utils.AppDetailsIntent(applications.get(applications.size() - 1).getPackageName()));

                        /**
                         * remove the old call back and add new task 15s delay
                         */
                        destroyHandler.removeCallbacks(destroyServiceCallbacks);
                        destroyHandler.postDelayed(destroyServiceCallbacks, timeoutDestroyService);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    };

    public void addViewInWindowManager() {
        if (!isAddViewInWindowManager) {
            windowManager.addView(view, params);
            isAddViewInWindowManager = true;
        }
    }

    public void boost(final List<Application> applications) {
        if (!isAddViewInWindowManager) {
            windowManager.addView(view, params);
            isAddViewInWindowManager = true;
        }

        if (applications.size() == 0) {
            this.stopSelf();
        }

        Log.d(TAG, "Boost size: " + applications.size());
        this.applications = applications;

        size = applications.size();
        index = applications.size() - 1;

        img_app_icon.setImageDrawable(Utils.getIconFromPackage(applications.get(index).getPackageName(), this));
        tv_progress.setText("Boosting " + (size - index) + "/" + size);
        sendBroadcast(new Intent().setAction(MyAccessibilityService.BROADCAST_FORCE_STOP).putExtra(MyAccessibilityService.FORCE_STOP, true));

        Log.d(TAG, "start app: " + applications.get(applications.size() - 1).getPackageName());
        startActivity(Utils.AppDetailsIntent(applications.get(applications.size() - 1).getPackageName()));

        destroyHandler.postDelayed(destroyServiceCallbacks, timeoutDestroyService);
    }

    public void boost(final List<Application> applications, Activity activity) {
        this.activity = activity;

        if (!isAddViewInWindowManager) {
            windowManager.addView(view, params);
            isAddViewInWindowManager = true;
        }

        if (applications.size() == 0) {
            this.stopSelf();
        }

        for (Application appScan : applications)
            totalCleanSize += appScan.getSize();

        Log.d(TAG, "Boost size: " + applications.size());
        this.applications = applications;
        size = applications.size();
        index = applications.size() - 1;

        img_app_icon.setImageDrawable(Utils.getIconFromPackage(applications.get(index).getPackageName(), this));
        tv_progress.setText("Boosting " + (size - index) + "/" + size);
        sendBroadcast(new Intent().setAction(MyAccessibilityService.BROADCAST_FORCE_STOP).putExtra(MyAccessibilityService.FORCE_STOP, true));

        Log.d(TAG, "start app: " + applications.get(applications.size() - 1).getPackageName());
        startActivity(Utils.AppDetailsIntent(applications.get(applications.size() - 1).getPackageName()));

        destroyHandler.postDelayed(destroyServiceCallbacks, timeoutDestroyService);
    }

    /**
     * the call back to make service stop self after 15s doesn't work
     */
    Runnable destroyServiceCallbacks = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "stop service in call back time out");
            sendBroadcast(new Intent().setAction(MyAccessibilityService.BROADCAST_FORCE_STOP).putExtra(MyAccessibilityService.FORCE_STOP, false));

            if (activity != null) activity.finish();
            BoosterService.this.stopSelf();
        }
    };

    @Override
    public void onDestroy() {
        destroyHandler.removeCallbacks(destroyServiceCallbacks);
        unregisterReceiver(accessibilityBroadcast);
        sendBroadcast(new Intent().setAction(MyAccessibilityService.BROADCAST_FORCE_STOP).putExtra(MyAccessibilityService.FORCE_STOP, false));
        try {
            if (view != null) {

                if (activity != null) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                windowManager.removeView(view);
                            } catch (Exception ignored) {
                            }
                        }
                    }, 1400);

                    if (activity.getLocalClassName().equals(PhoneBoostActivity.class.getName())) {
                        Intent intentDone = new Intent(getApplicationContext(), DoneBoostActivity.class);
                        intentDone.putExtra(DoneBoostActivity.TOTAL_SIZE_CLEANED, totalCleanSize);
                        activity.startActivity(intentDone);

                    } else {
//                        Intent intent = new Intent(getApplicationContext(), SafeActivity.class);
//                        intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.you_are_safe));
//                        activity.startActivity(intent);
                        Intent intentDone = new Intent(getApplicationContext(), DoneBoostActivity.class);
                        intentDone.putExtra(DoneBoostActivity.TOTAL_SIZE_CLEANED, totalCleanSize);
                        activity.startActivity(intentDone);
                    }
                    activity.finish();

                } else {
                    try {
                        windowManager.removeView(view);
                    } catch (Exception ignored) {
                    }
                }
            }
        } catch (Exception ignored) {

        }
        super.onDestroy();
    }
}
