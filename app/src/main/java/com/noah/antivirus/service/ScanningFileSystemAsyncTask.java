package com.noah.antivirus.service;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.graphics.drawable.ClipDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.activities.ScanningActivity;
import com.noah.antivirus.animation.BezierTranslateAnimation;
import com.noah.antivirus.animation.ScanningProgress;
import com.noah.antivirus.model.AppLock;
import com.noah.antivirus.model.AppProblem;
import com.noah.antivirus.model.Application;
import com.noah.antivirus.iface.IOnActionFinished;
import com.noah.antivirus.iface.IProblem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * Created by hexdump on 09/11/15.
 */
public class ScanningFileSystemAsyncTask extends AsyncTask<Void, Integer, Void> {
    IOnActionFinished _asyncTaskCallBack;


    public void setAsyncTaskCallback(IOnActionFinished asyncTaskCallback) {
        _asyncTaskCallBack = asyncTaskCallback;
    }

    private TextView tvProgress;
    private TextView bottomIssues;
    private ImageView imgThreat;
    private TextView tvThreat;
    private ImageView imgPrivacy;
    private TextView tvPrivacy;
    private ImageView imgBosster;
    private TextView tvBooster;
    private TextView tvStep;
    private TextView bottomIssues_privacy;
    private TextView bottomIssues_booster;
    private ImageView img_3;
    private ImageView img_2;
    private ClipDrawable clipDrawable3;
    private ClipDrawable clipDrawable2;

    private ScanningProgress scanningProgress;

    private boolean _isPaused = false;

    public void pause() {
        _isPaused = true;
    }

    public void resume() {
        _isPaused = false;
    }

    private boolean running = true;
    private int step = 1;

    private int totalPrivacyApps;
    private int totalBooster;
    private int totalProgress;

    private int countFound;
    private int countPrivacyApps;
    private int countBooster;

    private int countAnimation;

    private Context _activity;
    private List<PackageInfo> _packagesToScan;
    private Collection<AppProblem> _menaces;

    public ScanningFileSystemAsyncTask(ScanningActivity activity, List<PackageInfo> allPackages, Collection<IProblem> menaces) {
        _activity = activity;
        _packagesToScan = allPackages;

        countFound = 0;
        countPrivacyApps = 0;
        countBooster = 0;

        //Just get AppMenaces
        _menaces = new ArrayList<>();
        for (IProblem p : menaces) {
            if (p.getType() == IProblem.ProblemType.AppProblem)
                _menaces.add((AppProblem) p);
        }

        tvProgress = (TextView) activity.findViewById(R.id.tv_progress);
        bottomIssues = (TextView) activity.findViewById(R.id.bottomIssues);
        imgThreat = (ImageView) activity.findViewById(R.id.img_threat);
        tvThreat = (TextView) activity.findViewById(R.id.tv_threat);
        imgPrivacy = (ImageView) activity.findViewById(R.id.img_privacy);
        tvPrivacy = (TextView) activity.findViewById(R.id.tv_privacy);
        imgBosster = (ImageView) activity.findViewById(R.id.img_booster);
        tvBooster = (TextView) activity.findViewById(R.id.tv_booster);
        tvStep = (TextView) activity.findViewById(R.id.tv_step);
        bottomIssues_privacy = (TextView) activity.findViewById(R.id.bottomIssues_privacy);
        bottomIssues_booster = (TextView) activity.findViewById(R.id.bottomIssues_booster);
        scanningProgress = (ScanningProgress) activity.findViewById(R.id.progress);

        img_3 = (ImageView) activity.findViewById(R.id.img_3);
        clipDrawable3 = (ClipDrawable) img_3.getDrawable();
        img_2 = (ImageView) activity.findViewById(R.id.img_2);
        clipDrawable2 = (ClipDrawable) img_2.getDrawable();

    }

    @Override
    protected void onPreExecute() {
        ((ScanningActivity) _activity).getMonitorShieldService()._loadData();
        for (Application application : ((ScanningActivity) _activity).getMonitorShieldService().getRunningApplications()) {
            totalBooster += application.getSize() / 1024;
        }

        for (AppLock appLock : ((ScanningActivity) _activity).getMonitorShieldService().getAppLock()) {
            if (appLock.isRecommend()) ++totalPrivacyApps;
        }

        totalProgress = _packagesToScan.size() + totalPrivacyApps + totalBooster;
    }

    @Override
    protected void onCancelled() {
        running = false;
    }

    @Override
    protected Void doInBackground(Void... params) {
        int currentTotal = 0;
        int currentIndex = 0;

        try {
            //Scan App Problem
            PackageInfo pi;
            publishProgress(currentTotal);
            while (running && currentIndex < _packagesToScan.size()) {
                Thread.sleep(10 + (int )(Math.random() * 5));
                if (!_isPaused) {
                    pi = _packagesToScan.get(currentIndex);

                    boolean b = isPackageInMenacesSet(pi.packageName);
                    if (b) {
                        ++countFound;
                    }

                    ++currentTotal;
                    publishProgress(currentTotal);

                    ++currentIndex;
                }
            }

            // Scan App Lock
            step = 2;
            publishProgress(currentTotal);
            currentIndex = 0;
            while (running && currentIndex < totalPrivacyApps) {
                Thread.sleep(10 + (int )(Math.random() * 5));
                if (!_isPaused) {
                    ++currentIndex;
                    ++countPrivacyApps;

                    ++currentTotal;
                    publishProgress(currentTotal);
                }
            }

            // Booster
            step = 3;
            publishProgress(currentTotal);
            currentIndex = 0;

            while (running && currentIndex < totalBooster) {
                Thread.sleep(10 + (int )(Math.random() * 5));
                if (!_isPaused) {
                    ++currentIndex;
                    ++countBooster;

                    ++currentTotal;
                    publishProgress(currentTotal);
                }
            }

            step = 4;
            publishProgress(currentTotal);
        } catch (InterruptedException ex) {
            Log.w("APP", "Scanning task was interrupted");
        }

        return null;
    }

    boolean isPackageInMenacesSet(String packageName) {
        for (AppProblem menace : _menaces) {
            if (menace.getPackageName().equals(packageName))
                return true;
        }

        return false;
    }

    @Override
    protected void onProgressUpdate(Integer... params) {
        tvProgress.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
        int progress = params[0] * 100 / totalProgress;
        tvProgress.setText(progress + "%");
        scanningProgress.setProgress(progress);

        clipDrawable3.setLevel(10000 - progress * 100);
        clipDrawable2.setLevel(progress * 100);

        if (step == 1) {
            bottomIssues.setText(String.valueOf(countFound));
            return;
        }

        if (imgThreat.getVisibility() == View.VISIBLE) {
            imgThreat.setVisibility(View.GONE);
            if (countFound == 0) {
                bottomIssues.setVisibility(View.GONE);
                tvThreat.setText("0");
                tvThreat.setVisibility(View.VISIBLE);
                ++countAnimation;
            } else {
                setupAnimation(bottomIssues, tvThreat);
                bottomIssues.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
            }
            imgPrivacy.setImageResource(R.mipmap.ic_privacy_ac);
            tvStep.setText(_activity.getResources().getString(R.string.scanning_for_privacy));
        }

        if (step == 2) {
            bottomIssues_privacy.setText(String.valueOf(countPrivacyApps));
            return;
        }

        if (imgPrivacy.getVisibility() == View.VISIBLE) {
            imgPrivacy.setVisibility(View.GONE);
            if (countPrivacyApps == 0) {
                bottomIssues_privacy.setVisibility(View.GONE);
                tvPrivacy.setText("0");
                tvPrivacy.setVisibility(View.VISIBLE);
                ++countAnimation;
            } else {
                setupAnimation(bottomIssues_privacy, tvPrivacy);
                bottomIssues_privacy.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
            }
            imgBosster.setImageResource(R.mipmap.ic_phoneboost_ac);
            tvStep.setText(_activity.getResources().getString(R.string.freeable_memory));
        }

        if (step == 3) {
            bottomIssues_booster.setText(String.valueOf(countBooster));
            return;
        }

        if (imgBosster.getVisibility() == View.VISIBLE) {
            imgBosster.setVisibility(View.GONE);
            if (countBooster == 0) {
                bottomIssues_booster.setVisibility(View.GONE);
                tvBooster.setText("0MB");
                tvBooster.setVisibility(View.VISIBLE);
                ++countAnimation;
                if (countAnimation == 3) {
                    if (_asyncTaskCallBack != null)
                        _asyncTaskCallBack.onFinished();
                }
            } else {
                bottomIssues_booster.setText(bottomIssues_booster.getText() + "MB");
                setupAnimation(bottomIssues_booster, tvBooster);
                bottomIssues_booster.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
            }
        }
    }

    @Override
    protected void onPostExecute(Void result) {
//        if (_asyncTaskCallBack != null)
//            _asyncTaskCallBack.onFinished();
        scanningProgress.setVisibility(View.GONE);
    }

    private void setupAnimation(final View start, final View end) {
        start.post(new Runnable() {
            @Override
            public void run() {
                int[] startLocations = new int[2];
                start.getLocationOnScreen(startLocations);
                int startX = startLocations[0] + start.getWidth() / 2;
                int startY = startLocations[1] + start.getHeight();

                int[] endLocations = new int[2];
                end.getLocationOnScreen(endLocations);
                int endX = endLocations[0] + end.getWidth() / 2;
                int endY = endLocations[1] + end.getHeight() / 2;

                BezierTranslateAnimation bezierTranslateAnimation = new BezierTranslateAnimation(0, endX - startX, 0, endY - startY, endX - startX, -200);
                bezierTranslateAnimation.setFillAfter(true);
                bezierTranslateAnimation.setDuration(600);
                bezierTranslateAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        ++countAnimation;
                        if (countAnimation == 3) {
                            if (_asyncTaskCallBack != null)
                                _asyncTaskCallBack.onFinished();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                start.startAnimation(bezierTranslateAnimation);
            }
        });
    }
}
