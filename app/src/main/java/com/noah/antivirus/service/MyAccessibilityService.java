package com.noah.antivirus.service;

import android.accessibilityservice.AccessibilityService;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.noah.antivirus.R;
import com.noah.antivirus.base.BaseNavigationDrawerActivity;
import com.noah.antivirus.fragment.PhoneBoostFragment;
import com.noah.antivirus.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by phamtruong on 10/4/16.
 */

public class MyAccessibilityService extends AccessibilityService {
    public static final String TAG = "MyAccessibilityService";

    public static final String BROADCAST_FORCE_STOP = "BROADCAST_FORCE_STOP";
    public static final String FORCE_STOP = "FORCE_STOP";
    public static final String BROADCAST_BACK_TAP = "BROADCAST_BACK_TAP";
    private boolean force_stop;
    Handler handler = new Handler();

    private int countStep;

    private BroadcastReceiver forceStopBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            force_stop = intent.getBooleanExtra(FORCE_STOP, false);
        }
    };

    private BroadcastReceiver backTapBroadcast = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onReceive(Context context, Intent intent) {
            performGlobalAction(GLOBAL_ACTION_BACK);
            performGlobalAction(GLOBAL_ACTION_BACK);

        }
    };

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();

        autoBoostDevice();

        registerReceiver(forceStopBroadcast, new IntentFilter(BROADCAST_FORCE_STOP));
        registerReceiver(backTapBroadcast, new IntentFilter(BROADCAST_BACK_TAP));
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    public void onAccessibilityEvent(final AccessibilityEvent event) {
        if (force_stop) {
            // Disable LockService when boots
            if (Utils.isServiceRunning(this, LockService.class)) {
                stopService(new Intent(this, LockService.class));
            }

            if (event.getSource() != null) {
                Log.d(TAG, "enter accessibility event " + event.getPackageName() + " " + event.getClassName() + " " + event.getText().toString());
                if (event.getPackageName().equals("com.android.settings")) {
                    final AccessibilityNodeInfo accessibilityNodeInfo = event.getSource();

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            List<AccessibilityNodeInfo> stop_nodes = getNodeForceStopInfo(accessibilityNodeInfo);
                            if (stop_nodes != null && !stop_nodes.isEmpty()) {
                                AccessibilityNodeInfo node;
                                for (int i = 0; i < stop_nodes.size(); i++) {
                                    node = stop_nodes.get(i);
                                    if (node.getClassName().equals("android.widget.Button")) {
                                        if (node.isEnabled()) {
                                            if (countStep >= 2) {
                                                countStep = 0;
                                                Log.d(TAG, "perform back");
                                                performGlobalAction(GLOBAL_ACTION_BACK);
                                                sendBroadcast(new Intent().setAction(BoosterService.BROADCAST_ACCESSIBILITY).putExtra(BoosterService.STOPPED, true));
                                            } else {
                                                Log.d(TAG, "perform click force stop");
                                                node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                                ++countStep;
                                            }
                                        } else {
                                            countStep = 0;
                                            Log.d(TAG, "perform back 2");
                                            performGlobalAction(GLOBAL_ACTION_BACK);
                                            sendBroadcast(new Intent().setAction(BoosterService.BROADCAST_ACCESSIBILITY).putExtra(BoosterService.STOPPED, true));
                                        }
                                        node.recycle();
                                    }
                                }
                            }
                        }
                    }, 1500);

                    List<AccessibilityNodeInfo> ok_nodes = null;
                    if (event.getText() != null && event.getText().size() == 4) {
                        ok_nodes = event.getSource().findAccessibilityNodeInfosByText(event.getText().get(3).toString());
                    }
                    if (ok_nodes != null && !ok_nodes.isEmpty()) {
                        AccessibilityNodeInfo node;
                        for (int i = 0; i < ok_nodes.size(); i++) {
                            node = ok_nodes.get(i);
                            if (node.getClassName().equals("android.widget.Button")) {
                                Log.d(TAG, "perform click ok");
                                node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                ++countStep;
                            }
                            node.recycle();
                        }
                    }
                } else {  // Kiểm tra activity khác setting bật lên trong quá trình boost (click back nếu có để thoát)
                    if (!event.getPackageName().equals(getPackageName()) && !event.getPackageName().equals("com.android.systemui")) {
                        performGlobalAction(GLOBAL_ACTION_BACK);
                        sendBroadcast(new Intent().setAction(BoosterService.BROADCAST_ACCESSIBILITY).putExtra(BoosterService.STOPPED, true));
                    }
                }
            }
        }
    }

    @Override
    public void onInterrupt() {

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected boolean onKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_HOME && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (Utils.isServiceRunning(this, BoosterService.class)) {
                stopService(new Intent(this, BoosterService.class));
            }

            if (Utils.isServiceRunning(this, LockService.class)) {
                startActivity(Utils.getHomeIntent());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        stopService(new Intent(MyAccessibilityService.this, LockService.class));
                    }
                }, 500);
            }
        }

        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (Utils.isServiceRunning(this, LockService.class)) {
                startActivity(Utils.getHomeIntent());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        stopService(new Intent(MyAccessibilityService.this, LockService.class));
                    }
                }, 500);
            }
        }

        if (event.getKeyCode() == KeyEvent.KEYCODE_APP_SWITCH && event.getAction() == KeyEvent.ACTION_DOWN) {

            if (Utils.isServiceRunning(this, LockService.class)) {
                stopService(new Intent(MyAccessibilityService.this, LockService.class));
            }
        }

        return super.onKeyEvent(event);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(forceStopBroadcast);
        unregisterReceiver(backTapBroadcast);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    private List<AccessibilityNodeInfo> getNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        List<AccessibilityNodeInfo> stop_nodes;
        stop_nodes = accessibilityNodeInfo.findAccessibilityNodeInfosByText(getString(R.string.force_stop));
        if (stop_nodes.size() == 0)
            stop_nodes = accessibilityNodeInfo.findAccessibilityNodeInfosByText("Buộc đóng");
        if (stop_nodes.size() == 0)
            stop_nodes = accessibilityNodeInfo.findAccessibilityNodeInfosByViewId("com.android.settings:id/left_button");
        if (stop_nodes.size() == 0)
            stop_nodes = accessibilityNodeInfo.findAccessibilityNodeInfosByViewId("com.android.settings:id/force_stop_button");
        if (stop_nodes.size() == 0)
            stop_nodes = accessibilityNodeInfo.findAccessibilityNodeInfosByViewId("miui:id/v5_icon_menu_bar_primary_item");
        if (stop_nodes.size() == 0)
            stop_nodes = accessibilityNodeInfo.findAccessibilityNodeInfosByViewId("com.zui.appsmanager:id/force_stop");
        return stop_nodes;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void autoBoostDevice() {
        if (Utils.isServiceRunning(getApplicationContext(), BoosterService.class)) {
            Log.d(TAG, "advanced service is running");
            sendBroadcast(new Intent().setAction(PhoneBoostFragment.BROADCAST_ALLOW_ACCESSIBILITY));

            performGlobalAction(GLOBAL_ACTION_BACK);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    performGlobalAction(GLOBAL_ACTION_BACK);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent autoBoostIntent = new Intent();
                            autoBoostIntent.putExtra(PhoneBoostFragment.SHOULD_BOOST, true);
                            autoBoostIntent.setAction(PhoneBoostFragment.BROADCAST_ALLOW_ACCESSIBILITY);
                            sendBroadcast(autoBoostIntent);
                        }
                    }, 500);
                }
            }, 1000);

            return;
        }

        // Applock
        sendBroadcast(new Intent().setAction(PhoneBoostFragment.BROADCAST_ALLOW_ACCESSIBILITY));
        performGlobalAction(GLOBAL_ACTION_BACK);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                performGlobalAction(GLOBAL_ACTION_BACK);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent autoBoostIntent = new Intent();
                        autoBoostIntent.putExtra(BaseNavigationDrawerActivity.SHOULD_CREATE_PASS, true);
                        autoBoostIntent.setAction(PhoneBoostFragment.BROADCAST_ALLOW_ACCESSIBILITY);
                        sendBroadcast(autoBoostIntent);
                    }
                }, 500);
            }
        }, 1000);
    }

    /**
     * find the force stop button in app detail activity
     *
     * @param accessibilityNodeInfo
     * @return
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private List<AccessibilityNodeInfo> getNodeForceStopInfo(AccessibilityNodeInfo
                                                                     accessibilityNodeInfo) {
        List<AccessibilityNodeInfo> stop_nodes = new ArrayList<>();

        String a = m12129a(getApplicationContext(), "force_stop");
        if (!a.equals("")) {
            stop_nodes = accessibilityNodeInfo.findAccessibilityNodeInfosByText(a);
        }

        if (stop_nodes.size() == 0) {
            a = m12129a(getApplicationContext(), "finish_application");
            if (!a.equals("")) {
                stop_nodes = accessibilityNodeInfo.findAccessibilityNodeInfosByViewId(a);
            }
        }
        if (stop_nodes.size() == 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                stop_nodes = accessibilityNodeInfo.findAccessibilityNodeInfosByViewId("com.android.settings:id/right_button");
            else
                stop_nodes = accessibilityNodeInfo.findAccessibilityNodeInfosByViewId("com.android.settings:id/left_button");
        }
        if (stop_nodes.size() == 0)
            stop_nodes = accessibilityNodeInfo.findAccessibilityNodeInfosByViewId("com.android.settings:id/force_stop_button");
        if (stop_nodes.size() == 0)
            stop_nodes = accessibilityNodeInfo.findAccessibilityNodeInfosByViewId("miui:id/v5_icon_menu_bar_primary_item");
        if (stop_nodes.size() == 0)
            stop_nodes = accessibilityNodeInfo.findAccessibilityNodeInfosByViewId("com.zui.appsmanager:id/force_stop");

        return stop_nodes;
    }

    public static String m12129a(Context context, String str) {
        String str2 = "";
        try {
            ComponentName b = resolveComponentFromSettings(context);
            Resources resourcesForApplication = context.getPackageManager().getResourcesForApplication(b.getPackageName());
            int identifier = resourcesForApplication.getIdentifier(str, "string", b.getPackageName());
            if (identifier != 0) {
                str2 = resourcesForApplication.getString(identifier);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str2;
    }

    public static ComponentName resolveComponentFromSettings(Context context) {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", context.getPackageName(), null));
        return resolveComponent(context, intent);
    }

    public static ComponentName resolveComponent(Context context, Intent intent) {
        PackageManager packageManager = context.getPackageManager();
        ComponentName resolveActivity = intent.resolveActivity(packageManager);
        if (resolveActivity == null) {
            return null;
        }
        try {
            ActivityInfo activityInfo = packageManager.getActivityInfo(resolveActivity, 0);
            return activityInfo.targetActivity != null ? new ComponentName(resolveActivity.getPackageName(), activityInfo.targetActivity) : resolveActivity;
        } catch (PackageManager.NameNotFoundException e) {
            return resolveActivity;
        }
    }
}
