package com.noah.antivirus.service;

import android.app.Dialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.activities.AppLockCreatePasswordActivity;
import com.noah.antivirus.activities.AppLockForgotPasswordActivity;
import com.noah.antivirus.activities.AppLockImageActivity;
import com.noah.antivirus.activities.AppLockSettingsActivity;
import com.noah.antivirus.databases.ImagesDatabaseHelper;
import com.noah.antivirus.model.Image;
import com.noah.antivirus.model.Selfie;
import com.noah.antivirus.util.HomeWatcher;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.util.Utils;
import com.takwolf.android.lock9.Lock9View;

public class LockService extends Service {
    public static final String ACTION_APPLICATION_PASSED = "com.star.applock.applicationpassedtest";
    public static final int NOTIFICATION_ID_APP_LOCK = 1111;
    public static final String KEY_THIEVES = "thieves";
    public static final String KEY_APP_THIEVES = "app_thieves";
    public static final String KEY_UNLOCKED = "unlocked";

    private WindowManager windowManager;
    private View view;
    private ImageView img_app_icon;
    private TextView tv_app_name;

    private String pakageName;
    private int countFailed;
    private SharedPreferences sharedPreferences;
    private ImagesDatabaseHelper imagesDatabaseHelper;

    private HomeWatcher mHomeWatcher;

    public LockService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Bắt sự kiện Home và recents
        mHomeWatcher = new HomeWatcher(this);
        mHomeWatcher.setOnHomePressedListener(new HomeWatcher.OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                if (Utils.isServiceRunning(LockService.this, LockService.class)) {
                    stopSelf();
                }
            }

            @Override
            public void onHomeLongPressed() {
                if (Utils.isServiceRunning(LockService.this, LockService.class)) {
                    stopSelf();
                }
            }
        });
        mHomeWatcher.startWatch();

        sharedPreferences = getSharedPreferences(AppLockCreatePasswordActivity.SHARED_PREFERENCES_NAME, 0);
        imagesDatabaseHelper = ImagesDatabaseHelper.getInstance(this);
        windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams params;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    PixelFormat.TRANSLUCENT);
        } else {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
        }
        params.gravity = Gravity.TOP;
        params.screenOrientation = Configuration.ORIENTATION_PORTRAIT;

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.lock_view, null);

        ImageView img_close = (ImageView) view.findViewById(R.id.img_close);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(Utils.getHomeIntent());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        LockService.this.stopSelf();
                    }
                }, 500);
            }
        });
        img_app_icon = (ImageView) view.findViewById(R.id.img_app_icon);
        tv_app_name = (TextView) view.findViewById(R.id.tv_app_name);
        TypeFaceUttils.setNomal(this, tv_app_name);

        Lock9View lock_view = (Lock9View) view.findViewById(R.id.lock_view);
        Lock9View lock_view_disvibrate = (Lock9View) view.findViewById(R.id.lock_view_disvibrate);
        if (sharedPreferences.getBoolean(AppLockSettingsActivity.KEY_VIBRATE, false)) {
            lock_view.setVisibility(View.VISIBLE);
            lock_view_disvibrate.setVisibility(View.GONE);
        } else {
            lock_view.setVisibility(View.GONE);
            lock_view_disvibrate.setVisibility(View.VISIBLE);
        }

        lock_view.setCallBack(new Lock9View.CallBack() {
            @Override
            public void onFinish(String password) {
                finish(password);
            }
        });

        lock_view_disvibrate.setCallBack(new Lock9View.CallBack() {
            @Override
            public void onFinish(String password) {
                finish(password);
            }
        });

        windowManager.addView(view, params);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        pakageName = intent.getStringExtra("packageName");
        img_app_icon.setImageDrawable(Utils.getIconFromPackage(pakageName, this));
        tv_app_name.setText(Utils.getAppNameFromPackage(this, pakageName));
        return START_NOT_STICKY;
    }

    private void finish(String password) {
        if (password.equals(sharedPreferences.getString(AppLockCreatePasswordActivity.KEY_PASSWORD, null))) {
            sendBroadcast(new Intent().setAction(ACTION_APPLICATION_PASSED).putExtra("packageName", pakageName));

            // Push notification
            if (sharedPreferences.getBoolean(KEY_THIEVES, false)) {
                Image image = imagesDatabaseHelper.findByID(sharedPreferences.getLong(KEY_APP_THIEVES, -1));
                if (image != null) {
                    Intent intent = new Intent(this, AppLockImageActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("id", image.getId());
                    Utils.notificateAppLock(this, NOTIFICATION_ID_APP_LOCK, R.mipmap.ic_thieves, getResources().getString(R.string.someone_tries_to_open_your_app), image.getAppName(), getResources().getString(R.string.someone_tries_to_open_your_app), intent);
                }
            }
            sharedPreferences.edit().putBoolean(KEY_THIEVES, false).apply();

            if (sharedPreferences.getBoolean(AppLockCreatePasswordActivity.KEY_RELOCK_POLICY, false)) {
                sharedPreferences.edit().putBoolean(KEY_UNLOCKED, true).apply();
            }

            stopSelf();
        } else {
            ++countFailed;
            if (countFailed == 3) {
                if (sharedPreferences.getBoolean(AppLockSettingsActivity.KEY_SELFIE, false)) {
                    (new Selfie(this, pakageName)).takePhoto();
                }
            }

            final Dialog dialog = new Dialog(this, R.style.MaterialDialogSheet);
            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
            dialog.setContentView(R.layout.snackbar_view);
            dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            TextView tv_decription = (TextView) dialog.findViewById(R.id.tv_decription);
            TextView tv_forgot_password = (TextView) dialog.findViewById(R.id.tv_forgot_password);
            TypeFaceUttils.setNomal(this, tv_decription);
            TypeFaceUttils.setNomal(this, tv_forgot_password);
            tv_forgot_password.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(Utils.getHomeIntent());
                    Intent intent = new Intent(LockService.this, AppLockForgotPasswordActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    dialog.dismiss();
                    stopSelf();
                }
            });
            dialog.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            }, 1500);
        }
    }


    @Override
    public void onDestroy() {
        mHomeWatcher.stopWatch();
        super.onDestroy();
        try {
            if (view != null) windowManager.removeView(view);
        } catch (Exception ignored) {

        }
    }
}
