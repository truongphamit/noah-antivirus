package com.noah.antivirus.whypay.Event;

/**
 * Created by Hungpq on 4/18/17.
 */

public class ConfirmSmsCancelEvent {
    private String address;
    private String msgBody;

    public ConfirmSmsCancelEvent(String address, String msgBody) {
        this.address = address;
        this.msgBody = msgBody;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMsgBody() {
        return msgBody;
    }

    public void setMsgBody(String msgBody) {
        this.msgBody = msgBody;
    }
}
