package com.noah.antivirus.whypay.Model;

/**
 * Created by Hungpq on 4/13/17.
 */

public class SimInfo {
    private int id_;
    private String display_name;
    private String icc_id;
    private int slot;
    private String carrierName;
    private String number;
    private String sim_id;
    private String mcc;
    private String mnc;

    public SimInfo(int id_, String display_name, String icc_id, String carrierName, String number, String sim_id, String mcc, String mnc) {
        this.id_ = id_;
        this.display_name = display_name;
        this.icc_id = icc_id;
        this.carrierName = carrierName;
        this.number = number;
        this.sim_id = sim_id;
        this.mcc = mcc;
        this.mnc = mnc;
    }

    public SimInfo(int id_, String display_name, String icc_id, int slot) {
        this.id_ = id_;
        this.display_name = display_name;
        this.icc_id = icc_id;
        this.slot = slot;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getMnc() {
        return mnc;
    }

    public void setMnc(String mnc) {
        this.mnc = mnc;
    }

    public int getId_() {
        return id_;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public String getIcc_id() {
        return icc_id;
    }

    public int getSlot() {
        return slot;
    }

    public void setId_(int id_) {
        this.id_ = id_;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public void setIcc_id(String icc_id) {
        this.icc_id = icc_id;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSim_id() {
        return sim_id;
    }

    public void setSim_id(String sim_id) {
        this.sim_id = sim_id;
    }

    @Override
    public String toString() {
        return "SimInfo{" +
                "id_=" + id_ +
                ", display_name='" + display_name + '\'' +
                ", icc_id='" + icc_id + '\'' +
                ", slot=" + slot + '\'' +
                ", sim_id=" + sim_id + '\'' +
                ", number=" + number + '\'' +
                ", carrier_name=" + carrierName + '\'' +
                ", mcc=" + mcc + " , mnc=" + mnc +
                '}';
    }
}