package com.noah.antivirus.whypay.Activity;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.telephony.SmsManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.noah.antivirus.R;
import com.noah.antivirus.whypay.Activity.BaseActivity.BaseToolbarVASActivity;
import com.noah.antivirus.whypay.Constant.Constant;
import com.noah.antivirus.whypay.Dialog.DialogContentCancel;
import com.noah.antivirus.whypay.Event.AgreeSendSmsEvent;
import com.noah.antivirus.whypay.Event.ConfirmSmsCancelEvent;
import com.noah.antivirus.whypay.Utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Timer;
import java.util.TimerTask;

public class VasCheckerActivity extends BaseToolbarVASActivity {
    public String TAG = "MainActivity";

    public static String SMS_SENT = "SMS_SENT";
    public static String SMS_DELIVERED = "SMS_DELI";
    public static final int REQUEST_PERMISSION = 4;

    private SharedPreferences sharedPreferences;

    private int telecomType;

    private ImageView imgSimType, imgChangeSimType;
    private Button btnCheckVas;

    private Handler handlerProcess;
    private Timer timerProcess;

    @Override
    public int getLayoutId() {
        return R.layout.activity_vas_checker;
    }

    @Override
    public String getToolbarText() {
        return "Kiểm tra dịch vụ trừ tiền";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getSharedPreferences(Constant.sharePreference, MODE_PRIVATE);

        initDraw();

    }

    @Override
    protected void onStart() {
        Log.d(TAG, "calling on start");
        super.onStart();
        EventBus.getDefault().register(this);
        checkCurrentStateProcess();
        getCurrentSimType();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private void initDraw() {

        imgSimType = (ImageView) findViewById(R.id.img_sim_type);
        btnCheckVas = (Button) findViewById(R.id.btn_check_vas);
        imgChangeSimType = (ImageView) findViewById(R.id.img_change_sim_type);

        imgChangeSimType.setVisibility(View.GONE);

        btnCheckVas.setOnClickListener(checkVas);
    }

    private void checkCurrentStateProcess() {
        boolean isProcessing = sharedPreferences.getBoolean(Constant.sharePreference_key_processing, false);
        long lastBoostedTime = sharedPreferences.getLong(Constant.sharePreference_key_time_sent, 0);
        long currentTime = System.currentTimeMillis();

        if ((currentTime - lastBoostedTime) < Constant.time_to_wait_sms && lastBoostedTime != 0 && isProcessing) {
            btnCheckVas.setText("Đang kiểm tra .");

            timerProcess = new Timer();
            timerProcess.schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (btnCheckVas.getText().equals("Đang kiểm tra ."))
                                btnCheckVas.setText("Đang kiểm tra ..");
                            else if (btnCheckVas.getText().equals("Đang kiểm tra .."))
                                btnCheckVas.setText("Đang kiểm tra ...");
                            else if (btnCheckVas.getText().equals("Đang kiểm tra ..."))
                                btnCheckVas.setText("Đang kiểm tra .");
                        }
                    });
                }
            }, 0, 800);

            btnCheckVas.setOnClickListener(null);
        } else {
            if (isProcessing)
                sharedPreferences.edit().putBoolean(Constant.sharePreference_key_processing, false).commit();
            btnCheckVas.setText("Kiểm tra ngay");
            if (timerProcess != null) timerProcess.cancel();
            btnCheckVas.setOnClickListener(checkVas);
        }
    }

    private View.OnClickListener checkVas = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (!sharedPreferences.getBoolean(Constant.sharePreference_key_agree_send_sms, false)) {
                Utils.showAgreeSendSmsDialog(VasCheckerActivity.this);
            } else if (!Utils.checkPermission(VasCheckerActivity.this, REQUEST_PERMISSION)) {

            } else {
                btnCheckVas.setOnClickListener(null);
                checkVas();
            }
        }
    };

    private void checkVas() {
        telecomType = sharedPreferences.getInt(Constant.sharePreference_key_telecom_type, 0);

        if (telecomType == Constant.telecom_type_vinaphone)
            sendSms(Constant.address_vinaphone_check_vas, Constant.key_word_vinaphone_check_vas, false);
        else if (telecomType == Constant.telecom_type_viettel)
            sendSms(Constant.address_viettel_check_vas, Constant.key_word_viettel_check_vas, false);
        else if (telecomType == Constant.telecom_type_mobifone)
            sendSms(Constant.address_mobifone_check_vas, Constant.key_word_mobifone_check_vas, false);

        registerReceiver(receiver, new IntentFilter(SMS_SENT));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    checkVas();
                } else {
                    finish();
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAgreeSendSmsEvent(AgreeSendSmsEvent event) {
        if (!sharedPreferences.getBoolean(Constant.sharePreference_key_agree_send_sms, false)) {
            Utils.showAgreeSendSmsDialog(VasCheckerActivity.this);
        } else if (!Utils.checkPermission(VasCheckerActivity.this, REQUEST_PERMISSION)) {

        } else {
            checkVas();
        }
    }

    @Subscribe
    public void onSmsCancelConfirmEvent(ConfirmSmsCancelEvent event) {
        DialogContentCancel dialog = new DialogContentCancel(VasCheckerActivity.this, event.getMsgBody(), event.getAddress());
        dialog.show();
    }

    private void getCurrentSimType() {
        telecomType = Utils.checkSimType(this);
        if (telecomType == Constant.telecom_type_vinaphone)
            imgSimType.setImageResource(R.drawable.logo_vina);
        else if (telecomType == Constant.telecom_type_viettel)
            imgSimType.setImageResource(R.drawable.logo_vt);
        else if (telecomType == Constant.telecom_type_mobifone)
            imgSimType.setImageResource(R.drawable.logo_mobi);
        else {
            imgSimType.setVisibility(View.GONE);
            btnCheckVas.setVisibility(View.GONE);
        }
    }


    private void sendSms(String phonenumber, String message, boolean isBinary) {
        Utils.sendSms(this, phonenumber, message, isBinary, true);

        long currentTime = System.currentTimeMillis();
        sharedPreferences.edit().putLong(Constant.sharePreference_key_time_sent, currentTime).commit();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String message = null;

            switch (getResultCode()) {
                case Activity.RESULT_OK:

                    message = "Message sent!";
                    sharedPreferences.edit().putBoolean(Constant.sharePreference_key_processing, true).commit();
                    checkCurrentStateProcess();
                    break;

                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    message = "Error. Message not sent.";
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    message = "Error: No service.";
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    message = "Error: Null PDU.";
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    message = "Error: Radio off.";
                    Toast.makeText(getApplicationContext(), "Kiểm tra thất bại. Vui lòng thử lại!", Toast.LENGTH_LONG);
                    btnCheckVas.setOnClickListener(checkVas);

                    break;
            }
            Log.d(TAG, "message: " + message);
        }
    };

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(receiver);
        } catch (Exception ignored) {
        }
        super.onDestroy();
    }
}
