package com.noah.antivirus.whypay.Model;

import java.io.Serializable;

/**
 * Created by Hungpq on 4/10/17.
 */

public class VASItem implements Serializable {

    private String title;
    private String description;
    private String address;
    private String formatCancel;
    private boolean isSendSmsCancel;

    public VASItem(String title, String description, String address, String formatCancel) {
        this.title = title;
        this.description = description;
        this.address = address;
        this.formatCancel = formatCancel;
        this.isSendSmsCancel = false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFormatCancel() {
        return formatCancel;
    }

    public void setFormatCancel(String formatCancel) {
        this.formatCancel = formatCancel;
    }

    public boolean isSendSmsCancel() {
        return isSendSmsCancel;
    }

    public void setSendSmsCancel(boolean sendSmsCancel) {
        isSendSmsCancel = sendSmsCancel;
    }

    public String toString() {
        String string = title + " " + formatCancel + " " + address;
        return string;
    }
}
