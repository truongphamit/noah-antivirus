package com.noah.antivirus.whypay.Notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.noah.antivirus.R;
import com.noah.antivirus.whypay.Activity.VASResultActivity;
import com.noah.antivirus.whypay.Model.ResultVas;
import com.noah.antivirus.whypay.Utils.Utils;


/**
 * Created by Hungpq on 4/11/17.
 */

public class ResultNotification extends NotificationCompat.Builder {
    public static String VAS_RESULT = "VAS_RESULT";
    public static int NOTIFICATION_RESULT_ID = 0;
    private Context context;

    /**
     * Constructor.
     * <p/>
     * Automatically sets the when field to {@link System#currentTimeMillis()
     * System.currentTimeMillis()} and the audio stream to the
     * {@link Notification#STREAM_DEFAULT}.
     *
     * @param context A {@link Context} that will be used to construct the
     *                RemoteViews. The Context will not be held past the lifetime of this
     *                Builder object.
     */
    public ResultNotification(Context context, ResultVas vasItem) {
        super(context);
        this.context = context;
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.notification_result_vas);

        Intent intent = new Intent(context, VASResultActivity.class);
        intent.putExtra(VAS_RESULT, vasItem);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        this.setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setCustomContentView(remoteViews)
                .setLights(Color.parseColor("#2979FF"), 600, 4000);

        remoteViews.setImageViewResource(R.id.img_icon_launcher, R.mipmap.ic_launcher);
        remoteViews.setImageViewBitmap(R.id.txt_notification, buildTextNoti());

    }

    /**
     * draw text percent information in noti by bitmap
     */
    public Bitmap buildTextNoti() {
        int height = (int) Utils.convertDpToPixel(45, context);
        int width = (int) Utils.convertDpToPixel(300, context);
        int textSize = (int) Utils.convertSpToPixel(15, context);

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        Paint paint = new Paint();
        Typeface clock = Typeface.createFromAsset(context.getAssets(), "fonts/UTM Avo.ttf");
        paint.setTypeface(clock);
        paint.setAntiAlias(true);
        paint.setSubpixelText(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(context.getResources().getColor(R.color.black));

        paint.setTextSize(textSize);
        /**
         * the rectangle of the text paint
         */
        Rect rectangle = new Rect();
        String text1, text2;
        text1 = "Kết quả xử lý quét các dịch vụ";

        paint.getTextBounds(text1, 0, text1.length(), rectangle);

        int centerHeight = height / 2;
        canvas.drawText(text1, 0, centerHeight + rectangle.height() / 2, paint);

        return bitmap;
    }
}
