package com.noah.antivirus.whypay.Event;

import android.content.Intent;

import java.util.ArrayList;

/**
 * Created by Hungpq on 4/15/17.
 */

public class SendSmsCancelEvent {
    int posCancel;

    public SendSmsCancelEvent(int posCancel) {
        this.posCancel = posCancel;
    }

    public int getPosCancel() {
        return posCancel;
    }

    public void setPosCancel(int posCancel) {
        this.posCancel = posCancel;
    }
}
