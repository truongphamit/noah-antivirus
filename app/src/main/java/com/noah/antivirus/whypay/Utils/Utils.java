package com.noah.antivirus.whypay.Utils;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;

import com.noah.antivirus.whypay.Activity.VASResultActivity;
import com.noah.antivirus.whypay.Activity.VasCheckerActivity;
import com.noah.antivirus.whypay.Constant.Constant;
import com.noah.antivirus.whypay.Dialog.DialogAlertSendSms;
import com.noah.antivirus.whypay.Dialog.SimTypePicker;
import com.noah.antivirus.whypay.Model.SimInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Hungpq on 4/11/17.
 */

public class Utils {
    public static String TAG = "Utils";

    public static void showPickSimTYpeDialog(Activity activity) {
        SimTypePicker simTypePicker = new SimTypePicker(activity);
        simTypePicker.show();
    }

    public static void showAgreeSendSmsDialog(Activity activity) {
        DialogAlertSendSms alertSendSms = new DialogAlertSendSms(activity);
        alertSendSms.show();
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * convert device specific pixels to density independent pixels.
     *
     * @param context
     * @param sp
     * @return
     */
    public static float convertSpToPixel(float sp, Context context) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return sp * scaledDensity;
    }

    public static void sendSms(Activity activity, String phonenumber, String message, boolean isBinary, boolean isCheckVas) {
        SmsManager manager = SmsManager.getDefault();
        PendingIntent piSend = null;
        PendingIntent piDelivered = null;

        if (isCheckVas) {
            piSend = PendingIntent.getBroadcast(activity, 0, new Intent(VasCheckerActivity.SMS_SENT), 0);
            piDelivered = PendingIntent.getBroadcast(activity, 0, new Intent(VasCheckerActivity.SMS_DELIVERED), 0);
        } else {
            piSend = PendingIntent.getBroadcast(activity, 0, new Intent(VASResultActivity.SMS_SENT_CANCEL), 0);
            piDelivered = PendingIntent.getBroadcast(activity, 0, new Intent(VASResultActivity.SMS_DELIVERED_CANCEL), 0);
        }

        if (isBinary) {
            byte[] data = new byte[message.length()];

            for (int index = 0; index < message.length() && index < 160; ++index) {
                data[index] = (byte) message.charAt(index);
            }

            manager.sendDataMessage(phonenumber, null, (short) 0, data, piSend, piDelivered);
        } else {
            int length = message.length();

            if (length > 160) {
                ArrayList<String> messagelist = manager.divideMessage(message);

                manager.sendMultipartTextMessage(phonenumber, null, messagelist, null, null);
            } else {
                manager.sendTextMessage(phonenumber, null, message, piSend, piDelivered);
            }
        }
    }

    public static void deleteSms(Context activity) {
        try {
            int smsCarrierType = activity.getSharedPreferences(Constant.sharePreference, Context.MODE_PRIVATE).getInt(Constant.sharePreference_key_telecom_type, 99);
            String addressSent = "";
            if (smsCarrierType == Constant.telecom_type_vinaphone) {
                addressSent = Constant.address_vinaphone_check_vas;
            } else if (smsCarrierType == Constant.telecom_type_viettel) {
                addressSent = Constant.address_viettel_check_vas;
            } else if (smsCarrierType == Constant.telecom_type_mobifone) {
                addressSent = Constant.address_mobifone_check_vas;
            } else return;

            Uri uriSms = Uri.parse("content://sms/inbox");
            Cursor c = activity.getContentResolver().query(uriSms,
                    new String[]{"_id", "address", "date", "body"}, null, null, null);

            if (c != null && c.moveToFirst()) {
                do {
                    long id = c.getLong(0);
                    String address = c.getString(1);
                    String body = c.getString(3);

                    if (addressSent.equals(address)) {
                        Log.d(TAG, "delete msg: " + body);
                        activity.getContentResolver().delete(
                                Uri.parse("content://sms/" + id), null, null);

                        break;
                    }
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<SimInfo> getSIMInfo(Context context) {

        List<SimInfo> simInfoList = new ArrayList<>();
        Uri URI_TELEPHONY = Uri.parse("content://telephony/siminfo/");
        Cursor c = context.getContentResolver().query(URI_TELEPHONY, null, null, null, null);
        for (int i = 0; i < 14; i++) {
            Log.d(TAG, c.getColumnName(i));
        }
        if (c.moveToFirst()) {
            do {
                int id = c.getInt(c.getColumnIndex("_id"));
                String simID = c.getString(c.getColumnIndex("sim_id"));
                String icc_id = c.getString(c.getColumnIndex("icc_id"));
                String display_name = c.getString(c.getColumnIndex("display_name"));
                String number = c.getString(c.getColumnIndex("number"));
                String carrier = c.getString(c.getColumnIndex("carrier_name"));
                String mcc = c.getString(c.getColumnIndex("mcc"));
                String mnc = c.getString(c.getColumnIndex("mnc"));

                SimInfo simInfo = new SimInfo(id, display_name, icc_id, carrier, number, simID, mcc, mnc);
                Log.d(TAG, simInfo.toString());
                simInfoList.add(simInfo);

            } while (c.moveToNext());
        }
        c.close();

        return simInfoList;
    }

    public static int checkSimType(Context context) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String carrierName = manager.getNetworkOperatorName();
        Log.d(TAG, "get carrier name: " + carrierName);

        if (carrierName.trim().toLowerCase().contains("vinaphone")) {
            context.getSharedPreferences(Constant.sharePreference, Context.MODE_PRIVATE).edit().putInt(Constant.sharePreference_key_telecom_type, Constant.telecom_type_vinaphone).commit();
            return Constant.telecom_type_vinaphone;
        } else if (carrierName.trim().toLowerCase().contains("viettel")) {
            context.getSharedPreferences(Constant.sharePreference, Context.MODE_PRIVATE).edit().putInt(Constant.sharePreference_key_telecom_type, Constant.telecom_type_viettel).commit();
            return Constant.telecom_type_viettel;
        } else if (carrierName.trim().toLowerCase().contains("mobifone")) {
            context.getSharedPreferences(Constant.sharePreference, Context.MODE_PRIVATE).edit().putInt(Constant.sharePreference_key_telecom_type, Constant.telecom_type_mobifone).commit();
            return Constant.telecom_type_mobifone;
        }

        context.getSharedPreferences(Constant.sharePreference, Context.MODE_PRIVATE).edit().putInt(Constant.sharePreference_key_telecom_type, Constant.telecom_type_not_detect).commit();
        return Constant.telecom_type_not_detect;
    }

    public static boolean checkPermission(Activity activity, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            // Here, thisActivity is the current activity
            if (activity.checkSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED ||
                    activity.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE},
                        requestCode);

                return false;
            } else {
                return true;
            }
        }

        return true;
    }


}
