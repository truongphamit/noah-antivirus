package com.noah.antivirus.whypay.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.whypay.Adapter.SimTypeAdapter;
import com.noah.antivirus.whypay.Constant.Constant;
import com.noah.antivirus.whypay.Model.SimType;
import com.noah.antivirus.whypay.UIComponent.RecyclerItemClickListener;

import java.util.Arrays;

/**
 * Created by Hungpq on 4/10/17.
 */

public class SimTypePicker extends Dialog {

    private Activity activity;
    private RecyclerView recyclerView;
    private SharedPreferences sharedPreferences;

    private SimType[] LIST_SIM_TYPE = new SimType[]{
            new SimType("Vinaphone"),
            new SimType("Viettel"),
            new SimType("Mobifone")

    };

    public SimTypePicker(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_pick_sim_type);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = width / 4 * 3;
        getWindow().setAttributes(lp);

        sharedPreferences = getContext().getSharedPreferences(Constant.sharePreference, Context.MODE_PRIVATE);
        int telecomType = sharedPreferences.getInt(Constant.sharePreference_key_telecom_type, 0);
        LIST_SIM_TYPE[telecomType].setChosen(true);

        recyclerView = (RecyclerView) findViewById(R.id.sim_type_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));

        final SimTypeAdapter adapter = new SimTypeAdapter(Arrays.asList(LIST_SIM_TYPE));
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (position) {
                    case 0: {
                        changSimType(Constant.telecom_type_vinaphone);
                        ((ImageView) activity.findViewById(R.id.img_sim_type)).setImageResource(R.drawable.logo_vina);
                        break;
                    }
                    case 1: {
                        changSimType(Constant.telecom_type_viettel);
                        ((ImageView) activity.findViewById(R.id.img_sim_type)).setImageResource(R.drawable.logo_vt);
                        break;
                    }
                    default: {
                        changSimType(Constant.telecom_type_mobifone);
                        ((ImageView) activity.findViewById(R.id.img_sim_type)).setImageResource(R.drawable.logo_mobi);
                        break;
                    }
                }
                notifyAdapterChangeAll(adapter, position);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

//        this.setCanceledOnTouchOutside(false);

    }

    public void notifyAdapterChangeAll(SimTypeAdapter adapter, int posChosen) {
        for (int i = 0; i < LIST_SIM_TYPE.length; i++) {
            if (LIST_SIM_TYPE[i].isChosen()) {
                LIST_SIM_TYPE[i].setChosen(false);
                adapter.notifyItemChanged(i);
            }
        }
        LIST_SIM_TYPE[posChosen].setChosen(true);
        adapter.notifyItemChanged(posChosen);
    }

    private void changSimType(int position) {
        sharedPreferences.edit().putInt(Constant.sharePreference_key_telecom_type, position).commit();
        sharedPreferences.edit().putBoolean(Constant.sharePreference_key_not_pick_tel_type, false).commit();
    }
}
