package com.noah.antivirus.whypay.Model;

/**
 * Created by Hungpq on 4/10/17.
 */

public class SimType {
    private String simTypeName;
    private boolean isChosen;

    public SimType(String simTypeName, boolean isChosen) {
        this.simTypeName = simTypeName;
        this.isChosen = isChosen;
    }

    public SimType(String simTypeName) {
        this.simTypeName = simTypeName;
        isChosen = false;
    }

    public boolean isChosen() {
        return isChosen;
    }

    public void setChosen(boolean chosen) {
        isChosen = chosen;
    }

    public String getSimTypeName() {
        return simTypeName;
    }

    public void setSimTypeName(String simTypeName) {
        this.simTypeName = simTypeName;
    }
}
