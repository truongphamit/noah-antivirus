package com.noah.antivirus.whypay.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.noah.antivirus.R;
import com.noah.antivirus.whypay.Adapter.ViewHolder.VASItemViewHolder;
import com.noah.antivirus.whypay.Model.VASItem;

import java.util.ArrayList;

/**
 * Created by Hungpq on 4/11/17.
 */

public class VASItemAdapter extends RecyclerView.Adapter<VASItemViewHolder> {
    private ArrayList<VASItem> vasItems;
    private Activity activity;

    public VASItemAdapter(ArrayList<VASItem> vasItems, Activity activity) {
        this.activity = activity;
        this.vasItems = vasItems;
    }

    @Override
    public VASItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_vas, parent, false);
        return new VASItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VASItemViewHolder holder, int position) {
        holder.bind(vasItems.get(position), activity, position, VASItemAdapter.this);
    }

    @Override
    public int getItemCount() {
        return vasItems.size();
    }
}
