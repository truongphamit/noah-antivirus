package com.noah.antivirus.whypay.Activity;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.noah.antivirus.R;
import com.noah.antivirus.whypay.Activity.BaseActivity.BaseToolbarVASActivity;
import com.noah.antivirus.whypay.Adapter.VASItemAdapter;
import com.noah.antivirus.whypay.Dialog.DialogContentCancel;
import com.noah.antivirus.whypay.Event.ConfirmSmsCancelEvent;
import com.noah.antivirus.whypay.Event.SendSmsCancelEvent;
import com.noah.antivirus.whypay.Model.ResultVas;
import com.noah.antivirus.whypay.Notification.ResultNotification;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

public class VASResultActivity extends BaseToolbarVASActivity {
    public String TAG = "VASResultActivity";

    public static String SMS_SENT_CANCEL = "SMS_SENT_CANCEL";
    public static String SMS_DELIVERED_CANCEL = "SMS_DELI_CANCEL";

    private ResultVas resultVas;

    private TextView txtNoVas;
    private RecyclerView recyclerView;
    private VASItemAdapter adapter;

    //list to store which position is cancel
    private ArrayList<Integer> posCancels;

    //store the last item
    int lasItemSendSmsCancel;

    @Override
    public int getLayoutId() {
        return R.layout.activity_vas_result;
    }

    @Override
    public String getToolbarText() {
        return "Các dịch vụ gây mất tiền";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerReceiver(receiver, new IntentFilter(SMS_SENT_CANCEL));
        initDraw();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    private void initDraw() {
        posCancels = new ArrayList<>();

        txtNoVas = (TextView) findViewById(R.id.txt_no_vas);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        resultVas = (ResultVas) getIntent().getSerializableExtra(ResultNotification.VAS_RESULT);

        if (resultVas == null || resultVas.getVasItems().size() == 0) {
            recyclerView.setVisibility(View.GONE);
            txtNoVas.setVisibility(View.VISIBLE);
        } else {
            txtNoVas.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            adapter = new VASItemAdapter(resultVas.getVasItems(), this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            recyclerView.setAdapter(adapter);
        }

        NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationmanager.cancel(ResultNotification.NOTIFICATION_RESULT_ID);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String message = null;

            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    message = "Message sent!";
                    if (posCancels != null && posCancels.size() > 0) {
                        int posSendSms = posCancels.get(0);

                        if (posSendSms < resultVas.getVasItems().size()) {
                            Log.d(TAG, "remove vas canceled " + posSendSms);
                            resultVas.getVasItems().remove(posSendSms);
                            adapter.notifyItemRemoved(posSendSms);
                            posCancels.remove(0);

                            for (int i = 0; i < posCancels.size(); i++) {
                                if (posCancels.get(i) > posSendSms)
                                    posCancels.set(i, posCancels.get(i) - 1);
                            }
                        }
                    }
                    break;

                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    message = "Error. Message not sent.";
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    message = "Error: No service.";
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    message = "Error: Null PDU.";
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    message = "Error: Radio off.";

                    if (posCancels != null && posCancels.size() > 0) {
                        int posSendSms = posCancels.get(0);

                        if (posSendSms < resultVas.getVasItems().size()) {
                            Toast.makeText(getApplicationContext(), "Gửi tin nhắn không thành công. Vui lòng thử lại", Toast.LENGTH_LONG).show();
                            resultVas.getVasItems().get(posSendSms).setSendSmsCancel(false);
                            adapter.notifyItemChanged(posSendSms);
                            posCancels.remove(0);
                        }
                    }
                    break;
            }
            Log.d(TAG, "message: " + message);
        }
    };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSendSmsCancelEvent(SendSmsCancelEvent event) {
        posCancels.add(event.getPosCancel());
    }

    @Subscribe
    public void onSmsCancelConfirmEvent(ConfirmSmsCancelEvent event) {
        DialogContentCancel dialog = new DialogContentCancel(VASResultActivity.this, event.getMsgBody(), event.getAddress());
        dialog.show();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(receiver);
        } catch (Exception ignored) {
        }
        super.onDestroy();
    }
}
