package com.noah.antivirus.base;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.noah.antivirus.BuildConfig;
import com.noah.antivirus.R;
import com.noah.antivirus.activities.PhoneBoostActivity;
import com.noah.antivirus.browser.MainBrowserActivity;
import com.noah.antivirus.browser.tools.StaticTools;
import com.noah.antivirus.util.AllowAccessibilityReceiverEvent;
import com.noah.antivirus.util.Utils;
import com.noah.antivirus.activities.AppLockCreatePasswordActivity;
import com.noah.antivirus.activities.AppLockScreenActivity;
import com.noah.antivirus.activities.IgnoredListActivity;
import com.noah.antivirus.activities.PhoneInfoActivity;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.whypay.Activity.VasCheckerActivity;
import com.noah.antivirus.whypay.Constant.Constant;
import com.noah.antivirus.wifisecurity.WifiSecurityActivity;

import butterknife.ButterKnife;

/**
 * Created by HuyLV-CT on 6/21/2016.
 */
public abstract class BaseNavigationDrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    //    private NavigationView nav_view;
    private DrawerLayout drawer_layout;

    public static final String BROADCAST_ALLOW_ACCESSIBILITY = "BROADCAST_ALLOW_ACCESSIBILITY";
    public static final String SHOULD_CREATE_PASS = "SHOULD_CREATE_PASS";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(R.string.app_name);
        TypeFaceUttils.setNomal(this, mTitle);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_layout.addDrawerListener(toggle);
        toggle.syncState();

        changeTextFont();
        initMenu();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(allowAccessibilityService, new IntentFilter(BROADCAST_ALLOW_ACCESSIBILITY));
    }

    private void changeTextFont() {
        TextView tvShowIgnoredList = (TextView) drawer_layout.findViewById(R.id.tv_show_ignored_list);
        TypeFaceUttils.setMenu(this, tvShowIgnoredList);
        TextView tvAutoScan = (TextView) drawer_layout.findViewById(R.id.tv_auto_scan);
        TypeFaceUttils.setMenu(this, tvAutoScan);
        TextView tvAppLock = (TextView) drawer_layout.findViewById(R.id.tv_app_lock);
        TypeFaceUttils.setMenu(this, tvAppLock);
        TextView tvPhoneInfo = (TextView) drawer_layout.findViewById(R.id.tv_phone_info);
        TypeFaceUttils.setMenu(this, tvPhoneInfo);
        TextView tvFeedback = (TextView) drawer_layout.findViewById(R.id.tv_feedback);
        TypeFaceUttils.setMenu(this, tvFeedback);
        TextView tvRateUs = (TextView) drawer_layout.findViewById(R.id.tv_rate_us);
        TypeFaceUttils.setMenu(this, tvRateUs);
        TextView tvMoreApp = (TextView) drawer_layout.findViewById(R.id.tv_more_app);
        TypeFaceUttils.setMenu(this, tvMoreApp);
        TextView tvVersion = (TextView) drawer_layout.findViewById(R.id.tv_version);
        TypeFaceUttils.setMenu(this, tvVersion);
        tvVersion.setText(getResources().getString(R.string.version) + " " + BuildConfig.VERSION_NAME);
        TextView tvBrowser = (TextView) drawer_layout.findViewById(R.id.tv_browser);
        TypeFaceUttils.setMenu(this, tvBrowser);
        TextView tvBoost = (TextView) drawer_layout.findViewById(R.id.tv_phone_boost);
        TypeFaceUttils.setMenu(this, tvBoost);
        TextView tvWifiSecurity = (TextView) drawer_layout.findViewById(R.id.tv_wifi_security);
        TypeFaceUttils.setMenu(this, tvWifiSecurity);
    }

    private void initMenu() {
        final View phoneInfo = drawer_layout.findViewById(R.id.menu_phone_info);
        phoneInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseNavigationDrawerActivity.this, PhoneInfoActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        final View showIgnoredList = drawer_layout.findViewById(R.id.menu_show_ignored_list);
        showIgnoredList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseNavigationDrawerActivity.this, IgnoredListActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        final SharedPreferences settings = getSharedPreferences("Settings", 0);
        ToggleButton toggleAutoScan = (ToggleButton) drawer_layout.findViewById(R.id.toggle_auto_scan);
        toggleAutoScan.setChecked(settings.getBoolean("auto_scan", true));
        toggleAutoScan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = settings.edit();
                if (isChecked) {
                    editor.putBoolean("auto_scan", true);
                    editor.apply();
                } else {
                    editor.putBoolean("auto_scan", false);
                    editor.apply();
                }
            }
        });

        View appLock = findViewById(R.id.menu_app_lock);
        appLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermisAppLock();
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        View rateUs = drawer_layout.findViewById(R.id.menu_rate_us);
        rateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticTools.rate(BaseNavigationDrawerActivity.this);
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        View moreApp = drawer_layout.findViewById(R.id.menu_more_app);
        moreApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticTools.moreApp(BaseNavigationDrawerActivity.this, getString(R.string.store_dev_id));
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        View feedback = drawer_layout.findViewById(R.id.menu_feedback);
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticTools.feedback(BaseNavigationDrawerActivity.this);
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        View browser = drawer_layout.findViewById(R.id.menu_browser);
        browser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseNavigationDrawerActivity.this, MainBrowserActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        drawer_layout.findViewById(R.id.menu_phone_boost).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseNavigationDrawerActivity.this, PhoneBoostActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        if (com.noah.antivirus.whypay.Utils.Utils.checkSimType(getApplicationContext()) == Constant.telecom_type_not_detect) {
            drawer_layout.findViewById(R.id.menu_vas_checker).setVisibility(View.GONE);
        } else {
            drawer_layout.findViewById(R.id.menu_vas_checker).setVisibility(View.VISIBLE);
            drawer_layout.findViewById(R.id.menu_vas_checker).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(BaseNavigationDrawerActivity.this, VasCheckerActivity.class));
                    drawer_layout.closeDrawer(GravityCompat.START);
                }
            });
        }

        drawer_layout.findViewById(R.id.menu_wifi_security).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseNavigationDrawerActivity.this, WifiSecurityActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(allowAccessibilityService);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void checkPermisAppLock() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!Utils.isUsageAccessEnabled(BaseNavigationDrawerActivity.this)) {
                Utils.openUsageAccessSetings(BaseNavigationDrawerActivity.this);
                return;
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(BaseNavigationDrawerActivity.this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                startActivity(intent);
                return;
            }
        }

        if (!Utils.isAccessibilitySettingsOn(BaseNavigationDrawerActivity.this)) {
            startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));

            Dialog dialog = new Dialog(BaseNavigationDrawerActivity.this);
            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_guide_accessibility);
            dialog.show();
            return;
        }

        SharedPreferences appLockSettings = getSharedPreferences(AppLockCreatePasswordActivity.SHARED_PREFERENCES_NAME, 0);
        if (appLockSettings.getString(AppLockCreatePasswordActivity.KEY_PASSWORD, null) != null) {
            startActivity(new Intent(BaseNavigationDrawerActivity.this, AppLockScreenActivity.class));
        } else {
            startActivity(new Intent(BaseNavigationDrawerActivity.this, AppLockCreatePasswordActivity.class));
        }
    }

    /**
     * event when user allow accessibility service and auto boost device
     */
    public BroadcastReceiver allowAccessibilityService = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.getBooleanExtra(SHOULD_CREATE_PASS, false)) {
                    SharedPreferences appLockSettings = getSharedPreferences(AppLockCreatePasswordActivity.SHARED_PREFERENCES_NAME, 0);
                    if (appLockSettings.getString(AppLockCreatePasswordActivity.KEY_PASSWORD, null) == null) {
                        startActivity(new Intent(BaseNavigationDrawerActivity.this, AppLockCreatePasswordActivity.class));
                    } else {
                        startActivity(new Intent(BaseNavigationDrawerActivity.this, AppLockScreenActivity.class));
                    }
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }
    };
}
