package com.noah.antivirus.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.util.TypeFaceUttils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by HuyLV-CT on 6/28/2016.
 */
public abstract class BaseToolbarActivity extends AppCompatActivity {

    private int layoutId;
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        TypeFaceUttils.setNomal(this, mTitle);
        mTitle.setText(R.string.app_name);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public int getLayoutId() {
        return layoutId;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }


}
