package com.noah.antivirus.browser.models;

import android.graphics.Bitmap;

/**
 * Created by Noah.TruongPQ on 12/16/2016.
 */

public class BookmarkItem {
    private String url;
    private String title;
    private Bitmap icon;

    public BookmarkItem() {

    }

    public BookmarkItem(String url, String title, Bitmap icon) {
        this.url = url;
        this.title = title;
        this.icon = icon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Bitmap getIcon() {
        return icon;
    }

    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }
}
