package com.noah.antivirus.browser.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.noah.antivirus.R;

/**
 * Created by Noah.TruongPQ on 12/21/2016.
 */

public class DialogLongPressImageLink extends Dialog implements View.OnClickListener {
    private Context context;
    private String title;
    private String url;
    private String image_url;

    private TextView tv_title;
    private TextView open_link_new_tab;
    private TextView copy_link_address;
    private TextView download_image;
    private TextView open_image_in_new_tab;

    public interface OnClickOptionListener {
        void openLinkNewTab();
        void copyLinkAddress();
        void downloadImage();
        void openImageInNewTab();
    }

    private OnClickOptionListener listener;

    public void setListener(OnClickOptionListener listener) {
        this.listener = listener;
    }

    public DialogLongPressImageLink(Context context, String url, String image_url, String title) {
        super(context);
        this.context = context;
        this.url = url;
        this.image_url = image_url;
        this.title = title;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_browser_image_link_options);

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText(url);

        open_link_new_tab = (TextView) findViewById(R.id.open_link_new_tab);
        copy_link_address = (TextView) findViewById(R.id.copy_link_address);

        open_link_new_tab.setOnClickListener(this);
        copy_link_address.setOnClickListener(this);

        download_image = (TextView) findViewById(R.id.download_image);
        open_image_in_new_tab = (TextView) findViewById(R.id.open_image_in_new_tab);

        download_image.setOnClickListener(this);
        open_image_in_new_tab.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.open_link_new_tab:
                if (listener != null) listener.openLinkNewTab();
                dismiss();
                break;
            case R.id.copy_link_address:
                if (listener != null) listener.copyLinkAddress();
                dismiss();
                break;
            case R.id.download_image:
                if (listener != null) listener.downloadImage();
                dismiss();
                break;
            case R.id.open_image_in_new_tab:
                if (listener != null) listener.openImageInNewTab();
                dismiss();
                break;
        }
    }
}
