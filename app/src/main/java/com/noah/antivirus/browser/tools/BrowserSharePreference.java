package com.noah.antivirus.browser.tools;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Noah.TruongPQ on 12/6/2016.
 */

public class BrowserSharePreference {
    private static final String NAME = "browser_settings";
    private static final String KEY_ADBLOCK = "adblock";
    private static final String KEY_NO_IMAGE = "noimage";
    private static final String KEY_NIGHT_MODE = "nightmode";
    private static final String KEY_READER_MODE_TEXT_SIZE = "reader_mode_text_size";
    private static final String KEY_READING_NIGHT_MODE = "reading_nightmode";
    private static final String KEY_ADS_BLOCK_COUNT = "ads_block_count";

    public static void setAdBlock(Context context, boolean b) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, 0);
        sharedPreferences.edit().putBoolean(KEY_ADBLOCK, b).apply();
    }

    public static boolean getAdBlock(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, 0);
        return sharedPreferences.getBoolean(KEY_ADBLOCK, false);
    }

    public static void setNoImage(Context context, boolean b) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, 0);
        sharedPreferences.edit().putBoolean(KEY_NO_IMAGE, b).apply();
    }

    public static boolean getNoImage(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, 0);
        return sharedPreferences.getBoolean(KEY_NO_IMAGE, false);
    }

    public static void setNightMode(Context context, boolean b) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, 0);
        sharedPreferences.edit().putBoolean(KEY_NIGHT_MODE, b).apply();
    }

    public static boolean getNightMode(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, 0);
        return sharedPreferences.getBoolean(KEY_NIGHT_MODE, false);
    }

    public static void setReadingTextSize(Context context, int size) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, 0);
        sharedPreferences.edit().putInt(KEY_READER_MODE_TEXT_SIZE, size).apply();
    }

    public static int getReadingTextSize(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, 0);
        return sharedPreferences.getInt(KEY_READER_MODE_TEXT_SIZE, 2);
    }

    public static void setReadingNightMode(Context context, boolean b) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, 0);
        sharedPreferences.edit().putBoolean(KEY_READING_NIGHT_MODE, b).apply();
    }

    public static boolean getReadingNightMode(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, 0);
        return sharedPreferences.getBoolean(KEY_READING_NIGHT_MODE, false);
    }

    public static void setAdsBlockCount(Context context) {
        int count = getAdsBlockCount(context);
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, 0);
        sharedPreferences.edit().putInt(KEY_ADS_BLOCK_COUNT, ++count).apply();
    }

    public static int getAdsBlockCount(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, 0);
        return sharedPreferences.getInt(KEY_ADS_BLOCK_COUNT, 0);
    }
}
