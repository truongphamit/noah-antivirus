package com.noah.antivirus.browser.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.noah.antivirus.browser.MainBrowserActivity;
import com.noah.antivirus.browser.adpaters.BookmarksAdapter;
import com.noah.antivirus.browser.models.BookmarkItem;
import com.noah.antivirus.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookmarkFragment extends Fragment implements View.OnClickListener {
    @BindView(R.id.rv_bookmarks)
    RecyclerView rv_bookmarks;

    @BindView(R.id.img_close)
    ImageView img_close;

    private MainBrowserActivity activity;
    private BookmarksAdapter adapter;

    public static BookmarkFragment newInstance() {
        return new BookmarkFragment();
    }

    public BookmarkFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainBrowserActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bookmark, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        img_close.setOnClickListener(this);
        rv_bookmarks.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new BookmarksAdapter(activity, activity.getBookmarkManager());
        rv_bookmarks.setAdapter(adapter);

        adapter.setOnItemClickListener(new BookmarksAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                activity.getSupportFragmentManager().popBackStack();
                BookmarkItem bookmarkItem = activity.getBookmarkManager().getAllBookmarks().get(position);
                activity.getFragments().get(activity.getCurrentTab()).setUrl(bookmarkItem.getUrl());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_close:
                activity.getSupportFragmentManager().popBackStack();
                break;
        }
    }
}
