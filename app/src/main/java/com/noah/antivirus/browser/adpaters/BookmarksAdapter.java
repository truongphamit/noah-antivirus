package com.noah.antivirus.browser.adpaters;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.browser.dialogs.DialogBookmarkEdit;
import com.noah.antivirus.browser.models.BookmarkItem;
import com.noah.antivirus.browser.models.BookmarkManager;
import com.noah.antivirus.R;

/**
 * Created by Noah.TruongPQ on 12/16/2016.
 */

public class BookmarksAdapter extends RecyclerView.Adapter<BookmarksAdapter.ViewHolder> {
    private Context context;
    private BookmarkManager bookmarkManager;

    public BookmarksAdapter(Context context, BookmarkManager bookmarkManager) {
        this.context = context;
        this.bookmarkManager = bookmarkManager;
    }

    // Define listener member variable
    private static OnItemClickListener listener;

    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.item_bookmark, parent, false);

        // Return a new holder instance
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final BookmarkItem bookmarkItem = bookmarkManager.getAllBookmarks().get(position);
        TextView tvTitle = holder.tvTitle;
        ImageView imgIcon = holder.imgIcon;
        ImageView imgOption = holder.imgOption;
        imgOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFilterPopup(v, bookmarkItem);
            }
        });

        imgIcon.setImageDrawable(new BitmapDrawable(bookmarkItem.getIcon()));
        tvTitle.setText(bookmarkItem.getTitle());
    }

    @Override
    public int getItemCount() {
        return bookmarkManager.getAllBookmarks().size();
    }

    // Display anchored popup menu based on view selected
    private void showFilterPopup(View v, final BookmarkItem bookmarkItem) {
        PopupMenu popup = new PopupMenu(context, v);
        // Inflate the menu from xml
        popup.getMenuInflater().inflate(R.menu.bookmark_option, popup.getMenu());
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_edit:
                        DialogBookmarkEdit dialog = new DialogBookmarkEdit(context, bookmarkManager, bookmarkItem);
                        dialog.setOnChangeListener(new DialogBookmarkEdit.OnChangeListener() {
                            @Override
                            public void onChange() {
                                notifyDataSetChanged();
                            }
                        });
                        dialog.show();
                        return true;
                    case R.id.menu_delete:
                        bookmarkManager.deleteBookmark(bookmarkItem);
                        notifyDataSetChanged();
                        return true;
                    default:
                        return false;
                }
            }
        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu
        popup.show();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTitle;
        public ImageView imgIcon;
        public ImageView imgOption;

        public ViewHolder(final View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            imgIcon = (ImageView) itemView.findViewById(R.id.img_icon);
            imgOption = (ImageView) itemView.findViewById(R.id.img_option);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null)
                        listener.onItemClick(itemView, getLayoutPosition());
                }
            });
        }
    }
}
