package com.noah.antivirus.browser.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.noah.antivirus.browser.fragments.BookmarkSettingsFragment;
import com.noah.antivirus.browser.fragments.GeneralSettingsFragment;
import com.noah.antivirus.browser.fragments.PrivacySettingsFragment;
import com.noah.antivirus.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BrowserSettingsActivity extends AppCompatActivity implements View.OnClickListener{
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.menu)
    View menu;

    @BindView(R.id.item_general_settings)
    TextView item_general_settings;

    @BindView(R.id.item_bookmark_settings)
    TextView item_bookmark_settings;

    @BindView(R.id.item_privacy_settings)
    TextView item_privacy_settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser_settings);
        ButterKnife.bind(this);
        setTheme(R.style.SettingsFragmentStyle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_header);

        setTittle("Settings");

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
                    menu.setVisibility(View.GONE);
                } else {
                    setTittle("Settings");
                    menu.setVisibility(View.VISIBLE);
                }
            }
        });

        item_general_settings.setOnClickListener(this);
        item_privacy_settings.setOnClickListener(this);
        item_bookmark_settings.setOnClickListener(this);
    }

    public void setTittle(String tittle) {
        setTitle(tittle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.item_general_settings:
                slideFragment(new GeneralSettingsFragment());
                break;
            case R.id.item_privacy_settings:
                slideFragment(new PrivacySettingsFragment());
                break;
            case R.id.item_bookmark_settings:
                slideFragment(new BookmarkSettingsFragment());
                break;
        }
    }

    private void slideFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(R.id.container, fragment).addToBackStack(null).commit();
    }
}
