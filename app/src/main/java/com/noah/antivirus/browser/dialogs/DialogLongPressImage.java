package com.noah.antivirus.browser.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.browser.models.BookmarkItem;
import com.noah.antivirus.browser.models.BookmarkManager;
import com.noah.antivirus.R;

/**
 * Created by Noah.TruongPQ on 12/21/2016.
 */

public class DialogLongPressImage extends Dialog implements View.OnClickListener {
    private Context context;
    private String title;
    private String url;

    private TextView tv_title;
    private TextView download_image;
    private TextView open_image_in_new_tab;

    public interface OnClickOptionListener {
        void downloadImage();
        void openImageInNewTab();
    }

    public OnClickOptionListener listener;

    public void setListener(OnClickOptionListener listener) {
        this.listener = listener;
    }

    public DialogLongPressImage(Context context, String url, String title) {
        super(context);
        this.context = context;
        this.url = url;
        this.title = title;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_browser_image_options);

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText(title);

        download_image = (TextView) findViewById(R.id.download_image);
        open_image_in_new_tab = (TextView) findViewById(R.id.open_image_in_new_tab);

        download_image.setOnClickListener(this);
        open_image_in_new_tab.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.download_image:
                if (listener != null) listener.downloadImage();
                dismiss();
                break;
            case R.id.open_image_in_new_tab:
                if (listener != null) listener.openImageInNewTab();
                dismiss();
                break;
        }
    }
}
