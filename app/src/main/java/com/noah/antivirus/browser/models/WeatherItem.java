package com.noah.antivirus.browser.models;

import java.util.List;

/**
 * Created by Noah.TruongPQ on 12/30/2016.
 */

public class WeatherItem {
    private Coord coord;
    private List<Weather> weather;
    private String base;
    private Main main;
    private Wind wind;
    private Clound clouds;
    private long dt;
    private Sys sys;
    private long id;
    private String name;
    private int cod;

    public WeatherItem() {

    }

    public WeatherItem(Coord coord, List<Weather> weather, String base, Main main, Wind wind, Clound clouds, long dt, Sys sys, long id, String name, int cod) {
        this.coord = coord;
        this.weather = weather;
        this.base = base;
        this.main = main;
        this.wind = wind;
        this.clouds = clouds;
        this.dt = dt;
        this.sys = sys;
        this.id = id;
        this.name = name;
        this.cod = cod;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Clound getClouds() {
        return clouds;
    }

    public void setClouds(Clound clouds) {
        this.clouds = clouds;
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public class Coord {
        private float lon;
        private float lat;

        public Coord() {
        }

        public Coord(float lon, float lat) {
            this.lon = lon;
            this.lat = lat;
        }

        public float getLon() {
            return lon;
        }

        public void setLon(float lon) {
            this.lon = lon;
        }

        public float getLat() {
            return lat;
        }

        public void setLat(float lat) {
            this.lat = lat;
        }
    }

    public class Weather {
        private int id;
        private String main;
        private String description;
        private String icon;

        public Weather() {
        }

        public Weather(int id, String main, String description, String icon) {
            this.id = id;
            this.main = main;
            this.description = description;
            this.icon = icon;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMain() {
            return main;
        }

        public void setMain(String main) {
            this.main = main;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }
    }

    public class Main {
        private float temp;
        private float pressure;
        private int humidity;
        private float temp_min;
        private float temp_max;
        private float sea_level;
        private float grnd_level;

        public Main() {
        }

        public Main(float temp, float pressure, int humidity, float temp_min, float temp_max, float sea_level, float grnd_level) {
            this.temp = temp;
            this.pressure = pressure;
            this.humidity = humidity;
            this.temp_min = temp_min;
            this.temp_max = temp_max;
            this.sea_level = sea_level;
            this.grnd_level = grnd_level;
        }

        public float getTemp() {
            return temp;
        }

        public void setTemp(float temp) {
            this.temp = temp;
        }

        public float getPressure() {
            return pressure;
        }

        public void setPressure(float pressure) {
            this.pressure = pressure;
        }

        public int getHumidity() {
            return humidity;
        }

        public void setHumidity(int humidity) {
            this.humidity = humidity;
        }

        public float getTemp_min() {
            return temp_min;
        }

        public void setTemp_min(float temp_min) {
            this.temp_min = temp_min;
        }

        public float getTemp_max() {
            return temp_max;
        }

        public void setTemp_max(float temp_max) {
            this.temp_max = temp_max;
        }

        public float getSea_level() {
            return sea_level;
        }

        public void setSea_level(float sea_level) {
            this.sea_level = sea_level;
        }

        public float getGrnd_level() {
            return grnd_level;
        }

        public void setGrnd_level(float grnd_level) {
            this.grnd_level = grnd_level;
        }
    }

    public class Wind {
        private float speed;
        private float deg;

        public Wind() {
        }

        public Wind(float speed, float deg) {
            this.speed = speed;
            this.deg = deg;
        }

        public float getSpeed() {
            return speed;
        }

        public void setSpeed(float speed) {
            this.speed = speed;
        }

        public float getDeg() {
            return deg;
        }

        public void setDeg(float deg) {
            this.deg = deg;
        }
    }

    public class Clound {
        private int all;

        public Clound() {
        }

        public Clound(int all) {
            this.all = all;
        }

        public int getAll() {
            return all;
        }

        public void setAll(int all) {
            this.all = all;
        }
    }

    public class Sys {
        private float message;
        private String country;
        private long sunrise;
        private long sunset;

        public Sys() {
        }

        public Sys(float message, String country, long sunrise, long sunset) {
            this.message = message;
            this.country = country;
            this.sunrise = sunrise;
            this.sunset = sunset;
        }

        public float getMessage() {
            return message;
        }

        public void setMessage(float message) {
            this.message = message;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public long getSunrise() {
            return sunrise;
        }

        public void setSunrise(long sunrise) {
            this.sunrise = sunrise;
        }

        public long getSunset() {
            return sunset;
        }

        public void setSunset(long sunset) {
            this.sunset = sunset;
        }
    }
}
