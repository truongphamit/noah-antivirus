package com.noah.antivirus.browser.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.util.Log;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;
import com.noah.antivirus.browser.activities.BrowserSettingsActivity;
import com.noah.antivirus.browser.models.BookmarkManager;
import com.noah.antivirus.R;

import java.io.File;
import java.util.Set;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Noah.TruongPQ on 12/22/2016.
 */

public class BookmarkSettingsFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceClickListener {
    private static final int PICKFILE_REQUEST_CODE = 2;

    private static final String[] REQUIRED_PERMISSIONS = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private BrowserSettingsActivity activity;
    private BookmarkManager bookmarkManager;

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preference_bookmark);
        activity = (BrowserSettingsActivity) getActivity();
        activity.setTittle("Bookmark Settings");

        bookmarkManager = new BookmarkManager(getActivity());

        Preference export_bookmark = findPreference("export_bookmark");
        export_bookmark.setOnPreferenceClickListener(this);

        Preference import_bookmark = findPreference("import_bookmark");
        import_bookmark.setOnPreferenceClickListener(this);

        Preference delete_all_bookmark = findPreference("delete_all_bookmark");
        delete_all_bookmark.setOnPreferenceClickListener(this);

        PermissionsManager permissionsManager = PermissionsManager.getInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            permissionsManager.requestPermissionsIfNecessaryForResult(getActivity(), REQUIRED_PERMISSIONS, null);
        }
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        switch (preference.getKey()) {
            case "export_bookmark":
                PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(getActivity(), REQUIRED_PERMISSIONS,
                        new PermissionsResultAction() {
                            @Override
                            public void onGranted() {
                                bookmarkManager.exportBookmarks(getActivity());
                            }

                            @Override
                            public void onDenied(String permission) {
                                //TODO Show message
                            }
                        });
                break;
            case "import_bookmark":
                PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(getActivity(), REQUIRED_PERMISSIONS,
                        new PermissionsResultAction() {
                            @Override
                            public void onGranted() {
                                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                                intent.setType("file/*");
                                startActivityForResult(intent, PICKFILE_REQUEST_CODE);
                            }

                            @Override
                            public void onDenied(String permission) {
                                //TODO Show message
                            }
                        });
                break;
            case "delete_all_bookmark":
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
                builder.setTitle(getString(R.string.warning))
                        .setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                bookmarkManager.deleteAll();
                            }
                        }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setMessage("Do you want to delete all bookmark?");
                builder.show();
                break;
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request it is that we're responding to
        if (requestCode == PICKFILE_REQUEST_CODE) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                bookmarkManager.importBookmarksFromFile(new File(uri.getPath()), activity);
            }
        }
    }
}
