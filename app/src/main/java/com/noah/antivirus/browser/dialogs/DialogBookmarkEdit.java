package com.noah.antivirus.browser.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.browser.models.BookmarkItem;
import com.noah.antivirus.browser.models.BookmarkManager;
import com.noah.antivirus.R;

/**
 * Created by Noah.TruongPQ on 12/21/2016.
 */

public class DialogBookmarkEdit extends Dialog implements View.OnClickListener {
    private Context context;
    private BookmarkItem bookmarkItem;
    private BookmarkManager bookmarkManager;
    private ImageView imgIcon;
    private TextView tvHeader;
    private ImageView imgClose;
    private EditText edtTitle;
    private EditText edtUrl;
    private View delete;
    private View edit;

    public interface OnChangeListener {
        void onChange();
    }

    private OnChangeListener onChangeListener;

    public void setOnChangeListener(OnChangeListener onChangeListener) {
        this.onChangeListener = onChangeListener;
    }

    public DialogBookmarkEdit(Context context, BookmarkManager bookmarkManager, BookmarkItem bookmarkItem) {
        super(context);
        this.context = context;
        this.bookmarkItem = bookmarkItem;
        this.bookmarkManager = bookmarkManager;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_edit_bookmark);

        imgClose = (ImageView) findViewById(R.id.img_close);
        imgClose.setOnClickListener(this);

        imgIcon = (ImageView) findViewById(R.id.img_icon);
        imgIcon.setImageDrawable(new BitmapDrawable(bookmarkItem.getIcon()));
        tvHeader = (TextView) findViewById(R.id.tv_header);
        tvHeader.setText(bookmarkItem.getTitle());

        edtTitle = (EditText) findViewById(R.id.edt_title);
        edtUrl = (EditText) findViewById(R.id.edt_url);
        edtTitle.setText(bookmarkItem.getTitle());
        edtUrl.setText(bookmarkItem.getUrl());

        delete = findViewById(R.id.delete);
        delete.setOnClickListener(this);
        edit = findViewById(R.id.edit);
        edit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_close:
                dismiss();
                break;
            case R.id.delete:
                bookmarkManager.deleteBookmark(bookmarkItem);
                if (onChangeListener != null) onChangeListener.onChange();
                dismiss();
                break;
            case R.id.edit:
                String title = edtTitle.getText().toString();
                String url = edtUrl.getText().toString();
                if (title.equals(bookmarkItem.getTitle()) && url.equals(bookmarkItem.getUrl())) {
                    dismiss();
                } else {
                    BookmarkItem newItem = new BookmarkItem(url, title, bookmarkItem.getIcon());
                    bookmarkManager.deleteBookmark(bookmarkItem);
                    bookmarkManager.addBookmark(newItem);
                    if (onChangeListener != null) onChangeListener.onChange();
                    dismiss();
                }
                break;
        }
    }
}
