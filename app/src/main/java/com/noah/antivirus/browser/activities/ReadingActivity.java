package com.noah.antivirus.browser.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.noah.antivirus.browser.reading.HtmlFetcher;
import com.noah.antivirus.browser.reading.JResult;
import com.noah.antivirus.browser.reading.Speaker;
import com.noah.antivirus.browser.tools.BrowserSharePreference;
import com.noah.antivirus.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReadingActivity extends AppCompatActivity {
    private final int CHECK_CODE = 0x1;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_description)
    TextView tv_description;

    @BindView(R.id.img)
    ImageView img;

    @BindView(R.id.tv_body)
    TextView tv_body;

    @BindView(R.id.actionbar_title)
    TextView actionbar_title;

    @BindView(R.id.btn_reader_mode)
    ToggleButton btn_reader_mode;

    @BindView(R.id.btn_speak)
    ToggleButton btn_speak;

    @BindView(R.id.activity_reading)
    LinearLayout activity_reading;

    @BindView(R.id.img_text_size)
    ImageView img_text_size;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private Speaker speaker;

    private static final float XXLARGE = 30.0f;
    private static final float XLARGE = 26.0f;
    private static final float LARGE = 22.0f;
    private static final float MEDIUM = 18.0f;
    private static final float SMALL = 14.0f;
    private static final float XSMALL = 10.0f;

    private static float getTextSize(int size) {
        switch (size) {
            case 0:
                return XSMALL;
            case 1:
                return SMALL;
            case 2:
                return MEDIUM;
            case 3:
                return LARGE;
            case 4:
                return XLARGE;
            case 5:
                return XXLARGE;
            default:
                return MEDIUM;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reading);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_header);
        init();

        new AsyncTask<Void, Void, Void>() {
            private JResult result;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected Void doInBackground(Void... params) {
                HtmlFetcher fetcher = new HtmlFetcher();
                try {
                    result = fetcher.fetchAndExtract(getIntent().getStringExtra("url"), 2500, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressBar.setVisibility(View.GONE);
                if (result != null) {
                    actionbar_title.setText(result.getSitename());
                    tv_title.setText(result.getTitle());
                    Glide.with(ReadingActivity.this).load(result.getImageUrl()).into(img);
                    tv_description.setText(result.getDescription());
                    tv_body.setText(result.getText());
                }
            }
        }.execute();
    }

    @Override
    public void onDestroy() {
        speaker.destroy();
        super.onDestroy();
    }

    private void init() {
        checkTTS();
        setNightMode(BrowserSharePreference.getReadingNightMode(this));
        tv_body.setTextSize(getTextSize(BrowserSharePreference.getReadingTextSize(ReadingActivity.this)));

        btn_reader_mode.setChecked(BrowserSharePreference.getReadingNightMode(this));
        btn_reader_mode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setNightMode(isChecked);
                BrowserSharePreference.setReadingNightMode(ReadingActivity.this, isChecked);
            }
        });

        btn_speak.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    String text = tv_title.getText() + ". " + tv_description.getText() + "." + tv_body.getText();
                    speaker.allow(true);
                    speaker.speak(text);
                } else {
                    speaker.stop();
                }
            }
        });

        img_text_size.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(ReadingActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.textsize_dialog);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                SeekBar seekBar = (SeekBar) dialog.findViewById(R.id.seekbar);
                seekBar.setProgress(BrowserSharePreference.getReadingTextSize(ReadingActivity.this));
                seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        tv_title.setTextSize(getTextSize(progress));
                        tv_description.setTextSize(getTextSize(progress));
                        tv_body.setTextSize(getTextSize(progress));
                        BrowserSharePreference.setReadingTextSize(ReadingActivity.this, progress);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
                dialog.show();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CHECK_CODE){
            if(resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS){
                speaker = new Speaker(this);
            }else {
                Intent install = new Intent();
                install.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(install);
            }
        }
    }

    private void setNightMode(boolean b) {
        if (b) {
            activity_reading.setBackgroundColor(Color.BLACK);
            tv_title.setTextColor(Color.WHITE);
            tv_description.setTextColor(Color.GRAY);
            tv_body.setTextColor(Color.WHITE);
        } else {
            activity_reading.setBackgroundColor(Color.WHITE);
            tv_title.setTextColor(Color.BLACK);
            tv_description.setTextColor(Color.GRAY);
            tv_body.setTextColor(Color.BLACK);
        }
    }

    private void checkTTS(){
        Intent check = new Intent();
        check.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(check, CHECK_CODE);
    }
}
