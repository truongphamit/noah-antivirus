package com.noah.antivirus.browser.models;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.noah.antivirus.browser.tools.BitmapUtils;
import com.noah.antivirus.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by truon on 2/6/17.
 */

public class ShortcutManager {
    private static final String TITLE = "title";
    private static final String URL = "url";
    private static final String ICON = "icon";
    private static final String FILE_SHORTCUTS = "shortcut.dat";

    private File mFilesDir;
    private Map<String, BookmarkItem> shortcutsMap;
    private ExecutorService executorService;

    public ShortcutManager(@NonNull Context context) {
        shortcutsMap = new HashMap<>();
        executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new ShortcutManager.ShortcutInitializer(context));
    }

    public List<BookmarkItem> getAll() {
        return new ArrayList<>(shortcutsMap.values());
    }

    public boolean isShortcut(String url) {
        return shortcutsMap.containsKey(url);
    }

    public boolean add(@NonNull BookmarkItem item) {
        final String url = item.getUrl();
        if (shortcutsMap.containsKey(url)) {
            return false;
        }
        shortcutsMap.put(url, item);
        executorService.execute(new ShortcutsWriter(new LinkedList<>(shortcutsMap.values())));
        return true;
    }

    public synchronized boolean delete(@Nullable String url) {
        shortcutsMap.remove(url);
        executorService.execute(new ShortcutsWriter(new LinkedList<>(shortcutsMap.values())));
        return true;
    }

    public BookmarkItem getShortcut(String url) {
        return shortcutsMap.get(url);
    }

    private class ShortcutInitializer implements Runnable {
        private final Context context;

        private ShortcutInitializer(Context context) {
            this.context = context;
        }

        @Override
        public void run() {
            synchronized (ShortcutManager.this) {
                mFilesDir = context.getFilesDir();
                final Map<String, BookmarkItem> shortcuts = new HashMap<>();
                final File shortcutsFile = new File(mFilesDir, FILE_SHORTCUTS);


                BufferedReader reader;
                InputStream inputStream;

                try {
                    if (shortcutsFile.exists() && shortcutsFile.isFile()) {
                        //noinspection IOResourceOpenedButNotSafelyClosed
                        inputStream = new FileInputStream(shortcutsFile);
                    } else {
                        inputStream = context.getResources().openRawResource(R.raw.shortcut);
                    }
                    reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        JSONObject object = new JSONObject(line);
                        BookmarkItem item = new BookmarkItem();
                        item.setTitle(object.getString(TITLE));
                        String url = object.getString(URL);
                        item.setUrl(url);
                        item.setIcon(BitmapUtils.StringToBitMap(object.getString(ICON)));
                        shortcuts.put(url, item);
                    }

                    reader.close();
                    inputStream.close();
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }

                shortcutsMap = shortcuts;
            }
        }
    }

    private class ShortcutsWriter implements Runnable {
        private List<BookmarkItem> shortcuts;

        public ShortcutsWriter(List<BookmarkItem> shortcuts) {
            this.shortcuts = shortcuts;
        }

        @Override
        public void run() {
            File tempFile = new File(mFilesDir, String.format(Locale.US, "bm_%d.dat", System.currentTimeMillis()));
            File shortcutsFile = new File(mFilesDir, FILE_SHORTCUTS);
            boolean success = false;
            BufferedWriter bookmarkWriter;
            try {
                //noinspection IOResourceOpenedButNotSafelyClosed
                bookmarkWriter = new BufferedWriter(new FileWriter(tempFile, false));
                JSONObject object = new JSONObject();
                for (BookmarkItem item : shortcuts) {
                    object.put(URL, item.getUrl());

                    if (item.getTitle() != null) {
                        object.put(TITLE, item.getTitle());
                    } else {
                        object.put(TITLE, "");
                    }

                    if (item.getIcon() != null) {
                        object.put(ICON, BitmapUtils.BitMapToString(item.getIcon()));
                    } else {
                        object.put(ICON, "");
                    }
                    bookmarkWriter.write(object.toString());
                    bookmarkWriter.newLine();
                }
                bookmarkWriter.close();
                success = true;
            } catch (@NonNull IOException | JSONException e) {
                e.printStackTrace();
            }

            if (success) {
                // Overwrite the bookmarks file by renaming the temp file
                //noinspection ResultOfMethodCallIgnored
                tempFile.renameTo(shortcutsFile);
            }

        }
    }
}
