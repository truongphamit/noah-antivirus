package com.noah.antivirus.browser.fragments;


import android.Manifest;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.noah.antivirus.browser.MainBrowserActivity;
import com.noah.antivirus.browser.activities.BrowserSettingsActivity;
import com.noah.antivirus.browser.activities.ReadingActivity;
import com.noah.antivirus.browser.adpaters.ShortcutsAdapter;
import com.noah.antivirus.browser.dialogs.DialogLongPressImage;
import com.noah.antivirus.browser.dialogs.DialogLongPressImageLink;
import com.noah.antivirus.browser.dialogs.DialogLongPressLink;
import com.noah.antivirus.browser.models.BookmarkItem;
import com.noah.antivirus.browser.models.DailyWeather;
import com.noah.antivirus.browser.models.ShortcutManager;
import com.noah.antivirus.browser.models.WeatherItem;
import com.noah.antivirus.browser.networks.WeatherService;
import com.noah.antivirus.browser.tools.AdBlocker;
import com.noah.antivirus.browser.tools.BrowserSharePreference;
import com.noah.antivirus.browser.tools.DownloadHandler;
import com.noah.antivirus.browser.tools.StaticTools;
import com.noah.antivirus.browser.webview.AdvancedWebView;
import com.noah.antivirus.util.Utils;
import com.noah.antivirus.R;

import java.net.MalformedURLException;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.DOWNLOAD_SERVICE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabWebFragment extends Fragment implements View.OnClickListener, LocationListener {
    public static final String BASE_URL = "http://api.openweathermap.org/";
    private static final int MY_PERMISSIONS_REQUEST = 1;

    @BindView(R.id.edt_header)
    EditText edt_header;

    @BindView(R.id.webView)
    AdvancedWebView webView;

    @BindView(R.id.main_layout)
    View main_layout;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    @BindView(R.id.img_header)
    View img_header;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_ads_count)
    TextView tv_ads_count;

    @BindView(R.id.ads_count)
    View ads_count;

    //BottomBar

    @BindView(R.id.img_back)
    ImageView img_back;

    @BindView(R.id.img_forward)
    ImageView img_forward;

    @BindView(R.id.img_home)
    ImageView img_home;

    @BindView(R.id.img_bookmark)
    ImageView img_bookmark;

    @BindView(R.id.img_recent)
    ImageView img_recent;

    //Weather
    @BindView(R.id.tv_temp)
    TextView tv_temp;

    @BindView(R.id.tv_weather_main)
    TextView tv_weather_main;

    @BindView(R.id.tv_location_name)
    TextView tv_location_name;

    @BindView(R.id.tv_high_low_temp)
    TextView tv_high_low_temp;

    @BindView(R.id.tv_humidity)
    TextView tv_humidity;

    @BindView(R.id.img_weather)
    ImageView img_weather;

    @BindView(R.id.weather_container)
    View weather_container;

    @BindView(R.id.rv_shortcut)
    RecyclerView rv_shortcut;


    private String url;
    private Bundle webViewBundle;
    private MainBrowserActivity activity;
    private LocationManager locationManager;
    private WebViewHandler mWebViewHandler = new WebViewHandler(this);
    private ShortcutManager shortcutManager;
    private ShortcutsAdapter adapter;

    public View getMain_layout() {
        return main_layout;
    }

    public AdvancedWebView getWebView() {
        return webView;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static TabWebFragment newInstance() {
        TabWebFragment fragment = new TabWebFragment();
        return fragment;
    }

    public TabWebFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (webView == null) {
            webViewBundle = new Bundle();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_web, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (MainBrowserActivity) getActivity();
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

        initWeather();
        initShortcut();
        initWebView();
        init();
        initBottomBar();

        if (url != null) {
            swipe_refresh_layout.setVisibility(View.VISIBLE);
            webView.loadUrl(url);
            url = null;
        } else {
            if (webViewBundle != null && !webViewBundle.isEmpty()) {
                swipe_refresh_layout.setVisibility(View.VISIBLE);
                webView.restoreState(webViewBundle);
                webViewBundle.clear();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (webView != null) {
            webView.saveState(webViewBundle);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        // Load Settings
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        // Clear Cache
        if (preferences.getBoolean("clear_cache_on_exit", false)) {
            webView.clearCache(true);
        }

        // Clear History
        if (preferences.getBoolean("clear_history_on_exit", false)) {
            webView.clearHistory();
        }

        // Clear Cookies
        if (preferences.getBoolean("clear_cookies_on_exit", false)) {
            StaticTools.clearCookies(activity);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        loadSetting();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initWeather();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void initWeather() {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST);
            return;
        }

        if (locationManager != null) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                final WeatherService weatherService = retrofit.create(WeatherService.class);

                weatherService.getWeather(location.getLatitude(), location.getLongitude(), "8d13cb11b3a9d90902f527f02a00ed79", "metric").enqueue(new Callback<WeatherItem>() {
                    @Override
                    public void onResponse(Call<WeatherItem> call, Response<WeatherItem> response) {
                        WeatherItem weatherItem = response.body();
                        tv_temp.setText(String.valueOf(Math.round(weatherItem.getMain().getTemp())));
                        tv_weather_main.setText(" o  " + weatherItem.getWeather().get(0).getMain());
                        tv_location_name.setText(" c  " + weatherItem.getName());
                        try {
                            Glide.with(activity).load("http://openweathermap.org/img/w/" + weatherItem.getWeather().get(0).getIcon() + ".png").into(img_weather);
                        } catch (Exception ignored) {

                        }
                        tv_humidity.setText("Humidity " + String.valueOf(weatherItem.getMain().getHumidity()) + "%");

                        weather_container.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onFailure(Call<WeatherItem> call, Throwable t) {

                    }
                });

                weatherService.getDailyWeather(21.0317182, 105.7889918, "8d13cb11b3a9d90902f527f02a00ed79", "metric").enqueue(new Callback<DailyWeather>() {
                    @Override
                    public void onResponse(Call<DailyWeather> call, Response<DailyWeather> response) {
                        DailyWeather dailyWeather = response.body();
                        if (dailyWeather.getList() != null) {
                            String hlTemp = "High " + Math.round(dailyWeather.getList().get(0).getTemp().getMax()) + "\u2103 /" + " Low " + Math.round(dailyWeather.getList().get(0).getTemp().getMin()) + "\u2103";
                            tv_high_low_temp.setText(hlTemp);
                        }
                    }

                    @Override
                    public void onFailure(Call<DailyWeather> call, Throwable t) {

                    }
                });
            }
        }
    }

    private void initShortcut() {
        shortcutManager = new ShortcutManager(activity);
        adapter = new ShortcutsAdapter(activity, shortcutManager);
        rv_shortcut.setLayoutManager(new GridLayoutManager(activity, 4));
        rv_shortcut.setHasFixedSize(true);
        rv_shortcut.setAdapter(adapter);
        adapter.setOnItemClickListener(new ShortcutsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                if (position == shortcutManager.getAll().size()) { // Add new shortcut
                    Log.e("ADD", "___________________add new shortcut");
                    showFavoriteDialog(activity);
                } else {
                    swipe_refresh_layout.setVisibility(View.VISIBLE);
                    webView.loadUrl(shortcutManager.getAll().get(position).getUrl());
                }
            }
        });
    }

    private void showFavoriteDialog(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_add_favorite);
        ImageView imgClose = (ImageView) dialog.findViewById(R.id.img_close);
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        TextView add = (TextView) dialog.findViewById(R.id.add);
        final EditText edt = (EditText) dialog.findViewById(R.id.edt_url);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //add
                String key = edt.getText().toString();
                if (!StaticTools.checkURL(key)) {
                    return;
                }

                if (!(key.startsWith("http://") || (key.startsWith("https://")))) {
                    key = "http://" + key;
                }
                shortcutManager.add(new BookmarkItem(key, null, null));
                adapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void initBottomBar() {
        img_back.setOnClickListener(this);
        img_forward.setOnClickListener(this);
        img_home.setOnClickListener(this);
        img_bookmark.setOnClickListener(this);
        img_recent.setOnClickListener(this);
    }

    private void init() {
        ads_count.setOnClickListener(this);
        setAdsCount();

        edt_header.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN) {
                    swipe_refresh_layout.setVisibility(View.VISIBLE);
                    String key = edt_header.getText().toString();
                    if (!key.isEmpty()) {
                        if (StaticTools.checkURL(key)) {
                            if (!(key.startsWith("http://") || (key.startsWith("https://")))) {
                                webView.loadUrl("http://" + key);
                            } else {
                                webView.loadUrl(key);
                            }
                        } else {
                            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                            webView.loadUrl(preferences.getString("search_engine", "https://www.google.com/search?q=") + key);
                        }
                        edt_header.clearFocus();
                        Utils.hideKeyBoard(getActivity(), edt_header);
                    }
                    return true;
                }
                return false;
            }
        });

        main_layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                edt_header.clearFocus();
                return false;
            }
        });

        swipe_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webView.reload();
            }
        });

        img_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMenu(v);
            }
        });
    }

    private void popupMenu(View v) {
        final MenuBuilder menuBuilder = new MenuBuilder(getActivity());
        MenuInflater inflater = new MenuInflater(getActivity());
        inflater.inflate(R.menu.browser, menuBuilder);
        MenuPopupHelper optionsMenu = new MenuPopupHelper(getActivity(), menuBuilder, v);
        optionsMenu.setForceShowIcon(true);

        // State item checkbox
        menuBuilder.findItem(R.id.action_night_mode).setChecked(BrowserSharePreference.getNightMode(activity));
        menuBuilder.findItem(R.id.action_ads_block).setChecked(BrowserSharePreference.getAdBlock(activity));
        menuBuilder.findItem(R.id.action_no_image).setChecked(BrowserSharePreference.getNoImage(activity));

        menuBuilder.setCallback(new MenuBuilder.Callback() {
            @Override
            public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_bookmark:
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container_webview, BookmarkFragment.newInstance()).addToBackStack(null).commit();
                        return true;
                    case R.id.action_night_mode:
                        MenuItem nightModeItem = menuBuilder.findItem(R.id.action_night_mode);
                        if (nightModeItem.isChecked()) {
                            setNormalRendering();
                            activity.setTheme(R.style.Theme_Light);
                        } else {
                            setHardwareRendering();
                            activity.setTheme(R.style.Theme_Dark);
                        }
                        BrowserSharePreference.setNightMode(getActivity(), !nightModeItem.isChecked());
                        activity.recreate();
                        return true;
                    case R.id.action_ads_block:
                        MenuItem adsBlockItem = menuBuilder.findItem(R.id.action_ads_block);
                        webView.setAdsBlock(!adsBlockItem.isChecked());
                        BrowserSharePreference.setAdBlock(getActivity(), !adsBlockItem.isChecked());
                        return true;
                    case R.id.action_no_image:
                        MenuItem noImageItem = menuBuilder.findItem(R.id.action_no_image);
                        if (!noImageItem.isChecked()) {
                            webView.getSettings().setLoadsImagesAutomatically(false);
                            webView.getSettings().setBlockNetworkImage(true);
                        } else {
                            webView.getSettings().setLoadsImagesAutomatically(true);
                            webView.getSettings().setBlockNetworkImage(false);
                        }
                        BrowserSharePreference.setNoImage(getActivity(), !noImageItem.isChecked());
                        return true;
                    case R.id.action_shortcut:
                        StaticTools.createShortcut(getActivity());
                        Toast.makeText(getActivity(), "Created Shortcut", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.action_settings:
                        activity.startActivity(new Intent(activity, BrowserSettingsActivity.class));
                        return true;
                    case R.id.action_read_mode:
                        Intent intent = new Intent(activity, ReadingActivity.class);
                        intent.putExtra("url", webView.getUrl());
                        startActivity(intent);
                        return true;
                    case R.id.action_exit:
                        activity.finish();
                        return true;
                }
                return false;
            }

            @Override
            public void onMenuModeChange(MenuBuilder menu) {

            }
        });

        optionsMenu.show();
    }

    private void initWebView() {
        webView.setListener(getActivity(), new AdvancedWebView.Listener() {
            @Override
            public void onPageStarted(String url, Bitmap favicon) {
                edt_header.setText(url);
                progressBar.setVisibility(View.VISIBLE);

                try {
                    URL mysite = new URL("http://www.java2novice.com");
                    URL google = new URL("http://www.java2novice.com/");
                    if (mysite.getHost().equals(google.getHost())) {
                        Log.e("XXX", "Both sites are equal");
                    } else {
                        Log.e("YYY", "Both sites are not equal");
                    }
                } catch (MalformedURLException ex) {
                    ex.printStackTrace();
                }

                // Load icon for favorite
                if (shortcutManager.isShortcut(url)) {
                    shortcutManager.delete(url);
                    shortcutManager.add(new BookmarkItem(url, webView.getTitle(), favicon));
                }

                // Change state back in bottombar
                if (webView.canGoBack()) {
                    img_back.setImageResource(R.drawable.ic_browser_back_enable);
                    img_back.setTag(1);
                } else {
                    if (swipe_refresh_layout.getVisibility() == View.VISIBLE) {
                        img_back.setImageResource(R.drawable.ic_browser_back_enable);
                        img_back.setTag(1);
                    } else {
                        img_back.setImageResource(R.drawable.ic_browser_back_disable);
                        img_back.setTag(0);
                    }
                }

                // Change state forward in bottombar
                if (webView.canGoForward()) {
                    img_forward.setImageResource(R.drawable.ic_browser_forward_enable);
                    img_forward.setTag(1);
                } else {
                    img_forward.setImageResource(R.drawable.ic_browser_forward_disable);
                    img_forward.setTag(0);
                }

                // is Bookmark
                if (activity.getBookmarkManager().isBookmark(url)) {
                    img_bookmark.setImageResource(R.drawable.ic_browser_bookmark_enable);
                } else {
                    img_bookmark.setImageResource(R.drawable.ic_browser_bookmark_disable);
                }
            }

            @Override
            public void onPageFinished(final String url) {
                swipe_refresh_layout.setRefreshing(false);
                progressBar.setVisibility(View.INVISIBLE);

                setAdsCount();

//                String js ="javascript: ("
//                        +"function () { "
//
//                        +"var css = 'html {-webkit-filter: invert(100%);' +"
//                        +"    '-moz-filter: invert(100%);' + "
//                        +"    '-o-filter: invert(100%);' + "
//                        +"    '-ms-filter: invert(100%); }',"
//
//                        +"head = document.getElementsByTagName('head')[0],"
//                        +"style = document.createElement('style');"
//
//                        +"if (!window.counter) { window.counter = 1;} else  { window.counter ++;"
//                        +"if (window.counter % 2 == 0) { var css ='html {-webkit-filter: invert(0%); -moz-filter:    invert(0%); -o-filter: invert(0%); -ms-filter: invert(0%); }'}"
//                        +"};"
//
//                        +"style.type = 'text/css';"
//                        +"if (style.styleSheet){"
//                        +"style.styleSheet.cssText = css;"
//                        +"} else {"
//                        +"style.appendChild(document.createTextNode(css));"
//                        +"}"
//
//                        //injecting the css to the head
//                        +"head.appendChild(style);"
//                        +"}());";
//
//                webView.loadUrl(js);
            }

            @Override
            public void onPageError(int errorCode, String description, String failingUrl) {

            }

            @Override
            public void onDownloadRequested(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                String fileName = URLUtil.guessFileName(url, contentDisposition, mimeType);
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                DownloadManager dm = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
                dm.enqueue(request);
                Intent intent;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                } else {
                    intent = new Intent(Intent.ACTION_GET_CONTENT);
                }
                intent.addCategory(Intent.CATEGORY_OPENABLE); //CATEGORY.OPENABLE
                intent.setType(mimeType);//any application,any extension
                Toast.makeText(getApplicationContext(), "Downloading File", //To notify the Client that the file is being downloaded
                        Toast.LENGTH_LONG).show();

            }

            @Override
            public void onExternalPageRequest(String url) {
            }

            @Override
            public void onScrollChange(int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

            }

            @Override
            public void onCreateContextMenu(ContextMenu menu) {

            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                progressBar.setProgress(newProgress);
            }
        });

//        registerForContextMenu(webView);

        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
//        webView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/54.0.2840.85 Mobile Safari/537.36");

        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    edt_header.clearFocus();
                    Utils.hideKeyBoard(getActivity(), edt_header);
                } catch (Exception ignored) {

                }
                return false;
            }
        });

        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Message msg = mWebViewHandler.obtainMessage();
                if (msg != null) {
                    msg.setTarget(mWebViewHandler);
                    if (webView == null) {
                        return false;
                    }
                    webView.requestFocusNodeHref(msg);
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                if (img_back.getTag().toString().equals("1")) {
                    if (webView.canGoBack()) {
                        webView.goBack();
                    } else {
                        if (webView.getVisibility() == View.VISIBLE) {
                            swipe_refresh_layout.setVisibility(View.GONE);
                            img_back.setImageResource(R.drawable.ic_browser_back_disable);
                            img_back.setTag(0);
                            edt_header.setText("");
                        }
                    }
                }
                break;
            case R.id.img_forward:
                if (img_forward.getTag().toString().equals("1")) {
                    if (swipe_refresh_layout.getVisibility() == View.GONE) {
                        swipe_refresh_layout.setVisibility(View.VISIBLE);
                    } else {
                        if (webView.canGoForward()) webView.goForward();
                    }
                }
                break;
            case R.id.img_home:
                img_bookmark.setImageResource(R.drawable.ic_browser_bookmark_disable);
                img_back.setImageResource(R.drawable.ic_browser_back_disable);
                img_back.setTag(0);
                img_forward.setImageResource(R.drawable.ic_browser_forward_disable);
                img_forward.setTag(0);

                if (swipe_refresh_layout.getVisibility() == View.VISIBLE)
                    swipe_refresh_layout.setVisibility(View.GONE);
                webView.stopLoading();
                edt_header.setText("");
                break;
            case R.id.img_bookmark:
                if (swipe_refresh_layout.getVisibility() == View.VISIBLE) {
                    BookmarkItem bookmarkItem = new BookmarkItem();
                    bookmarkItem.setUrl(webView.getUrl());
                    bookmarkItem.setTitle(webView.getTitle());
                    if (webView.getFavicon() != null) {
                        bookmarkItem.setIcon(webView.getFavicon());
                    } else {
                        bookmarkItem.setIcon(BitmapFactory.decodeResource(activity.getResources(), R.drawable.ic_browser));
                    }

                    if (img_bookmark.getTag().toString().equals("0")) {
                        activity.getBookmarkManager().addBookmark(bookmarkItem);
                        img_bookmark.setImageResource(R.drawable.ic_browser_bookmark_enable);
                        img_bookmark.setTag("1");
                    } else {
                        activity.getBookmarkManager().deleteBookmark(bookmarkItem);
                        img_bookmark.setImageResource(R.drawable.ic_browser_bookmark_disable);
                        img_bookmark.setTag("0");
                    }
                }
                break;
            case R.id.img_recent:
                activity.showAllTab();
                break;
            case R.id.ads_count:
                showAdsPopup(v);
                break;
        }
    }

    private void loadSetting() {
        webView.setAdsBlock(BrowserSharePreference.getAdBlock(getActivity()));

        // No images
        if (BrowserSharePreference.getNoImage(getActivity())) {
            webView.getSettings().setLoadsImagesAutomatically(false);
            webView.getSettings().setBlockNetworkImage(true);
        }

        // Night Mode
        if (BrowserSharePreference.getNightMode(getActivity())) {
            setHardwareRendering();
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        WebSettings webSettings = webView.getSettings();

        // Set JavaScript
        webSettings.setJavaScriptEnabled(preferences.getBoolean("cb_javascript", true));
        webSettings.setJavaScriptCanOpenWindowsAutomatically(preferences.getBoolean("cb_javascript", true));

        // Set UsesAgent
        String usesAgent = preferences.getString("user_agent", getResources().getString(R.string.uses_agent_default));
        if (usesAgent.equals(getResources().getString(R.string.uses_agent_desktop))) {
            webView.setDesktopMode(true);
        } else {
            webView.setDesktopMode(false);
            webSettings.setUserAgentString(usesAgent);
        }

        // Set Location Access
        webView.setGeolocationEnabled(preferences.getBoolean("location_access", false));

        // Set Save Password
        webSettings.setSavePassword(preferences.getBoolean("save_password", true));
        webSettings.setSaveFormData(preferences.getBoolean("save_password", true));
    }

    // Enable night mdoe
    private void setHardwareRendering() {
        float[] sNegativeColorArray = {
                -1.0f, 0, 0, 0, 255, // red
                0, -1.0f, 0, 0, 255, // green
                0, 0, -1.0f, 0, 255, // blue
                0, 0, 0, 1.0f, 0 // alpha
        };

        Paint paint = new Paint();
        ColorMatrix matrix = new ColorMatrix();
        matrix.set(sNegativeColorArray);
        ColorMatrix matrixGray = new ColorMatrix();
        matrixGray.setSaturation(0);
        ColorMatrix concat = new ColorMatrix();
        concat.setConcat(matrix, matrixGray);
        ColorMatrixColorFilter filterInvertGray = new ColorMatrixColorFilter(concat);
        paint.setColorFilter(filterInvertGray);
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, paint);
    }

    // Disable night mode
    private void setNormalRendering() {
        webView.setLayerType(View.LAYER_TYPE_NONE, null);
    }

    private void longClickPage(@Nullable final String url) {
        if (webView == null) {
            return;
        }
        final WebView.HitTestResult result = webView.getHitTestResult();
        if (url != null) {
            if (result != null) {
                if (result.getType() == WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE || result.getType() == WebView.HitTestResult.IMAGE_TYPE) {
                    longPressImageLink(url, result.getExtra());
                } else {
                    longPressLink(url);
                }
            } else {
                longPressLink(url);
            }
        } else if (result != null && result.getExtra() != null) {
            final String newUrl = result.getExtra();
            if (result.getType() == WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE || result.getType() == WebView.HitTestResult.IMAGE_TYPE) {
                longPressImage(newUrl);
            } else {
                longPressLink(newUrl);
            }
        }

    }

    private void longPressLink(final String url) {
        DialogLongPressLink dialogLongPressLink = new DialogLongPressLink(activity, url, webView.getTitle());
        dialogLongPressLink.setListener(new DialogLongPressLink.OnClickOptionListener() {
            @Override
            public void openLinkNewTab() {
                activity.openLinkNewTab(url);
            }

            @Override
            public void copyLinkAddress() {
                StaticTools.setClipboard(activity, url);
            }
        });
        dialogLongPressLink.show();
    }

    private void longPressImage(final String url) {
        DialogLongPressImage dialog = new DialogLongPressImage(activity, url, webView.getTitle());
        dialog.setListener(new DialogLongPressImage.OnClickOptionListener() {
            @Override
            public void downloadImage() {
                DownloadHandler.onDownloadStart(activity, url, "attachment", null);
            }

            @Override
            public void openImageInNewTab() {
                activity.openLinkNewTab(url);
            }
        });
        dialog.show();
    }

    private void longPressImageLink(final String url, final String image_url) {
        DialogLongPressImageLink dialog = new DialogLongPressImageLink(activity, url, image_url, webView.getTitle());
        dialog.setListener(new DialogLongPressImageLink.OnClickOptionListener() {
            @Override
            public void openLinkNewTab() {
                activity.openLinkNewTab(url);
            }

            @Override
            public void copyLinkAddress() {
                StaticTools.setClipboard(activity, url);
            }

            @Override
            public void downloadImage() {
                DownloadHandler.onDownloadStart(activity, image_url, "attachment", null);
            }

            @Override
            public void openImageInNewTab() {
                activity.openLinkNewTab(image_url);
            }
        });
        dialog.show();
    }

    private static class WebViewHandler extends Handler {
        private TabWebFragment webFragment;

        WebViewHandler(TabWebFragment webFragment) {
            this.webFragment = webFragment;
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String url = msg.getData().getString("url");
            if (webFragment != null) {
                webFragment.longClickPage(url);
            }
        }
    }

    private void setAdsCount() {
        if (AdBlocker.ADS_COUNT != 0) {
            tv_ads_count.setVisibility(View.VISIBLE);
            tv_ads_count.setText(String.valueOf(AdBlocker.ADS_COUNT));
        } else {
            tv_ads_count.setVisibility(View.GONE);
        }
    }

    private void showAdsPopup(View v) {
        final PopupMenu popup = new PopupMenu(activity, v);
        // Inflate the menu from xml
        popup.getMenuInflater().inflate(R.menu.ads_options, popup.getMenu());

        String details = "<font color='#ff6666'>" + AdBlocker.ADS_COUNT + "</font>" + " ads blocked";
        MenuItem menuItem = popup.getMenu().findItem(R.id.action_details);
        menuItem.setTitle(Html.fromHtml(details));
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_settings:
                        final Dialog dialog = new Dialog(activity);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.ads_detail_dialog);
                        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                        TextView tvAds = (TextView) dialog.findViewById(R.id.tv_ads_count);
                        tvAds.setText(String.valueOf(BrowserSharePreference.getAdsBlockCount(activity)));

                        ImageView imgClose = (ImageView) dialog.findViewById(R.id.img_close);
                        imgClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });

                        ToggleButton control = (ToggleButton) dialog.findViewById(R.id.control);
                        control.setChecked(BrowserSharePreference.getAdBlock(activity));
                        control.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                BrowserSharePreference.setAdBlock(activity, b);
                                webView.setAdsBlock(b);
                            }
                        });

                        dialog.show();
                        return true;
                    default:
                        return false;
                }
            }
        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu
        popup.show();
    }

    // Location

    @Override
    public void onLocationChanged(Location location) {
        initWeather();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
