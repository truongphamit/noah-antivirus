package com.noah.antivirus.browser.models;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.noah.antivirus.browser.tools.BitmapUtils;
import com.noah.antivirus.browser.tools.StaticTools;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Noah.TruongPQ on 12/16/2016.
 */

public class BookmarkManager {
    private static final String TITLE = "title";
    private static final String URL = "url";
    private static final String ICON = "icon";
    private static final String FILE_BOOKMARKS = "bookmarks.dat";

    private File mFilesDir;
    private Map<String, BookmarkItem> bookmarksMap;
    private ExecutorService executorService;

    public BookmarkManager(@NonNull Context context) {
        bookmarksMap = new HashMap<>();
        executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new BookmarkInitializer(context));
    }

    public boolean isBookmark(String url) {
        return bookmarksMap.containsKey(url);
    }

    public boolean addBookmark(@NonNull BookmarkItem item) {
        final String url = item.getUrl();
        if (bookmarksMap.containsKey(url)) {
            return false;
        }
        bookmarksMap.put(url, item);
        executorService.execute(new BookmarksWriter(new LinkedList<>(bookmarksMap.values())));
        return true;
    }

    public synchronized void addBookmarkList(@Nullable List<BookmarkItem> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        for (BookmarkItem item : list) {
            final String url = item.getUrl();
            if (!bookmarksMap.containsKey(url)) {
                bookmarksMap.put(url, item);
            }
        }
        executorService.execute(new BookmarksWriter(new LinkedList<>(bookmarksMap.values())));
    }

    public synchronized boolean deleteBookmark(@Nullable BookmarkItem deleteItem) {
        if (deleteItem == null) {
            return false;
        }
        bookmarksMap.remove(deleteItem.getUrl());
        executorService.execute(new BookmarksWriter(new LinkedList<>(bookmarksMap.values())));
        return true;
    }

    public void deleteAll() {
        File bookmarksFile = new File(mFilesDir, FILE_BOOKMARKS);
        if (bookmarksFile.delete()) {
            bookmarksMap.clear();
        }
    }

    public List<BookmarkItem> getAllBookmarks() {
        File bookmarksFile = new File(mFilesDir, FILE_BOOKMARKS);
        if (!bookmarksFile.exists()) {
            bookmarksMap.clear();
        }

        return new ArrayList<>(bookmarksMap.values());
    }

    public synchronized void exportBookmarks(@NonNull Activity activity) {
        List<BookmarkItem> bookmarkList = getAllBookmarks();
        File bookmarksExport = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "Noah Browser Bookmarks Export.txt");
        int counter = 0;
        while (bookmarksExport.exists()) {
            counter++;
            bookmarksExport = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                    "Noah Browser Bookmarks Export-" + counter + ".txt");
        }
        BufferedWriter bookmarkWriter;
        try {
            //noinspection IOResourceOpenedButNotSafelyClosed
            bookmarkWriter = new BufferedWriter(new FileWriter(bookmarksExport,
                    false));
            JSONObject object = new JSONObject();
            for (BookmarkItem item : bookmarkList) {
                object.put(TITLE, item.getTitle());
                object.put(URL, item.getUrl());
                object.put(ICON, BitmapUtils.BitMapToString(item.getIcon()));
                bookmarkWriter.write(object.toString());
                bookmarkWriter.newLine();
            }
            StaticTools.showSnackbar(activity, "Bookmarks exported to " + ' ' + bookmarksExport.getPath());

            bookmarkWriter.close();
        } catch (@NonNull IOException | JSONException e) {
            e.printStackTrace();
        }

    }

    public synchronized void importBookmarksFromFile(@Nullable File file, @NonNull Activity activity) {
        if (file == null) {
            return;
        }
        List<BookmarkItem> list = new ArrayList<>();
        BufferedReader bookmarksReader = null;
        try {
            //noinspection IOResourceOpenedButNotSafelyClosed
            bookmarksReader = new BufferedReader(new FileReader(file));
            String line;
            int number = 0;
            while ((line = bookmarksReader.readLine()) != null) {
                JSONObject object = new JSONObject(line);
                BookmarkItem item = new BookmarkItem();
                item.setTitle(object.getString(TITLE));
                item.setUrl(object.getString(URL));
                item.setIcon(BitmapUtils.StringToBitMap(object.getString(ICON)));
                list.add(item);
                number++;
            }
            addBookmarkList(list);
            String message = "Bookmarks Were Imported";
            StaticTools.showSnackbar(activity, number + " " + message);
            bookmarksReader.close();
        } catch (@NonNull IOException | JSONException e) {
            e.printStackTrace();
            StaticTools.showSnackbar(activity, "Could not import bookmarks from file");
        }
    }

    private class BookmarkInitializer implements Runnable {
        private Context context;

        public BookmarkInitializer(Context context) {
            this.context = context;
        }

        @Override
        public void run() {
            mFilesDir = context.getFilesDir();
            File bookmarksFile = new File(mFilesDir, FILE_BOOKMARKS);

            BufferedReader bookmarksReader;
            InputStream inputStream;
            try {
                if (bookmarksFile.exists() && bookmarksFile.isFile()) {
                    //noinspection IOResourceOpenedButNotSafelyClosed
                    inputStream = new FileInputStream(bookmarksFile);

                    //noinspection IOResourceOpenedButNotSafelyClosed
                    bookmarksReader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;
                    while ((line = bookmarksReader.readLine()) != null) {
                        try {
                            JSONObject object = new JSONObject(line);
                            BookmarkItem item = new BookmarkItem();
                            item.setTitle(object.getString(TITLE));
                            String url = object.getString(URL);
                            item.setUrl(url);
                            item.setIcon(BitmapUtils.StringToBitMap(object.getString(ICON)));
                            bookmarksMap.put(url, item);
                        } catch (JSONException e) {
                            Log.e(getClass().getSimpleName(), "Can't parse line " + line, e);
                        }
                    }

                    bookmarksReader.close();
                    inputStream.close();
                }
            } catch (IOException e) {
                Log.e(getClass().getSimpleName(), "Error reading the bookmarks file", e);
            }
        }
    }

    private class BookmarksWriter implements Runnable {
        private List<BookmarkItem> bookmarks;

        public BookmarksWriter(List<BookmarkItem> bookmarks) {
            this.bookmarks = bookmarks;
        }

        @Override
        public void run() {
            File tempFile = new File(mFilesDir, String.format(Locale.US, "bm_%d.dat", System.currentTimeMillis()));
            File bookmarksFile = new File(mFilesDir, FILE_BOOKMARKS);
            boolean success = false;
            BufferedWriter bookmarkWriter;
            try {
                //noinspection IOResourceOpenedButNotSafelyClosed
                bookmarkWriter = new BufferedWriter(new FileWriter(tempFile, false));
                JSONObject object = new JSONObject();
                for (BookmarkItem item : bookmarks) {
                    object.put(URL, item.getUrl());
                    object.put(TITLE, item.getTitle());
                    object.put(ICON, BitmapUtils.BitMapToString(item.getIcon()));
                    bookmarkWriter.write(object.toString());
                    bookmarkWriter.newLine();
                }
                bookmarkWriter.close();
                success = true;
            } catch (@NonNull IOException | JSONException e) {
                e.printStackTrace();
            }

            if (success) {
                // Overwrite the bookmarks file by renaming the temp file
                //noinspection ResultOfMethodCallIgnored
                tempFile.renameTo(bookmarksFile);
            }

        }
    }
}
