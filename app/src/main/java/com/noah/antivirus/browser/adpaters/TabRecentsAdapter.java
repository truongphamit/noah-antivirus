package com.noah.antivirus.browser.adpaters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.noah.antivirus.browser.customview.RecentsAdapter;
import com.noah.antivirus.browser.fragments.TabWebFragment;
import com.noah.antivirus.browser.webview.AdvancedWebView;
import com.noah.antivirus.R;

import java.util.List;

/**
 * Created by Noah.TruongPQ on 12/5/2016.
 */

public class TabRecentsAdapter implements RecentsAdapter {

    private Context context;
    private List<TabWebFragment> fragments;

    public interface OnTabRemoveListener {
        void onRemove(int position);
    }

    private OnTabRemoveListener onTabRemoveListener;

    public void setOnTabRemoveListener(OnTabRemoveListener onTabRemoveListener) {
        this.onTabRemoveListener = onTabRemoveListener;
    }

    public TabRecentsAdapter(Context context, List<TabWebFragment> fragments) {
        this.context = context;
        this.fragments = fragments;
    }

    @Override
    public String getTitle(int position) {
        if (fragments.get(position).getWebView() != null) {
            String title = fragments.get(position).getWebView().getTitle();
            if (title.isEmpty()) {
                return "New Tab";
            } else {
                return title;
            }
        } else {
            return "New Tab";
        }
    }

    @Override
    public View getView(int position) {
        if (fragments.get(position).getWebView() != null) {
            AdvancedWebView webView = fragments.get(position).getWebView();
            if (webView.getParent() != null)
                ((ViewGroup) webView.getParent()).removeView(webView); // <- fix
            return fragments.get(position).getWebView();
        } else {
            ImageView iv = new ImageView(context);
            iv.setImageResource(R.drawable.com_facebook_button_like_background);
            return iv;
        }
    }

    @Override
    public Drawable getIcon(int position) {
        if (fragments.get(position).getWebView() != null) {
            Bitmap iconBitmap = fragments.get(position).getWebView().getFavicon();
            if (iconBitmap == null) {
                return context.getResources().getDrawable(R.drawable.ic_browser);
            } else {
                return new BitmapDrawable(context.getResources(), iconBitmap);
            }

        } else {
            return context.getResources().getDrawable(R.drawable.ic_browser);
        }
    }

    @Override
    public int getHeaderColor(int position) {
        return 0xeeeeeeee;
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public void remove(int position) {
        fragments.remove(position);
        if (onTabRemoveListener != null) onTabRemoveListener.onRemove(position);
    }
}
