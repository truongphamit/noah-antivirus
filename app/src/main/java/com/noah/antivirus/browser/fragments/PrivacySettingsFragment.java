package com.noah.antivirus.browser.fragments;

import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;

import com.noah.antivirus.browser.activities.BrowserSettingsActivity;
import com.noah.antivirus.R;

/**
 * Created by Noah.TruongPQ on 12/22/2016.
 */

public class PrivacySettingsFragment extends PreferenceFragmentCompat {
    private BrowserSettingsActivity activity;

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preference_privacy);
        activity = (BrowserSettingsActivity) getActivity();
        activity.setTittle("Privacy Settings");
    }
}
