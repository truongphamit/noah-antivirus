package com.noah.antivirus.browser.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.noah.antivirus.R;

/**
 * Created by Noah.TruongPQ on 12/21/2016.
 */

public class DialogLongPressLink extends Dialog implements View.OnClickListener {
    private Context context;
    private String title;
    private String url;

    private TextView tv_title;
    private TextView open_link_new_tab;
    private TextView copy_link_address;

    public interface OnClickOptionListener {
        void openLinkNewTab();
        void copyLinkAddress();
    }

    private OnClickOptionListener listener;

    public void setListener(OnClickOptionListener listener) {
        this.listener = listener;
    }

    public DialogLongPressLink(Context context, String url, String title) {
        super(context);
        this.context = context;
        this.url = url;
        this.title = title;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_browser_link_options);

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText(url);

        open_link_new_tab = (TextView) findViewById(R.id.open_link_new_tab);
        copy_link_address = (TextView) findViewById(R.id.copy_link_address);

        open_link_new_tab.setOnClickListener(this);
        copy_link_address.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.open_link_new_tab:
                if (listener != null) listener.openLinkNewTab();
                dismiss();
                break;
            case R.id.copy_link_address:
                if (listener != null) listener.copyLinkAddress();
                dismiss();
                break;
        }
    }
}
