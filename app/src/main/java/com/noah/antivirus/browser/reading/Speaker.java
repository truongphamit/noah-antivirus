package com.noah.antivirus.browser.reading;

import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.util.Locale;

/**
 * Created by truongpq on 1/16/17.
 */

public class Speaker implements TextToSpeech.OnInitListener {

    private Context context;

    private TextToSpeech tts;

    private boolean ready = false;

    private boolean allowed = false;

    public Speaker(Context context) {
        this.context = context;
        tts = new TextToSpeech(context, this);
    }

    public boolean isAllowed() {
        return allowed;
    }

    public void allow(boolean allowed) {
        this.allowed = allowed;
    }

    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {
            // Change this to match your
            // locale
            tts.setLanguage(Locale.getDefault());
            ready = true;
        } else {
            ready = false;
        }
    }


    public void speak(String text) {
        // Speak only if the TTS is ready
        // and the user has allowed speech
        String[] sentenceHolder = text.split("(?<=[a-z])\\.\\s+");
        if (ready && allowed) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                for (String s : sentenceHolder) {
                    tts.speak(s, TextToSpeech.QUEUE_ADD, null, null);
                }
            } else {
                for (String s : sentenceHolder) {
                    tts.speak(s, TextToSpeech.QUEUE_ADD, null);
                }
            }
        }

    }

    public void pause(int duration) {
        tts.playSilence(duration, TextToSpeech.QUEUE_ADD, null);
    }

    public void stop() {
        tts.stop();
    }

    // Free up resources
    public void destroy() {
        tts.stop();
        tts.shutdown();
    }
}
