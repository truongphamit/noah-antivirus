package com.noah.antivirus.browser.networks;

import com.noah.antivirus.browser.models.DailyWeather;
import com.noah.antivirus.browser.models.WeatherItem;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Noah.TruongPQ on 12/30/2016.
 */

public interface WeatherService {
    @GET("data/2.5/weather")
    Call<WeatherItem> getWeather(@Query("lat") double lat, @Query("lon") double lon, @Query("appid") String appid, @Query("units") String units);

    @GET("data/2.5/forecast/daily")
    Call<DailyWeather> getDailyWeather(@Query("lat") double lat, @Query("lon") double lon, @Query("appid") String appid, @Query("units") String units);
}
