package com.noah.antivirus.browser.adpaters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.browser.models.BookmarkItem;
import com.noah.antivirus.browser.models.ShortcutManager;
import com.noah.antivirus.R;

/**
 * Created by truon on 2/9/17.
 */

public class ShortcutsAdapter extends RecyclerView.Adapter<ShortcutsAdapter.ViewHolder> {
    private Context context;
    private ShortcutManager shortcutManager;

    // Define listener member variable
    private static OnItemClickListener listener;

    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public ShortcutsAdapter(Context context, ShortcutManager shortcutManager) {
        this.context = context;
        this.shortcutManager = shortcutManager;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.item_shortcut, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == shortcutManager.getAll().size()) {
            holder.favicon.setImageResource(R.drawable.add);
        } else {
            BookmarkItem item = shortcutManager.getAll().get(position);
            if (item.getIcon() == null) {
                holder.favicon.setImageResource(R.mipmap.ic_browser);
            } else {
                holder.favicon.setImageBitmap(item.getIcon());
            }

            if (item.getTitle() == null || item.getTitle().isEmpty()) {
                holder.tv_title.setText(item.getUrl());
            } else {
                holder.tv_title.setText(item.getTitle());
            }
        }
    }

    @Override
    public int getItemCount() {
        return shortcutManager.getAll().size() + 1;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView favicon;
        TextView tv_title;

        public ViewHolder(final View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            favicon = (ImageView) itemView.findViewById(R.id.img_favicon);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null)
                        listener.onItemClick(itemView, getLayoutPosition());
                }
            });
        }
    }
}
