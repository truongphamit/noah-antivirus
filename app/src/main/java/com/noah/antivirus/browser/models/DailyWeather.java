package com.noah.antivirus.browser.models;

import java.util.List;

/**
 * Created by Noah.TruongPQ on 1/3/2017.
 */

public class DailyWeather {
    private List<Item> list;

    public DailyWeather() {}

    public DailyWeather(List<Item> list) {
        this.list = list;
    }

    public List<Item> getList() {
        return list;
    }

    public void setList(List<Item> list) {
        this.list = list;
    }

    public class Item {
        private Temp temp;

        public Item() {}

        public Item(Temp temp) {
            this.temp = temp;
        }

        public Temp getTemp() {
            return temp;
        }

        public void setTemp(Temp temp) {
            this.temp = temp;
        }
    }

    public class Temp {
        private float min;
        private float max;

        public Temp() {}

        public Temp(float min, float max) {
            this.min = min;
            this.max = max;
        }

        public float getMin() {
            return min;
        }

        public void setMin(float min) {
            this.min = min;
        }

        public float getMax() {
            return max;
        }

        public void setMax(float max) {
            this.max = max;
        }
    }
}
