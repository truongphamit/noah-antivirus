package com.noah.antivirus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.noah.antivirus.util.Utils;
import com.noah.antivirus.R;
import com.noah.antivirus.model.AppLock;
import com.noah.antivirus.util.TypeFaceUttils;

import org.zakariya.stickyheaders.SectioningAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by truongpq on 8/24/16.
 */
public class AppLockAdapter extends SectioningAdapter {
    private Context c;
    List<AppLock> apps;
    List<AppLock> appLocks;
    List<AppLock> recommendApps;

    // Define listener member variable
    private static OnItemClickListener listener;

    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(ToggleButton toggleButton, int position, int sectionIndex, int itemIndex);
    }

    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public class HeaderViewHolder extends SectioningAdapter.HeaderViewHolder {
        TextView tv_header;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            tv_header = (TextView) itemView.findViewById(R.id.tv_header);
            TypeFaceUttils.setNomal(c, tv_header);
        }
    }

    public class ResultViewHolder extends ItemViewHolder {
        private View la_item;
        public TextView tvAppName;
        public ImageView imgIconApp;
        public ToggleButton toggle_lock;

        public ResultViewHolder(View view) {
            super(view);
            la_item = view.findViewById(R.id.la_item);
            tvAppName = (TextView) view.findViewById(R.id.tv_application_name);
            imgIconApp = (ImageView) view.findViewById(R.id.img_icon_app);
            toggle_lock = (ToggleButton) view.findViewById(R.id.toggle_lock);

            TypeFaceUttils.setNomal(c, tvAppName);
        }
    }

    public AppLockAdapter(Context c, List<AppLock> apps) {
        this.c = c;
        this.apps = apps;

        appLocks = new ArrayList<>();
        recommendApps = new ArrayList<>();
        getSectionList();
    }

    @Override
    public int getNumberOfSections() {
        return 2;
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        switch (sectionIndex) {
            case 0:
                return recommendApps.size();
            case 1:
                return appLocks.size();
            default:
                return 0;
        }
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return true;
    }

    @Override
    public SectioningAdapter.HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_header, parent, false);
        return new HeaderViewHolder(v);
    }

    @Override
    public ResultViewHolder onCreateItemViewHolder(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_app_lock, parent, false);
        return new ResultViewHolder(v);
    }

    @Override
    public void onBindHeaderViewHolder(SectioningAdapter.HeaderViewHolder viewHolder, int sectionIndex) {
        HeaderViewHolder hvh = (HeaderViewHolder) viewHolder;
        switch (sectionIndex) {
            case 0:
                hvh.tv_header.setText(c.getResources().getString(R.string.recommend_to_lock));
                break;
            case 1:
                hvh.tv_header.setText(c.getResources().getString(R.string.other));
                break;
        }
    }

    @Override
    public void onBindItemViewHolder(SectioningAdapter.ItemViewHolder viewHolder, int sectionIndex, final int itemIndex) {
        final ResultViewHolder holder = (ResultViewHolder) viewHolder;
        switch (sectionIndex) {
            case 0: {
                final AppLock appLock = recommendApps.get(itemIndex);
                holder.tvAppName.setText(appLock.getName());
                holder.imgIconApp.setImageDrawable(Utils.getIconFromPackage(appLock.getPackageName(), c));
                holder.toggle_lock.setChecked(appLock.isLock());

                holder.la_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null)
                            listener.onItemClick(holder.toggle_lock, apps.indexOf(appLock), 0, itemIndex);
                    }
                });

                holder.toggle_lock.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (holder.toggle_lock.isChecked()) {
                            holder.toggle_lock.setChecked(false);
                        } else {
                            holder.toggle_lock.setChecked(true);
                        }

                        if (listener != null)
                            listener.onItemClick(holder.toggle_lock, apps.indexOf(appLock), 0, itemIndex);
                    }
                });

                break;
            }
            case 1: {
                final AppLock appLock = appLocks.get(itemIndex);
                holder.tvAppName.setText(appLock.getName());
                holder.imgIconApp.setImageDrawable(Utils.getIconFromPackage(appLock.getPackageName(), c));
                holder.toggle_lock.setChecked(appLock.isLock());

                holder.la_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null)
                            listener.onItemClick(holder.toggle_lock, apps.indexOf(appLock), 1, itemIndex);
                    }
                });

                holder.toggle_lock.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (holder.toggle_lock.isChecked()) {
                            holder.toggle_lock.setChecked(false);
                        } else {
                            holder.toggle_lock.setChecked(true);
                        }

                        if (listener != null)
                            listener.onItemClick(holder.toggle_lock, apps.indexOf(appLock), 1, itemIndex);
                    }
                });

                break;
            }
        }
    }

    @Override
    public void notifyAllSectionsDataSetChanged() {
        getSectionList();
        super.notifyAllSectionsDataSetChanged();
    }

    private void getSectionList() {
        recommendApps.clear();
        appLocks.clear();
        for (AppLock appLock : apps) {
            if (Utils.isRecommendAppLock(appLock.getPackageName())) {
                recommendApps.add(appLock);
            } else {
                appLocks.add(appLock);
            }
        }

        Collections.sort(recommendApps);
        Collections.sort(appLocks);
    }

    @Override
    public void notifySectionItemChanged(int sectionIndex, int itemIndex) {
        switch (sectionIndex) {
            case 0:
                AppLock appLock = recommendApps.get(itemIndex);
                appLock.setLock(!appLock.isLock());
                AppLock app = apps.get(apps.indexOf(appLock));
                app.setLock(!app.isLock());
                break;
            case 1:
                AppLock appLock_1 = appLocks.get(itemIndex);
                appLock_1.setLock(!appLock_1.isLock());
                AppLock app_1 = apps.get(apps.indexOf(appLock_1));
                app_1.setLock(!app_1.isLock());
                break;
        }
        super.notifySectionItemChanged(sectionIndex, itemIndex);
    }
}
