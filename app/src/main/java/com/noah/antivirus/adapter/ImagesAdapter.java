package com.noah.antivirus.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.noah.antivirus.activities.AppLockImageActivity;
import com.noah.antivirus.model.Image;
import com.noah.antivirus.R;
import com.noah.antivirus.util.TypeFaceUttils;

import java.io.File;
import java.util.List;

/**
 * Created by truongpq on 9/9/16.
 */
public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {
    private Context context;
    private List<Image> images;

    public ImagesAdapter(Context context, List<Image> images) {
        this.context = context;
        this.images = images;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.item_image, parent, false);

        // Return a new holder instance
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Image image = images.get(position);

        File file = new File(image.getPath());
        Glide.with(context)
                .load(Uri.fromFile(file))
                .into(holder.img);

        holder.tvAppName.setText(image.getAppName());
        holder.tvDate.setText(image.getDate());

        TypeFaceUttils.setNomal(context, holder.tvAppName);
        TypeFaceUttils.setNomal(context, holder.tvDate);

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AppLockImageActivity.class);
                intent.putExtra("id", image.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView img;
        public TextView tvAppName;
        public TextView tvDate;
        public View item;

        public ViewHolder(final View itemView) {
            super(itemView);

            img = (ImageView) itemView.findViewById(R.id.image);
            tvAppName = (TextView) itemView.findViewById(R.id.tv_app_name);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            item = itemView.findViewById(R.id.item_image);
        }
    }
}
