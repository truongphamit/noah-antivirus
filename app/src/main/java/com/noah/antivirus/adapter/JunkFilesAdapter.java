package com.noah.antivirus.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.model.JunkOfApplication;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.util.Utils;

import java.util.Collections;
import java.util.List;

/**
 * Created by truongpq on 8/9/16.
 */
public class JunkFilesAdapter extends RecyclerView.Adapter<JunkFilesAdapter.ViewHolder> {
    private Context context;
    private List<JunkOfApplication> junkOfApplications;

    public JunkFilesAdapter(Context context, List<JunkOfApplication> junkOfApplications) {
        this.context = context;
        this.junkOfApplications = junkOfApplications;
        Collections.sort(junkOfApplications, Collections.reverseOrder());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.item_junk_files, parent, false);

        // Return a new holder instance
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        JunkOfApplication junkOfApplication = junkOfApplications.get(position);

        TextView tvAppName = holder.tvAppName;
        TypeFaceUttils.setNomal(context, tvAppName);
        tvAppName.setText(Utils.getAppNameFromPackage(context, junkOfApplication.getPackageName()));
        TextView tvAppCacheSize = holder.tvAppCacheSize;
        TypeFaceUttils.setNomal(context, tvAppCacheSize);
        tvAppCacheSize.setText(Utils.convertFileSizeToString(junkOfApplication.getCacheSize()));
        ImageView imgIconApp = holder.imgIconApp;
        imgIconApp.setImageDrawable(Utils.getIconFromPackage(junkOfApplication.getPackageName(), context));
    }

    @Override
    public int getItemCount() {
        return junkOfApplications.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvAppName;
        public TextView tvAppCacheSize;
        public ImageView imgIconApp;

        public ViewHolder(final View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            tvAppName = (TextView) itemView.findViewById(R.id.tv_app_name);
            tvAppCacheSize = (TextView) itemView.findViewById(R.id.tv_app_cache_size);
            imgIconApp = (ImageView) itemView.findViewById(R.id.img_app_icon);
        }
    }
}
