package com.noah.antivirus.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.model.AppLock;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.util.Utils;

import java.util.List;

/**
 * Created by truongpq on 8/26/16.
 */
public class ResultAppLockApdater extends RecyclerView.Adapter<ResultAppLockApdater.ViewHolder>{
    private Context context;
    private List<AppLock> appLocks;

    private static OnItemClickListener listener;
    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public ResultAppLockApdater(Context context, List<AppLock> appLocks) {
        this.context = context;
        this.appLocks = appLocks;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.item_result_app_lock, parent, false);

        // Return a new holder instance
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        AppLock appLock = appLocks.get(position);

        holder.imgIconApp.setImageDrawable(Utils.getIconFromPackage(appLock.getPackageName(), context));
        holder.tvAppName.setText(appLock.getName());

        holder.checkBox.setChecked(appLock.isLock());
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(holder.checkBox, position);
                }
            }
        });

        TypeFaceUttils.setNomal(context, holder.tvAppName);
    }

    @Override
    public int getItemCount() {
        return appLocks.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvAppName;
        public ImageView imgIconApp;
        public CheckBox checkBox;

        public ViewHolder(final View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            tvAppName = (TextView) itemView.findViewById(R.id.tv_application_name);
            imgIconApp = (ImageView) itemView.findViewById(R.id.img_icon_app);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
        }
    }
}
