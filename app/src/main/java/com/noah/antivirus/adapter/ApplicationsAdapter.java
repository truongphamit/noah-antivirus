package com.noah.antivirus.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.model.Application;
import com.noah.antivirus.util.TypeFaceUttils;

import java.util.List;

/**
 * Created by truongpq on 7/27/16.
 */
public class ApplicationsAdapter extends RecyclerView.Adapter<ApplicationsAdapter.ViewHolder> {

    private Context context;
    private List<Application> applications;

    public List<Application> getApplications() {
        return applications;
    }

    public void setApplications(List<Application> applications) {
        this.applications = applications;
    }

    private static OnItemClickListener listener;
    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public ApplicationsAdapter(Context context, List<Application> applications) {
        this.context = context;
        this.applications = applications;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.item_application, parent, false);

        // Return a new holder instance
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Application application = applications.get(position);

        TextView tvAppName = holder.tvAppName;
        ImageView imgIconApp = holder.imgIconApp;
        TextView tvSizeApp = holder.tvSizeApp;
        final CheckBox checkBox = holder.checkBox;

        TypeFaceUttils.setNomal(context, tvAppName);
        TypeFaceUttils.setNomal(context, tvSizeApp);

        tvAppName.setText(application.getName());
        imgIconApp.setImageDrawable(application.getIcon());
        tvSizeApp.setText(String.valueOf(((int) (application.getSize() / 1024) + "MB")));
        checkBox.setChecked(applications.get(position).isChoose());
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(checkBox, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return applications.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvAppName;
        public ImageView imgIconApp;
        public TextView tvSizeApp;
        public CheckBox checkBox;

        public ViewHolder(final View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            tvAppName = (TextView) itemView.findViewById(R.id.tv_application_name);
            imgIconApp = (ImageView) itemView.findViewById(R.id.img_icon_app);
            tvSizeApp = (TextView) itemView.findViewById(R.id.tv_size_application);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox_application);
        }
    }
}
