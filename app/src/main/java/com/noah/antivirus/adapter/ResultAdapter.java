package com.noah.antivirus.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.iface.IResultItemSelectedListener;
import com.noah.antivirus.iface.IProblem;
import com.noah.antivirus.model.AppProblem;
import com.noah.antivirus.model.SystemProblem;
import com.noah.antivirus.util.ProblemsDataSetTools;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.util.Utils;

import org.zakariya.stickyheaders.SectioningAdapter;

import java.util.ArrayList;

/**
 * Created by HuyLV-CT on 7/6/2016.
 */
public class ResultAdapter extends SectioningAdapter {

    public class HeaderViewHolder extends SectioningAdapter.HeaderViewHolder {
        TextView tv_header;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            tv_header = (TextView) itemView.findViewById(R.id.tv_header);
            TypeFaceUttils.setNomal(c, tv_header);
        }
    }

    public class ResultViewHolder extends ItemViewHolder {
        public ImageView iv_item_icon_app;
        public TextView tv_item_result_title;
        public TextView tv_item_result_detail;
        public LinearLayout rl_item_view;

        public ResultViewHolder(View view) {
            super(view);
            iv_item_icon_app = (ImageView) view.findViewById(R.id.iv_item_icon_app);
            tv_item_result_title = (TextView) view.findViewById(R.id.tv_item_result_title);
            tv_item_result_detail = (TextView) view.findViewById(R.id.tv_item_result_detail);
            rl_item_view = (LinearLayout) view.findViewById(R.id.rl_item_view);

            TypeFaceUttils.setNomal(c, tv_item_result_title);
            TypeFaceUttils.setNomal(c, tv_item_result_detail);
            TypeFaceUttils.setNomal(c, tv_item_result_detail);
        }
    }

    Context c;
    ArrayList<IProblem> problemArrayList;
    ArrayList<IProblem> appProblems;
    ArrayList<IProblem> systemProblems;

    ResultsAdapterProblemItem ri;

    private IResultItemSelectedListener _onItemChangedStateListener = null;

    public void setResultItemSelectedStateChangedListener(IResultItemSelectedListener listemer) {
        _onItemChangedStateListener = listemer;
    }


    public ResultAdapter(Context context, ArrayList<IProblem> problemArrayList) {
        this.c = context;
        this.problemArrayList = problemArrayList;

        appProblems = new ArrayList<>();
        ProblemsDataSetTools.getAppProblems(problemArrayList, appProblems);
        systemProblems = new ArrayList<>();
        ProblemsDataSetTools.getSystemProblems(problemArrayList, systemProblems);
    }

    @Override
    public int getNumberOfSections() {
        return 2;
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        switch (sectionIndex) {
            case 0:
                return appProblems.size();
            case 1:
                return systemProblems.size();
            default:
                return 0;
        }
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return true;
    }

    @Override
    public SectioningAdapter.HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_header, parent, false);
        return new HeaderViewHolder(v);
    }

    @Override
    public ResultViewHolder onCreateItemViewHolder(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_scan_result, parent, false);
        return new ResultViewHolder(v);
    }

    @Override
    public void onBindHeaderViewHolder(SectioningAdapter.HeaderViewHolder viewHolder, int sectionIndex) {
        HeaderViewHolder hvh = (HeaderViewHolder) viewHolder;
        switch (sectionIndex) {
            case 0:
                hvh.tv_header.setText(c.getString(R.string.application));
                break;
            case 1:
                hvh.tv_header.setText(c.getString(R.string.system));
                break;
        }
    }

    @Override
    public void onBindItemViewHolder(SectioningAdapter.ItemViewHolder viewHolder, int sectionIndex, int itemIndex) {
        final ResultViewHolder holder = (ResultViewHolder) viewHolder;
        switch (sectionIndex) {
            case 0: {
                ri = new ResultsAdapterProblemItem(appProblems.get(itemIndex));
                final AppProblem ap = ri.getAppProblem();
                holder.tv_item_result_title.setText(Utils.getAppNameFromPackage(c, ap.getPackageName()));
                holder.iv_item_icon_app.setImageDrawable(Utils.getIconFromPackage(ap.getPackageName(), c));
                if (ap.isDangerous()) {
                    holder.tv_item_result_detail.setTextColor(ContextCompat.getColor(c, R.color.HighRiskColor));
                    holder.tv_item_result_detail.setText(R.string.high_risk);
                } else {
                    holder.tv_item_result_detail.setTextColor(ContextCompat.getColor(c, R.color.MediumRiskColor));
                    holder.tv_item_result_detail.setText(R.string.medium_risk);
                }

                holder.rl_item_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (_onItemChangedStateListener != null) {
                            _onItemChangedStateListener.onItemSelected(ap, holder.iv_item_icon_app, c);

                        }
                    }
                });
                break;
            }
            case 1: {
                ResultsAdapterProblemItem ri = new ResultsAdapterProblemItem(systemProblems.get(itemIndex));
                final SystemProblem sp = ri.getSystemProblem();

                holder.tv_item_result_title.setText(sp.getTitle(c));
                holder.iv_item_icon_app.setImageDrawable(sp.getIcon(c));
                if (sp.isDangerous()) {
                    holder.tv_item_result_detail.setTextColor(ContextCompat.getColor(c, R.color.HighRiskColor));
                    holder.tv_item_result_detail.setText(R.string.high_risk);
                } else {
                    holder.tv_item_result_detail.setTextColor(ContextCompat.getColor(c, R.color.MediumRiskColor));
                    holder.tv_item_result_detail.setText(R.string.medium_risk);
                }

                holder.rl_item_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (_onItemChangedStateListener != null)
                            _onItemChangedStateListener.onItemSelected(sp, holder.iv_item_icon_app, c);
                    }
                });
                break;
            }
        }
    }

    public void remove(IProblem iProblem) {
        if (iProblem.getType() == IProblem.ProblemType.AppProblem) {
            int index = appProblems.indexOf(iProblem);
            appProblems.remove(iProblem);
            notifySectionItemRemoved(0, index);
        }

        if (iProblem.getType() == IProblem.ProblemType.SystemProblem) {
            int index = systemProblems.indexOf(iProblem);
            systemProblems.remove(iProblem);
            notifySectionItemRemoved(1, index);
        }
    }
}
