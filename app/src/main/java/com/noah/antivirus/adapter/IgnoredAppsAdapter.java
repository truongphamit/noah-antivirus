package com.noah.antivirus.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.iface.IProblem;
import com.noah.antivirus.model.AppProblem;
import com.noah.antivirus.model.MenacesCacheSet;
import com.noah.antivirus.model.SystemProblem;
import com.noah.antivirus.model.UserWhiteList;
import com.noah.antivirus.service.MonitorShieldService;
import com.noah.antivirus.util.Utils;
import com.noah.antivirus.R;
import com.noah.antivirus.util.TypeFaceUttils;

import java.util.List;

/**
 * Created by truongpq on 7/20/16.
 */
public class IgnoredAppsAdapter extends RecyclerView.Adapter<IgnoredAppsAdapter.ViewHolder> {

    private List<IProblem> iProblems;
    private Context context;
    private MonitorShieldService monitorShieldService;

    // Define listener member variable
    private static OnItemClickListener listener;

    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public IgnoredAppsAdapter(Context context, List<IProblem> iProblems, MonitorShieldService monitorShieldService) {
        this.iProblems = iProblems;
        this.context = context;
        this.monitorShieldService = monitorShieldService;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.item_ignored_app, parent, false);

        // Return a new holder instance
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final IProblem iProblem = iProblems.get(position);

        TextView tvName = holder.tvAppName;
        ImageView imgIcon = holder.imgIconApp;

        TypeFaceUttils.setNomal(context, tvName);

        if (iProblem.getType() == IProblem.ProblemType.AppProblem) {
            AppProblem appProblem = (AppProblem) iProblem;
            tvName.setText(Utils.getAppNameFromPackage(context, appProblem.getPackageName()));
            imgIcon.setImageDrawable(Utils.getIconFromPackage(appProblem.getPackageName(), context));
        } else {
            SystemProblem sp = ((SystemProblem) iProblem);
            tvName.setText(sp.getTitle(context));
            imgIcon.setImageDrawable(sp.getIcon(context));
        }

    }

    @Override
    public int getItemCount() {
        return iProblems.size();
    }

    public void removeItem(int position) {
        IProblem iProblem = iProblems.get(position);
        MenacesCacheSet menaceCacheSet = monitorShieldService.getMenacesCacheSet();
        UserWhiteList userWhiteList = monitorShieldService.getUserWhiteList();
        userWhiteList.removeItem(iProblem);
        userWhiteList.writeToJSON();
        menaceCacheSet.addItem(iProblem);
        menaceCacheSet.writeToJSON();

        iProblems.remove(iProblem);
        notifyItemRemoved(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvAppName;
        public ImageView imgIconApp;

        public ViewHolder(final View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            tvAppName = (TextView) itemView.findViewById(R.id.tv_app_name);
            imgIconApp = (ImageView) itemView.findViewById(R.id.img_icon_app);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null)
                        listener.onItemClick(itemView, getLayoutPosition());
                }
            });
        }
    }
}
