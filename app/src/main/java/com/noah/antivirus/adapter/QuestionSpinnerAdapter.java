package com.noah.antivirus.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.util.TypeFaceUttils;

/**
 * Created by truongpq on 8/20/16.
 */
public class QuestionSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {
    private Context context;
    private String[] questions;

    public QuestionSpinnerAdapter(Context context, String[] questions) {
        this.context = context;
        this.questions = questions;
    }

    @Override
    public int getCount() {
        return questions.length;
    }

    @Override
    public Object getItem(int position) {
        return questions[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(context);
        TypeFaceUttils.setNomal(context, txt);
        txt.setPadding(32, 32, 32, 32);
        txt.setTextSize(18);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setText(questions[position]);
        txt.setTextColor(Color.parseColor("#000000"));
        return txt;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(context);
        TypeFaceUttils.setNomal(context, txt);
        txt.setTextSize(16);
        txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_spinner_down, 0);
        txt.setText(questions[position]);
        txt.setTextColor(Color.parseColor("#000000"));
        return txt;
    }
}
