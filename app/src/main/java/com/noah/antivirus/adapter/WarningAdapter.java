package com.noah.antivirus.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.model.WarningData;
import com.noah.antivirus.R;
import com.noah.antivirus.iface.IProblem;
import com.noah.antivirus.model.ActivityData;
import com.noah.antivirus.model.AppProblem;
import com.noah.antivirus.model.PermissionData;
import com.noah.antivirus.model.SystemProblem;
import com.noah.antivirus.util.TypeFaceUttils;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by HuyLV-CT on 7/5/2016.
 */
public class WarningAdapter extends RecyclerView.Adapter<WarningAdapter.WarningHolder> {

    Context c;
    ArrayList<WarningData> warningDatas;
    IProblem problem;

    public WarningAdapter(Context c, IProblem problem) {
        this.c = c;
        this.problem = problem;

        warningDatas = new ArrayList<>();
        if (problem.getType() == IProblem.ProblemType.AppProblem) {
            AppProblem bp = (AppProblem) problem;

            Set<PermissionData> permissionDataList = bp.getPermissionData();
            for (PermissionData ad : permissionDataList) {
                WarningData wd = new WarningData();
                wd.icon = ContextCompat.getDrawable(c, setPermissionIcon(ad.getPermissionName()));
                wd.title = setPermissionTitle(ad.getPermissionName());
                wd.text = setPermissionMessage(ad.getPermissionName());
                warningDatas.add(wd);
            }

            boolean installedGPlay = bp.getInstalledThroughGooglePlay();
            if (!installedGPlay) {
                WarningData wd = new WarningData();
                wd.icon = ContextCompat.getDrawable(c, R.mipmap.information);
                wd.title = c.getResources().getString(R.string.title_installedGPlay);
                wd.text = c.getResources().getString(R.string.installedGPlay_message);
                warningDatas.add(wd);
            }
        } else {
            SystemProblem bp = (SystemProblem) problem;

            Context context = c;
            WarningData wd = new WarningData();
            wd.icon = bp.getSubIcon(context);
            wd.title = bp.getSubTitle(context);
            wd.text = bp.getDescription(context);
            warningDatas.add(wd);
        }
    }

    @Override
    public WarningHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_warning, parent, false);
        return new WarningHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WarningHolder holder, int position) {
        WarningData warningData = warningDatas.get(position);
        holder.iv_warning_icon.setImageDrawable(warningData.icon);
        holder.tv_warning_title.setText(warningData.title);
        holder.tv_warning_detail.setText(warningData.text);
    }

    @Override
    public int getItemCount() {
        return warningDatas.size();
    }

    public class WarningHolder extends RecyclerView.ViewHolder {
        public ImageView iv_warning_icon;
        public TextView tv_warning_title;
        public TextView tv_warning_detail;

        public WarningHolder(View view) {
            super(view);
            iv_warning_icon = (ImageView) view.findViewById(R.id.iv_warning_icon);
            tv_warning_title = (TextView) view.findViewById(R.id.tv_warning_title);
            tv_warning_detail = (TextView) view.findViewById(R.id.tv_warning_detail);

            TypeFaceUttils.setNomal(c, tv_warning_title);
            TypeFaceUttils.setNomal(c, tv_warning_detail);
        }
    }


    public String setPermissionMessage(String permissionName) {
        String message = "";
        Resources resources = c.getResources();

        if (permissionName.contains("READ_PHONE_STATE")) {

            message = resources.getString(R.string.read_phone_message);

        } else if (permissionName.contains("ACCESS_FINE_LOCATION")) {

            message = resources.getString(R.string.access_fine_message);

        } else if (permissionName.contains("READ_SMS")) {

            message = resources.getString(R.string.read_sms_message);

        } else if (permissionName.contains("WRITE_SMS")) {

            message = resources.getString(R.string.write_sms_message);

        } else if (permissionName.contains("SEND_SMS")) {

            message = resources.getString(R.string.send_sms_message);

        } else if (permissionName.contains("READ_HISTORY_BOOKMARKS")) {

            message = resources.getString(R.string.read_history_message);

        } else if (permissionName.contains("WRITE_HISTORY_BOOKMARKS")) {

            message = resources.getString(R.string.write_history_message);
        } else if (permissionName.contains("CALL_PHONE")) {

            message = resources.getString(R.string.call_phone_message);
        } else if (permissionName.contains("PROCESS_OUTGOING_CALLS")) {

            message = resources.getString(R.string.outgoing_phone_message);
        } else if (permissionName.contains("RECORD_AUDIO")) {

            message = resources.getString(R.string.record_audio_message);
        } else if (permissionName.contains("CAMERA")) {

            message = resources.getString(R.string.camera_message);
        }
        return message;
    }


    public String setPermissionTitle(String permissionName) {
        String message = "";
        Resources resources = c.getResources();

        if (permissionName.contains("READ_PHONE_STATE")) {

            message = resources.getString(R.string.phone_data_shared);

        } else if (permissionName.contains("ACCESS_FINE_LOCATION")) {

            message = resources.getString(R.string.location_shared);

        } else if (permissionName.contains("READ_SMS")) {

            message = resources.getString(R.string.read_your_sms);

        } else if (permissionName.contains("WRITE_SMS")) {

            message = resources.getString(R.string.write_sms_title);

        } else if (permissionName.contains("SEND_SMS")) {

            message = resources.getString(R.string.send_sms_title);

        } else if (permissionName.contains("READ_HISTORY_BOOKMARKS")) {

            message = resources.getString(R.string.read_history_bookmark_title);

        } else if (permissionName.contains("WRITE_HISTORY_BOOKMARKS")) {

            message = resources.getString(R.string.write_history_bookmark_title);
        } else if (permissionName.contains("CALL_PHONE")) {

            message = resources.getString(R.string.can_make_call_title);
        } else if (permissionName.contains("PROCESS_OUTGOING_CALLS")) {

            message = resources.getString(R.string.outgoing_calls_title);
        } else if (permissionName.contains("RECORD_AUDIO")) {

            message = resources.getString(R.string.record_audio_title);
        } else if (permissionName.contains("CAMERA")) {

            message = resources.getString(R.string.access_camera_title);
        }
        return message;
    }

    public int setPermissionIcon(String permissionName) {
        int icon = 0;


        if (permissionName.contains("READ_PHONE_STATE")) {

            icon = R.mipmap.phone_icon;

        } else if (permissionName.contains("ACCESS_FINE_LOCATION")) {

            icon = R.mipmap.fine_location_icon;

        } else if (permissionName.contains("READ_SMS")) {

            icon = R.mipmap.read_sms;

        } else if (permissionName.contains("WRITE_SMS")) {

            icon = R.mipmap.send_sms;

        } else if (permissionName.contains("SEND_SMS")) {

            icon = R.mipmap.send_sms;

        } else if (permissionName.contains("READ_HISTORY_BOOKMARKS")) {

            icon = R.mipmap.history_icon;

        } else if (permissionName.contains("WRITE_HISTORY_BOOKMARKS")) {

            icon = R.mipmap.history_icon;
        } else if (permissionName.contains("CALL_PHONE")) {

            icon = R.mipmap.phone_icon;
        } else if (permissionName.contains("PROCESS_OUTGOING_CALLS")) {

            icon = R.mipmap.phone_icon;
        } else if (permissionName.contains("RECORD_AUDIO")) {

            icon = R.mipmap.record_audio_icon;
        } else if (permissionName.contains("CAMERA")) {

            icon = R.mipmap.camera_icon;
        }

        return icon;
    }
}
