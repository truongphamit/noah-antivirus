package com.noah.antivirus.wifisecurity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.text.format.Formatter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.activities.SafeActivity;
import com.noah.antivirus.wifisecurity.baseactivities.BaseToolbarWifiSecurityActivity;
import com.noah.antivirus.wifisecurity.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.abdolahi.CircularMusicProgressBar;

public class WifiSecurityActivity extends BaseToolbarWifiSecurityActivity {

    @BindView(R.id.progress)
    CircularMusicProgressBar progress;

    @BindView(R.id.tv_wifi)
    TextView tv_wifi;

    @BindView(R.id.scan_info)
    View scan_info;

    @BindView(R.id.load_network_encryption)
    ImageView load_network_encryption;

    @BindView(R.id.load_no_captive_portal)
    ImageView load_no_captive_portal;

    @BindView(R.id.load_internet_connection)
    ImageView load_internet_connection;

    @BindView(R.id.load_no_arp_poisoning)
    ImageView load_no_arp_poisoning;

    @BindView(R.id.network_encryption)
    View network_encryption;

    @BindView(R.id.no_captive_portal)
    View no_captive_portal;

    @BindView(R.id.internet_connection)
    View internet_connection;

    @BindView(R.id.no_arp)
    View no_arp;

    @OnClick(R.id.progress)
    public void onScan(View view) {
        if (Utils.isWifiEnabled(this)) {
            progress.clearAnimation();
            scanWifi = new ScanWifi();
            scanWifi.execute();
        } else {
            progress.clearAnimation();
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        }
    }

    private boolean isScaning;
    private ScanWifi scanWifi;

    private BroadcastReceiver wifiChangeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                boolean connected = info.isConnected();

                if (connected) {
                    tv_wifi.setText(getString(R.string.wifi_name, Utils.getWifiName(WifiSecurityActivity.this)));
                    progress.setImageResource(R.drawable.ic_center_wifi);
                } else {
                    tv_wifi.setText(R.string.wifi_is_not_connected);
                    progress.setImageResource(R.drawable.ic_center_wifi_dis);
                }
            }
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.activity_wifi_security;
    }

    @Override
    public String getToolbarText() {
        return getString(R.string.wifi_security);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        IntentFilter intentFilter = new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        registerReceiver(wifiChangeReceiver, intentFilter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
        if (scanWifi != null) scanWifi.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (scanWifi != null) scanWifi.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (wifiChangeReceiver != null) {
            unregisterReceiver(wifiChangeReceiver);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                stopScan();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        stopScan();
    }

    private void stopScan() {
        if (isScaning) {
            com.noah.antivirus.util.Utils.showConfirmDialog(this, getString(R.string.stop_scanning), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    isScaning = false;
                    if (scanWifi != null) scanWifi.stop();
                    finish();
                }
            });
        } else {
            finish();
        }
    }

    private void init() {
        if (Utils.isWifiEnabled(this)) {
            tv_wifi.setText(getString(R.string.wifi_name, Utils.getWifiName(this)));
            progress.setImageResource(R.drawable.ic_center_wifi);
        } else {
            tv_wifi.setText(R.string.wifi_is_not_connected);
            progress.setImageResource(R.drawable.ic_center_wifi_dis);
        }

        progress.setProgressAnimationState(false);

        if (!isScaning) {
            Utils.scaleView(progress);
        }
    }

    private class ScanWifi extends AsyncTask<Void, Integer, Void> {
        private Animation loadingAnimation;
        private boolean running;
        private boolean isPaused = false;

        public ScanWifi() {
            loadingAnimation = AnimationUtils.loadAnimation(WifiSecurityActivity.this, R.anim.animation);
        }

        @Override
        protected void onCancelled() {
            running = false;
        }

        public void stop() {
            running = false;
        }

        public void pause() {
            isPaused = true;
        }

        public void resume() {
            isPaused = false;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isScaning = true;
            running = true;
            progress.setClickable(false);
            Animation animShow = AnimationUtils.loadAnimation(WifiSecurityActivity.this, R.anim.view_show);
            animShow.setDuration(500);
            scan_info.startAnimation(animShow);
            scan_info.setVisibility(View.VISIBLE);
            animateLoad(25, loadingAnimation);
        }

        @Override
        protected Void doInBackground(Void... params) {
            int current = 1;
            while (running && current <= 100) {
                if (!isPaused) {
                    try {
                        Thread.sleep(80);
                        publishProgress(current++);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            complete(values[0]);
            progress.setValue(values[0]);
            animateLoad(values[0] + 25, loadingAnimation);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (running) {
                Intent intent = new Intent(WifiSecurityActivity.this, SafeActivity.class);
                boolean check = Utils.checkNetworkEncryption(WifiSecurityActivity.this)
                        && Utils.checkNoCaptivePortal(WifiSecurityActivity.this)
                        && Utils.checkInternetConnection(WifiSecurityActivity.this)
                        && Utils.checkARPPoisoning();
                if (check) {
                    intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.wifi_is_secure));
                    startActivity(intent);
                    finish();
                } else {
                    showConfirmDialog();
                }
            }
        }

        private void animateLoad(int step, Animation animation) {
            switch (step) {
                case 25:
                    load_network_encryption.setVisibility(View.VISIBLE);
                    load_network_encryption.startAnimation(animation);
                    break;
                case 50:
                    load_no_captive_portal.setVisibility(View.VISIBLE);
                    load_no_captive_portal.startAnimation(animation);
                    break;
                case 75:
                    load_internet_connection.setVisibility(View.VISIBLE);
                    load_internet_connection.startAnimation(animation);
                    break;
                case 100:
                    load_no_arp_poisoning.setVisibility(View.VISIBLE);
                    load_no_arp_poisoning.startAnimation(animation);
                    break;
            }
        }

        private void complete(int step) {
            switch (step) {
                case 25:
                    load_network_encryption.clearAnimation();
                    if (Utils.checkNetworkEncryption(WifiSecurityActivity.this)) {
                        load_network_encryption.setImageResource(R.drawable.done);
                    } else {
                        load_network_encryption.setImageResource(R.drawable.ic_warning);
                    }
                    break;
                case 50:
                    load_no_captive_portal.clearAnimation();
                    if (Utils.checkNoCaptivePortal(WifiSecurityActivity.this)) {
                        load_no_captive_portal.setImageResource(R.drawable.done);
                    } else {
                        load_no_captive_portal.setImageResource(R.drawable.ic_warning);
                    }
                    break;
                case 75:
                    load_internet_connection.clearAnimation();
                    if (Utils.checkInternetConnection(WifiSecurityActivity.this)) {
                        load_internet_connection.setImageResource(R.drawable.done);
                    } else {
                        load_internet_connection.setImageResource(R.drawable.ic_warning);
                    }
                    break;
                case 100:
                    load_no_arp_poisoning.clearAnimation();
                    if (Utils.checkARPPoisoning()) {
                        load_no_arp_poisoning.setImageResource(R.drawable.done);
                    } else {
                        load_no_arp_poisoning.setImageResource(R.drawable.ic_warning);
                    }
                    break;
            }
        }
    }

    public void showConfirmDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WifiSecurityActivity.this, R.style.MyAlertDialogStyle);
        alertDialogBuilder.setTitle(R.string.wifi_is_not_secure);
        alertDialogBuilder.setMessage(getResources().getString(R.string.please_disconnect, Utils.getWifiName(WifiSecurityActivity.this)));
        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.disconnect), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                finish();
            }
        });
        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }
}
