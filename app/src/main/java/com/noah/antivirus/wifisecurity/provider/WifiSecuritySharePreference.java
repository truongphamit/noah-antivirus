package com.noah.antivirus.wifisecurity.provider;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by truongpq on 16/05/2017.
 */

public class WifiSecuritySharePreference {
    public static SharedPreferences getSharePreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}
