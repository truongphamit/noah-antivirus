package com.noah.antivirus.wifisecurity.baseactivities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.noah.antivirus.R;


/**
 * Created by Hungpq on 3/27/17.
 */

public abstract class BaseToolbarWifiSecurityActivity extends BaseFontActivity {
    private int layoutId;
    private String toolbarText = "Why Pay";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        setupToolbar();
    }

    public void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));

        ((TextView) findViewById(R.id.txt_toolbar)).setText(getToolbarText());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public abstract int getLayoutId();

    public abstract String getToolbarText();
}
