package com.noah.antivirus.animation;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.noah.antivirus.R;

/**
 * Created by truongpq on 7/30/16.
 */
public class ScanningProgress extends FrameLayout {

    private Paint paint;
    private Context context;
    private int mProgress;
    private int mWaveToTop;
    private ImageView imageView;

    public ScanningProgress(Context context) {
        super(context);
        init(context);
    }

    public ScanningProgress(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ScanningProgress(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    private void init(Context context) {
        paint = new Paint();
        this.context = context;
        imageView = new ImageView(context);
        imageView.setImageResource(R.mipmap.bg4);
        mWaveToTop = (int) (getHeight() * (1f - 0 / 100f));
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, Gravity.TOP);
        addView(imageView, params);
    }

    public void setProgress(int progress) {
        this.mProgress = progress > 100 ? 100 : progress;
        computeWaveToTop();
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if (hasWindowFocus) {
            computeWaveToTop();
        }
    }

    private void computeWaveToTop() {
        mWaveToTop = (int) (getHeight() * (1f - mProgress / 100f));
        ViewGroup.LayoutParams params = imageView.getLayoutParams();
        if (params != null) {
            ((LayoutParams) params).topMargin = mWaveToTop;
        }
        imageView.setLayoutParams(params);
    }

    public float getYProgress() {
        int[] location = new int[2];
        imageView.getLocationOnScreen(location);
        return location[1] - imageView.getHeight();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
