package com.noah.antivirus.activities;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.noah.antivirus.R;
import com.noah.antivirus.databases.ImagesDatabaseHelper;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.model.Image;
import com.noah.antivirus.util.TypeFaceUttils;

import java.io.File;

import butterknife.BindView;

public class AppLockImageActivity extends BaseToolbarActivity {

    @BindView(R.id.tv_app_name)
    TextView tv_app_name;

    @BindView(R.id.tv_date)
    TextView tv_date;

    @BindView(R.id.img_del)
    ImageView img_del;

    private void customFont() {
        TypeFaceUttils.setNomal(this, tv_app_name);
        TypeFaceUttils.setNomal(this, tv_date);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_app_lock_image;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customFont();
        final ImagesDatabaseHelper imagesDatabaseHelper = ImagesDatabaseHelper.getInstance(this);
        ImageView img = (ImageView) findViewById(R.id.img);
        int id = getIntent().getIntExtra("id", -1);
        if (id != -1) {
            final Image image = imagesDatabaseHelper.findByID(id);
            final File file = new File(image.getPath());
            Glide.with(this)
                    .load(Uri.fromFile(file))
                    .into(img);

            tv_app_name.setText(image.getAppName());
            tv_date.setText(image.getDate());

            img_del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (file.delete()) {
                        imagesDatabaseHelper.delete(image.getId());
                        finish();
                    }
                }
            });
        }

    }
}
