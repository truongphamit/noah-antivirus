package com.noah.antivirus.activities;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.graphics.drawable.ClipDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.model.AppLock;
import com.noah.antivirus.model.JunkOfApplication;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.iface.IOnActionFinished;
import com.noah.antivirus.iface.IProblem;
import com.noah.antivirus.model.AppData;
import com.noah.antivirus.service.MonitorShieldService;
import com.noah.antivirus.service.ScanningFileSystemAsyncTask;
import com.noah.antivirus.util.ProblemsDataSetTools;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.util.Utils;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import butterknife.BindView;

/**
 * Created by HuyLV-CT on 6/28/2016.
 */
public class ScanningActivity extends BaseToolbarActivity implements MonitorShieldService.IClientInterface{

    //Registration for any activity inside our app to listen to service calls
    ScanningFileSystemAsyncTask _currentScanTask = null;

    @BindView(R.id.tv_progress)
    TextView tv_progress;

    @BindView(R.id.tv_step)
    TextView tv_step;

    @BindView(R.id.bottomIssues)
    TextView bottomIssues;

    @BindView(R.id.bottomIssues_privacy)
    TextView bottomIssues_privacy;

    @BindView(R.id.bottomIssues_booster)
    TextView bottomIssues_booster;

    @BindView(R.id.tv_title_threat)
    TextView tv_title_threat;

    @BindView(R.id.tv_title_privacy)
    TextView tv_title_privacy;

    @BindView(R.id.tv_title_booster)
    TextView tv_title_booster;

    @BindView(R.id.img_2)
    ImageView img2;

    @BindView(R.id.img_3)
    ImageView img3;

    private void customFont() {
        TypeFaceUttils.setNomal(this, tv_progress);
        TypeFaceUttils.setNomal(this, tv_step);
        TypeFaceUttils.setNomal(this, bottomIssues);
        TypeFaceUttils.setNomal(this, bottomIssues_privacy);
        TypeFaceUttils.setNomal(this, bottomIssues_booster);
        TypeFaceUttils.setNomal(this, tv_title_threat);
        TypeFaceUttils.setNomal(this, tv_title_privacy);
        TypeFaceUttils.setNomal(this, tv_title_booster);

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_scanning;
    }

    private boolean bound;
    private MonitorShieldService monitorShieldService;

    public MonitorShieldService getMonitorShieldService() {
        return monitorShieldService;
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            bound = true;
            MonitorShieldService.MonitorShieldLocalBinder binder = (MonitorShieldService.MonitorShieldLocalBinder) service;
            monitorShieldService = binder.getServiceInstance();

            monitorShieldService.registerClient(ScanningActivity.this);

            new MyTask().execute();

            if (monitorShieldService.getMenacesCacheSet().getItemCount() == 0) {
                img2.setImageResource(R.drawable.clip_bg2);
            } else if (monitorShieldService.getMenacesCacheSet().getItemCount() <= 10) {
                img2.setImageResource(R.drawable.clip_bg2_1);
            } else {
                img2.setImageResource(R.drawable.clip_bg2_2);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bound = false;
            monitorShieldService = null;
        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        bindService(new Intent(this, MonitorShieldService.class), serviceConnection, BIND_AUTO_CREATE);

        customFont();

        tv_progress.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);

        ClipDrawable clipDrawable = (ClipDrawable) img3.getDrawable();
        clipDrawable.setLevel(10000);
    }

    //Registration for any activity inside our app to listen to service calls
    MonitorShieldService.IClientInterface _appMonitorServiceListener = null;
    @Override
    public void onMonitorFoundMenace(IProblem menace) {
        if (_appMonitorServiceListener != null)
            _appMonitorServiceListener.onMonitorFoundMenace(menace);
    }

    @Override
    public void onScanResult(List<PackageInfo> allPackages, Set<IProblem> menacesFound) {
        if (_appMonitorServiceListener != null)
            _appMonitorServiceListener.onScanResult(allPackages, menacesFound);
    }

    class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            _startRealScan();
            return null;
        }
    }

    private void _startRealScan() {
        startMonitorScan(new MonitorShieldService.IClientInterface() {
            @Override
            public void onMonitorFoundMenace(IProblem menace) {

            }

            @Override
            public void onScanResult(List<PackageInfo> allPackages, Set<IProblem> menacesFound) {
                AppData appData = getAppData();
                appData.setFirstScanDone(true);
                appData.setFirstScanDone(true);
                appData.serialize(ScanningActivity.this);

                Utils.numOfPackages = allPackages.size();
                _startScanningAnimation(allPackages, menacesFound);
            }
        });

        if (monitorShieldService != null) {
            monitorShieldService.scanFileSystem();
        }
    }

    public void startMonitorScan(MonitorShieldService.IClientInterface listener) {
        _appMonitorServiceListener = listener;
    }

    private void _startScanningAnimation(final List<PackageInfo> allPackages, final Collection<? extends IProblem> tempBadResults) {
        Collection<IProblem> appProblems = new ArrayList<IProblem>();
        ProblemsDataSetTools.getAppProblems(tempBadResults, appProblems);

        _currentScanTask = new ScanningFileSystemAsyncTask(this, allPackages, appProblems);
        _currentScanTask.setAsyncTaskCallback(new IOnActionFinished() {
            @Override
            public void onFinished() {
                _currentScanTask = null;
                AppData appData = getAppData();
                appData.setLastScanDate(new DateTime());
                appData.serialize(ScanningActivity.this);
                _doAfterScanWork(tempBadResults);
            }
        });
        _currentScanTask.execute();
    }

    public AppData getAppData() {
        return AppData.getInstance(this);
    }

    void _doAfterScanWork(final Collection<? extends IProblem> tempBadResults) {

        _currentScanTask = null;
        if (isSafeSate()) {
            Intent intent = new Intent(this, SafeActivity.class);
            intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.you_are_safe));
            startActivity(intent);
        } else {
            Intent i = new Intent(ScanningActivity.this, ScanningResultActivity.class);
            startActivity(i);
        }
        finish();
    }

    private boolean isSafeSate() {
        boolean application = (monitorShieldService.getMenacesCacheSet().getItemCount() == 0);

        boolean phoneBoost = (monitorShieldService.getRunningApplications().size() == 0);

        long cacheSize = 0;
        for (JunkOfApplication junkOfApplication : Utils.junkOfApplications) {
            cacheSize += junkOfApplication.getCacheSize();
        }
        boolean junk = (cacheSize / (1024 * 1024) == 0);

        int countApp = 0;
        for (AppLock appLock : monitorShieldService.getAppLock()) {
            if (appLock.isRecommend()) ++countApp;
        }
        boolean protect = countApp == 0;

        return (application && phoneBoost && junk && protect);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (_currentScanTask != null) {
            _currentScanTask.cancel(true);
        }
    }

    @Override
    public void onBackPressed() {
        Utils.showConfirmDialog(this, getString(R.string.stop_scanning), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Utils.showConfirmDialog(this, getString(R.string.stop_scanning), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        }
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (_currentScanTask != null)
            _currentScanTask.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (_currentScanTask != null)
            _currentScanTask.resume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (bound && monitorShieldService != null) {
            unbindService(serviceConnection);
            bound = false;
        }
    }
}
