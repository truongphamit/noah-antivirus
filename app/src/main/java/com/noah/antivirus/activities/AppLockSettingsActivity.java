package com.noah.antivirus.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.service.LockService;
import com.noah.antivirus.service.MonitorShieldService;
import com.noah.antivirus.R;
import com.noah.antivirus.util.TypeFaceUttils;

import butterknife.BindView;

public class AppLockSettingsActivity extends BaseToolbarActivity {
    public static final String KEY_SELFIE = "selfie";
    public static final String KEY_VIBRATE = "vibrate";

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.tv_applocker_service)
    TextView tv_applocker_service;

    @BindView(R.id.tv_applocker_service_decription)
    TextView tv_applocker_service_decription;

    @BindView(R.id.title_1)
    TextView title_1;

    @BindView(R.id.tv_edit_password)
    TextView tv_edit_password;

    @BindView(R.id.tv_edit_password_deription)
    TextView tv_edit_password_deription;

    @BindView(R.id.tv_relock_policy)
    TextView tv_relock_policy;

    @BindView(R.id.tv_relock_policy_decription)
    TextView tv_relock_policy_decription;

    @BindView(R.id.tv_relock_timeout)
    TextView tv_relock_timeout;

    @BindView(R.id.tv_relock_timeout_deription)
    TextView tv_relock_timeout_deription;

    @BindView(R.id.checkbox_applocker_service)
    CheckBox checkbox_applocker_service;

    @BindView(R.id.item_edit_password)
    View item_edit_password;

    @BindView(R.id.checkbox_relock_policy)
    CheckBox checkbox_relock_policy;

    @BindView(R.id.item_relock_timeout)
    View item_relock_timeout;

    @BindView(R.id.tv_selfie)
    TextView tv_selfie;

    @BindView(R.id.tv_selfie_deription)
    TextView tv_selfie_deription;

    @BindView(R.id.item_selfie)
    View item_selfie;

    @BindView(R.id.checkbox_selfie)
    CheckBox checkbox_selfie;

    @BindView(R.id.tv_vibrate)
    TextView tv_vibrate;

    @BindView(R.id.tv_vibrate_deription)
    TextView tv_vibrate_deription;

    @BindView(R.id.checkbox_vibrate)
    CheckBox checkbox_vibrate;

    private SharedPreferences sharedPreferences;

    private void customFont() {
        TypeFaceUttils.setNomal(this, title);
        TypeFaceUttils.setNomal(this, tv_applocker_service);
        TypeFaceUttils.setNomal(this, tv_applocker_service_decription);
        TypeFaceUttils.setNomal(this, title_1);
        TypeFaceUttils.setNomal(this, tv_edit_password);
        TypeFaceUttils.setNomal(this, tv_edit_password_deription);
        TypeFaceUttils.setNomal(this, tv_relock_policy);
        TypeFaceUttils.setNomal(this, tv_relock_policy_decription);
        TypeFaceUttils.setNomal(this, tv_relock_timeout);
        TypeFaceUttils.setNomal(this, tv_relock_timeout_deription);
        TypeFaceUttils.setNomal(this, tv_selfie);
        TypeFaceUttils.setNomal(this, tv_selfie_deription);
        TypeFaceUttils.setNomal(this, tv_vibrate);
        TypeFaceUttils.setNomal(this, tv_vibrate_deription);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_app_lock_settings;
    }

    private boolean bound;
    private MonitorShieldService monitorShieldService;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            bound = true;
            MonitorShieldService.MonitorShieldLocalBinder binder = (MonitorShieldService.MonitorShieldLocalBinder) service;
            monitorShieldService = binder.getServiceInstance();
            initView();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bound = false;
            monitorShieldService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customFont();
        sharedPreferences = getSharedPreferences(AppLockCreatePasswordActivity.SHARED_PREFERENCES_NAME, 0);
        bindService(new Intent(this, MonitorShieldService.class), serviceConnection, BIND_AUTO_CREATE);
    }

    private void initView() {
        checkbox_applocker_service.setChecked(sharedPreferences.getBoolean(AppLockCreatePasswordActivity.KEY_APPLOCKER_SERVICE, true));
        checkbox_applocker_service.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sharedPreferences.edit().putBoolean(AppLockCreatePasswordActivity.KEY_APPLOCKER_SERVICE, isChecked).apply();
                if (isChecked) {
                    monitorShieldService.startLockAppTask();
                } else {
                    monitorShieldService.stopLockAppTask();
                }
            }
        });

        item_edit_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppLockSettingsActivity.this, AppLockEditPasswordActivity.class));
            }
        });

        checkbox_relock_policy.setChecked(sharedPreferences.getBoolean(AppLockCreatePasswordActivity.KEY_RELOCK_POLICY, false));
        checkbox_relock_policy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sharedPreferences.edit().putBoolean(AppLockCreatePasswordActivity.KEY_RELOCK_POLICY, isChecked).apply();
                if (!isChecked) {
                    sharedPreferences.edit().putBoolean(LockService.KEY_UNLOCKED, false).apply();
                }
            }
        });

        item_selfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppLockSettingsActivity.this, AppLockImagesActivity.class));
            }
        });

        item_relock_timeout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(AppLockSettingsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_single_choice_items);
                WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
                params.width = WindowManager.LayoutParams.MATCH_PARENT;
                dialog.getWindow().setAttributes(params);

                RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radio_group);
                RadioButton rd_1 = (RadioButton) dialog.findViewById(R.id.rd_1);
                TypeFaceUttils.setNomal(AppLockSettingsActivity.this, rd_1);
                RadioButton rd_2 = (RadioButton) dialog.findViewById(R.id.rd_2);
                TypeFaceUttils.setNomal(AppLockSettingsActivity.this, rd_2);
                RadioButton rd_3 = (RadioButton) dialog.findViewById(R.id.rd_3);
                TypeFaceUttils.setNomal(AppLockSettingsActivity.this, rd_3);
                RadioButton rd_4 = (RadioButton) dialog.findViewById(R.id.rd_4);
                TypeFaceUttils.setNomal(AppLockSettingsActivity.this, rd_4);

                int lockmode = sharedPreferences.getInt(AppLockCreatePasswordActivity.KEY_RELOCK_TIMEOUT, 1);
                tv_relock_timeout_deription.setText(getLockOptionMode(lockmode));
                switch (lockmode) {
                    case 1:
                        radioGroup.check(R.id.rd_1);
                        break;
                    case 2:
                        radioGroup.check(R.id.rd_2);
                        break;
                    case 3:
                        radioGroup.check(R.id.rd_3);
                        break;
                    case 4:
                        radioGroup.check(R.id.rd_4);
                        break;
                }

                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        int values = 1;
                        switch (checkedId) {
                            case R.id.rd_1:
                                values = 1;
                                break;
                            case R.id.rd_2:
                                values = 2;
                                break;
                            case R.id.rd_3:
                                values = 3;
                                break;
                            case R.id.rd_4:
                                values = 4;
                                break;
                        }
                        sharedPreferences.edit().putInt(AppLockCreatePasswordActivity.KEY_RELOCK_TIMEOUT, values).apply();
                        tv_relock_timeout_deription.setText(getLockOptionMode(values));
                    }
                });

                dialog.show();
            }
        });

        checkbox_selfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkbox_selfie.isChecked()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        boolean permissionCamera = ContextCompat.checkSelfPermission(AppLockSettingsActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
                        boolean permissionRead = ContextCompat.checkSelfPermission(AppLockSettingsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                        boolean permissionWrite = ContextCompat.checkSelfPermission(AppLockSettingsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                        if (permissionCamera && permissionRead && permissionWrite) {
                            sharedPreferences.edit().putBoolean(KEY_SELFIE, true).apply();
                        } else {
                            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1122);
                        }
                    } else {
                        sharedPreferences.edit().putBoolean(KEY_SELFIE, true).apply();
                    }
                } else {
                    sharedPreferences.edit().putBoolean(KEY_SELFIE, false).apply();
                }
            }
        });

        checkbox_vibrate.setChecked(sharedPreferences.getBoolean(KEY_VIBRATE, false));
        checkbox_vibrate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sharedPreferences.edit().putBoolean(KEY_VIBRATE, isChecked).apply();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1122:
                boolean permissionCamera = ContextCompat.checkSelfPermission(AppLockSettingsActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
                boolean permissionRead = ContextCompat.checkSelfPermission(AppLockSettingsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                boolean permissionWrite = ContextCompat.checkSelfPermission(AppLockSettingsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                if (permissionCamera && permissionRead && permissionWrite) {
                    sharedPreferences.edit().putBoolean(KEY_SELFIE, true).apply();
                } else {
                    checkbox_selfie.setChecked(false );
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (bound && monitorShieldService != null) {
            unbindService(serviceConnection);
            bound = false;
        }
    }

    private String getLockOptionMode(int mode) {
        switch (mode) {
            case 1:
                return getResources().getString(R.string.after_screen_is_locked);
            case 2:
                return getResources().getString(R.string.after_3_minutes);
            case 3:
                return getResources().getString(R.string.after_5_minutes);
            case 4:
                return getResources().getString(R.string.lock_when_you_launch_an_app);
            default:
                return null;
        }
    }
}
