package com.noah.antivirus.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.databases.ImagesDatabaseHelper;
import com.noah.antivirus.model.Image;
import com.noah.antivirus.model.Selfie;
import com.noah.antivirus.service.LockService;
import com.noah.antivirus.util.Utils;
import com.takwolf.android.lock9.Lock9View;

import butterknife.BindView;

public class AppLockScreenActivity extends BaseToolbarActivity {
    private SharedPreferences sharedPreferences;
    private ImagesDatabaseHelper imagesDatabaseHelper;

    @BindView(R.id.lock_view)
    Lock9View lock_view;

    @BindView(R.id.layout)
    View layout;

    @BindView(R.id.tv_forgot_password)
    TextView tv_forgot_password;

    @BindView(R.id.layout_lock)
    View layout_lock;

    @BindView(R.id.lock_view_disvibrate)
    Lock9View lock_view_disvibrate;

    private int countFailed;

    @Override
    public int getLayoutId() {
        return R.layout.activity_app_lock_screen;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences(AppLockCreatePasswordActivity.SHARED_PREFERENCES_NAME, 0);
        imagesDatabaseHelper = ImagesDatabaseHelper.getInstance(this);

        lock_view.setCallBack(new Lock9View.CallBack() {
            @Override
            public void onFinish(String password) {
                finish(password);
            }
        });

        lock_view_disvibrate.setCallBack(new Lock9View.CallBack() {
            @Override
            public void onFinish(String password) {
                finish(password);
            }
        });

        if (sharedPreferences.getBoolean(AppLockSettingsActivity.KEY_VIBRATE, false)) {
            lock_view.setVisibility(View.VISIBLE);
            lock_view_disvibrate.setVisibility(View.GONE);
        } else {
            lock_view.setVisibility(View.GONE);
            lock_view_disvibrate.setVisibility(View.VISIBLE);
        }

        tv_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppLockScreenActivity.this, AppLockForgotPasswordActivity.class));
            }
        });
    }

    private void finish(String password) {
        if (password.equals(sharedPreferences.getString(AppLockCreatePasswordActivity.KEY_PASSWORD, null))) {
            startActivity(new Intent(AppLockScreenActivity.this, AppLockHomeActivity.class));

            // Push notification
            if (sharedPreferences.getBoolean(LockService.KEY_THIEVES, false)) {
                Image image = imagesDatabaseHelper.findByID(sharedPreferences.getLong(LockService.KEY_APP_THIEVES, -1));
                if (image != null) {
                    Intent intent = new Intent(AppLockScreenActivity.this, AppLockImageActivity.class);
                    intent.putExtra("id", image.getId());
                    Utils.notificateAppLock(AppLockScreenActivity.this, LockService.NOTIFICATION_ID_APP_LOCK, R.mipmap.ic_thieves, "Someone tries to open your app.", image.getAppName(), "Someone tries to open your app.", intent);
                }
            }
            sharedPreferences.edit().putBoolean(LockService.KEY_THIEVES, false).apply();

            finish();
        } else {
            ++countFailed;
            if (countFailed == 3) {
                if (sharedPreferences.getBoolean(AppLockSettingsActivity.KEY_SELFIE, false)) {
                    (new Selfie(AppLockScreenActivity.this, getPackageName())).takePhoto();
                }
            }

                Snackbar snackbar = Snackbar.make(layout, R.string.patterns_do_not_match, Snackbar.LENGTH_SHORT)
                        .setAction(getResources().getString(R.string.forgot_password), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(AppLockScreenActivity.this, AppLockForgotPasswordActivity.class));
                            }
                        });
            snackbar.setActionTextColor(Color.CYAN);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_action);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            snackbar.show();
        }
    }
}
