package com.noah.antivirus.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.NativeExpressAdView;
import com.noah.antivirus.R;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.fragment.ApplicationFragment;
import com.noah.antivirus.fragment.JunkFilesFragment;
import com.noah.antivirus.fragment.PhoneBoostFragment;
import com.noah.antivirus.fragment.ProblemDetailFragment;
import com.noah.antivirus.fragment.ResultAppLockFragment;
import com.noah.antivirus.iface.Communicator;
import com.noah.antivirus.iface.IProblem;
import com.noah.antivirus.model.Application;
import com.noah.antivirus.service.MonitorShieldService;
import com.noah.antivirus.util.ID;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.util.Utils;
import com.noah.antivirus.model.AppLock;
import com.noah.antivirus.model.JunkOfApplication;

import java.util.List;

import butterknife.BindView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ScanningResultActivity extends BaseToolbarActivity implements View.OnClickListener, Communicator {
    public static final String RESULT_APPLOCK_FRAGMENT_TAG = "APPLOCK";
    public static final String RESULT_PHONEBOOST_FRAGMENT_TAG = "PHONEBOOST";
    public static final String RESULT_JUNKS_FRAGMENT_TAG = "JUNKS";
    public static final String RESULT_APPLICATION_FRAGMENT_TAG = "APPLICATION";
    public static final String PROBLEM_DETAIL_FRAGMENT_TAG = "PROBLEM_DETAIL";

    @BindView(R.id.img_app_lock)
    ImageView img_app_lock;

    @BindView(R.id.img_app_lock_1)
    ImageView img_app_lock_1;

    @BindView(R.id.img_app_lock_2)
    ImageView img_app_lock_2;

    @BindView(R.id.img_app_lock_3)
    ImageView img_app_lock_3;

    @BindView(R.id.tv_app_lock)
    TextView tv_app_lock;

    @BindView(R.id.result_app_lock)
    View result_app_lock;

    @BindView(R.id.result_application)
    View result_application;

    @BindView(R.id.tv_application)
    TextView tv_application;

    @BindView(R.id.tv_phone_boost)
    TextView tv_phone_boost;

    @BindView(R.id.result_booster)
    View result_booster;

    @BindView(R.id.tv_num_of_issues)
    TextView tv_num_of_issues;

    @BindView(R.id.result_junk_files)
    View result_junk_files;

    @BindView(R.id.tv_junk_files_size)
    TextView tv_junk_files_size;

    @BindView(R.id.img_application)
    ImageView img_application;

    @BindView(R.id.img_application_1)
    ImageView img_application_1;

    @BindView(R.id.img_application_2)
    ImageView img_application_2;

    @BindView(R.id.img_application_3)
    ImageView img_application_3;

    @BindView(R.id.tv_detecting_dangerous)
    TextView tv_detecting_dangerous;

    @BindView(R.id.tv_title_application)
    TextView tv_title_application;

    @BindView(R.id.tv_title_phone_boost)
    TextView tv_title_phone_boost;

    @BindView(R.id.tv_mb_phone_boost)
    TextView tv_mb_phone_boost;

    @BindView(R.id.tv_freeable_memory)
    TextView tv_freeable_memory;

    @BindView(R.id.tv_title_junk_files)
    TextView tv_title_junk_files;

    @BindView(R.id.tv_junk_found)
    TextView tv_junk_found;

    @BindView(R.id.tv_mb_junk_files)
    TextView tv_mb_junk_files;

    @BindView(R.id.scanning_result_layout)
    View scanning_result_layout;

    @BindView(R.id.result_layout)
    View result_layout;

    @BindView(R.id.ads_container)
    FrameLayout ads_container;

    private NativeExpressAdView mNativeExpressAdView;

    public void setBackground(int id) {
        scanning_result_layout.setBackgroundResource(id);
    }

    private boolean showAd = true;

    public void setShowAd(boolean showAd) {
        this.showAd = showAd;
    }

    private IProblem comu;

    public IProblem getComu() {
        return comu;
    }

    public void setComu(IProblem comu) {
        this.comu = comu;
    }

    private boolean app_skip_all;

    public void setApp_skip_all(boolean app_skip_all) {
        this.app_skip_all = app_skip_all;
    }

    private void customFont() {
        TypeFaceUttils.setNomal(this, tv_app_lock);
        TypeFaceUttils.setNomal(this, tv_detecting_dangerous);
        TypeFaceUttils.setNomal(this, tv_title_application);
        TypeFaceUttils.setNomal(this, tv_application);
        TypeFaceUttils.setNomal(this, tv_title_phone_boost);
        TypeFaceUttils.setNomal(this, tv_phone_boost);
        TypeFaceUttils.setNomal(this, tv_mb_phone_boost);
        TypeFaceUttils.setNomal(this, tv_freeable_memory);
        TypeFaceUttils.setNomal(this, tv_title_junk_files);
        TypeFaceUttils.setNomal(this, tv_junk_found);
        TypeFaceUttils.setNomal(this, tv_junk_files_size);
        TypeFaceUttils.setNomal(this, tv_mb_junk_files);
        TypeFaceUttils.setNomal(this, tv_num_of_issues);
    }

    private boolean bound;
    private MonitorShieldService monitorShieldService;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            bound = true;
            MonitorShieldService.MonitorShieldLocalBinder binder = (MonitorShieldService.MonitorShieldLocalBinder) service;
            monitorShieldService = binder.getServiceInstance();
            new Init().execute();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bound = false;
            monitorShieldService = null;
        }
    };

    public MonitorShieldService getMonitorShieldService() {
        return monitorShieldService;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_scanning_result;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                    result_layout.setVisibility(View.VISIBLE);
                } else {
                    result_layout.setVisibility(View.GONE);
                }
            }
        });

        customFont();
        new LoadIcon().execute();

        result_application.setOnClickListener(this);
        result_booster.setOnClickListener(this);
        result_junk_files.setOnClickListener(this);
        result_app_lock.setOnClickListener(this);

        loadAds();
    }

    @Override
    public void respond(IProblem iProblem, int type) {
        ApplicationFragment fragment = (ApplicationFragment) getSupportFragmentManager().findFragmentByTag(ScanningResultActivity.RESULT_APPLICATION_FRAGMENT_TAG);
        if (fragment != null) fragment.deleteItem(iProblem, type);
    }

    private class LoadIcon extends AsyncTask<Void, Drawable, Void> {
        private int count;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            img_app_lock.setImageResource(R.mipmap.ic_launcher);
            img_app_lock_1.setImageResource(R.mipmap.ic_launcher);
            img_app_lock_2.setImageResource(R.mipmap.ic_launcher);
            img_app_lock_3.setImageResource(R.mipmap.ic_launcher);
            img_application.setImageResource(R.mipmap.ic_launcher);
            img_application_1.setImageResource(R.mipmap.ic_launcher);
            img_application_2.setImageResource(R.mipmap.ic_launcher);
            img_application_3.setImageResource(R.mipmap.ic_launcher);
        }

        @Override
        protected Void doInBackground(Void... params) {
            List<PackageInfo> allPackages = Utils.getApps(ScanningResultActivity.this, PackageManager.GET_ACTIVITIES | PackageManager.GET_PERMISSIONS);
            List<PackageInfo> nonSystemApps = Utils.getNonSystemApps(ScanningResultActivity.this, allPackages);
            int maxSize = nonSystemApps.size();
            if (maxSize != 0) {
                Drawable drawable = Utils.getIconFromPackage(nonSystemApps.get((int) (Math.random() * maxSize)).packageName, ScanningResultActivity.this);
                publishProgress(drawable);
                Drawable drawable_1 = Utils.getIconFromPackage(nonSystemApps.get((int) (Math.random() * maxSize)).packageName, ScanningResultActivity.this);
                publishProgress(drawable_1);
                Drawable drawable_2 = Utils.getIconFromPackage(nonSystemApps.get((int) (Math.random() * maxSize)).packageName, ScanningResultActivity.this);
                publishProgress(drawable_2);
                Drawable drawable_3 = Utils.getIconFromPackage(nonSystemApps.get((int) (Math.random() * maxSize)).packageName, ScanningResultActivity.this);
                publishProgress(drawable_3);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Drawable... values) {
            super.onProgressUpdate(values);
            switch (count) {
                case 0:
                    img_application.setImageDrawable(values[0]);
                    img_app_lock.setImageDrawable(values[0]);
                    ++count;
                    break;
                case 1:
                    img_application_1.setImageDrawable(values[0]);
                    img_app_lock_1.setImageDrawable(values[0]);
                    ++count;
                    break;
                case 2:
                    img_application_2.setImageDrawable(values[0]);
                    img_app_lock_2.setImageDrawable(values[0]);
                    ++count;
                    break;
                case 3:
                    img_application_3.setImageDrawable(values[0]);
                    img_app_lock_3.setImageDrawable(values[0]);
                    break;
            }
        }
    }

    private class Init extends AsyncTask<Void, Integer, Void> {
        int item = 1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            tv_num_of_issues.setText(getResources().getString(R.string.found) + " " + monitorShieldService.getMenacesCacheSet().getItemCount() + " " + getResources().getString(R.string.issues));
            result_app_lock.setVisibility(View.GONE);
            result_application.setVisibility(View.GONE);
            result_booster.setVisibility(View.GONE);
            result_junk_files.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            int countApp = 0;
            for (AppLock appLock : monitorShieldService.getAppLock()) {
                if (appLock.isRecommend()) ++countApp;
            }
            publishProgress(countApp);

            publishProgress(monitorShieldService.getMenacesCacheSet().getItemCount());

            int totalBoost = 0;
            List<Application> runningApplications = monitorShieldService.getRunningApplications();

            for (Application application : runningApplications) {
                totalBoost += application.getSize() / 1024;
            }

            long cacheSize = 0;
            for (JunkOfApplication junkOfApplication : Utils.junkOfApplications) {
                cacheSize += junkOfApplication.getCacheSize();
            }
            publishProgress(totalBoost, (int) cacheSize / (1024 * 1024));
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            switch (item) {
                case 1:
                    if (values[0] != 0) {
                        String number = "<font color='#cc0000'>" + values[0] + "</font>";
                        tv_app_lock.setText(Html.fromHtml(getResources().getString(R.string.more_than) + " " + number + " " + getResources().getString(R.string.apps_with_privacy_issues) + "."));
                        result_app_lock.setVisibility(View.VISIBLE);
                    } else {
                        result_app_lock.setVisibility(View.GONE);
                    }
                    ++item;
                    break;
                case 2:
                    if (values[0] == 0 || app_skip_all) {
                        result_application.setVisibility(View.GONE);
                        tv_application.setText(String.valueOf(Utils.numOfPackages));
                    } else {
                        result_application.setVisibility(View.VISIBLE);
                        tv_application.setText(String.valueOf(Utils.numOfPackages));
                    }
                    ++item;
                    break;
                case 3:
                    if (values[0] == 0) {
                        result_booster.setVisibility(View.GONE);
                    } else {
                        result_booster.setVisibility(View.VISIBLE);
                        tv_phone_boost.setText(String.valueOf(values[0]));
                    }

                    if (values[1] == 0) {
                        result_junk_files.setVisibility(View.GONE);
                    } else {
                        result_junk_files.setVisibility(View.VISIBLE);
                        tv_junk_files_size.setText(String.valueOf(values[1]));
                    }
                    ++item;
                    break;
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private boolean isSafeSate() {
        boolean application = (monitorShieldService.getMenacesCacheSet().getItemCount() == 0);
        if (app_skip_all) application = true;

        boolean phoneBoost = (monitorShieldService.getRunningApplications().size() == 0);

        long cacheSize = 0;
        for (JunkOfApplication junkOfApplication : Utils.junkOfApplications) {
            cacheSize += junkOfApplication.getCacheSize();
        }
        boolean junk = (cacheSize / (1024 * 1024) == 0);

        int countApp = 0;
        for (AppLock appLock : monitorShieldService.getAppLock()) {
            if (appLock.isRecommend()) ++countApp;
        }
        boolean protect = countApp == 0;

        return (application && phoneBoost && junk && protect);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.result_app_lock:
                slideInFragment(ScanningResultActivity.RESULT_APPLOCK_FRAGMENT_TAG);
                break;
            case R.id.result_application:
                slideInFragment(ScanningResultActivity.RESULT_APPLICATION_FRAGMENT_TAG);
                break;
            case R.id.result_booster:
                slideInFragment(ScanningResultActivity.RESULT_PHONEBOOST_FRAGMENT_TAG);
                break;
            case R.id.result_junk_files:
                slideInFragment(ScanningResultActivity.RESULT_JUNKS_FRAGMENT_TAG);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindService(new Intent(this, MonitorShieldService.class), serviceConnection, BIND_AUTO_CREATE);

        // Resume the NativeExpressAdView.
        mNativeExpressAdView.resume();
    }

    @Override
    protected void onPause() {
        // Pause the NativeExpressAdView.
        mNativeExpressAdView.pause();

        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        scanning_result_layout.setBackgroundResource(R.drawable.background_danger);
    }

    public ResultAppLockFragment getResultAppLockFragment() {
        FragmentManager fm = getSupportFragmentManager();
        ResultAppLockFragment f = (ResultAppLockFragment) fm.findFragmentByTag(RESULT_APPLOCK_FRAGMENT_TAG);

        if (f == null)
            return new ResultAppLockFragment();
        else
            return f;
    }

    public PhoneBoostFragment getPhoneBoostFragment() {
        FragmentManager fm = getSupportFragmentManager();
        PhoneBoostFragment f = (PhoneBoostFragment) fm.findFragmentByTag(RESULT_PHONEBOOST_FRAGMENT_TAG);

        if (f == null)
            return new PhoneBoostFragment();
        else
            return f;
    }

    public JunkFilesFragment getJunkFilesFragment() {
        FragmentManager fm = getSupportFragmentManager();
        JunkFilesFragment f = (JunkFilesFragment) fm.findFragmentByTag(RESULT_JUNKS_FRAGMENT_TAG);

        if (f == null)
            return new JunkFilesFragment();
        else
            return f;
    }

    public ApplicationFragment getApplicationFragment() {
        FragmentManager fm = getSupportFragmentManager();
        ApplicationFragment f = (ApplicationFragment) fm.findFragmentByTag(RESULT_APPLICATION_FRAGMENT_TAG);

        if (f == null)
            return new ApplicationFragment();
        else
            return f;
    }

    public ProblemDetailFragment getProblemDetailFragment() {
        FragmentManager fm = getSupportFragmentManager();
        ProblemDetailFragment f = (ProblemDetailFragment) fm.findFragmentByTag(PROBLEM_DETAIL_FRAGMENT_TAG);

        if (f == null)
            return new ProblemDetailFragment();
        else
            return f;
    }

    public Fragment slideInFragment(String fragmentId) {
        Fragment fmt = getSupportFragmentManager().findFragmentByTag(fragmentId);
        if (fmt != null && fmt.isVisible())
            return null;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right);


        Fragment f = null;
        switch (fragmentId) {
            case RESULT_APPLOCK_FRAGMENT_TAG:
                f = getResultAppLockFragment();
                break;
            case RESULT_PHONEBOOST_FRAGMENT_TAG:
                f = getPhoneBoostFragment();
                break;
            case RESULT_JUNKS_FRAGMENT_TAG:
                f = getJunkFilesFragment();
                break;
            case RESULT_APPLICATION_FRAGMENT_TAG:
                f = getApplicationFragment();
                break;
            case PROBLEM_DETAIL_FRAGMENT_TAG:
                f = getProblemDetailFragment();
                break;
            default:
        }

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack if needed
        transaction.replace(R.id.container_fragment, f, fragmentId);
        transaction.addToBackStack(fragmentId);

        // Commit the transaction (Si no lo hago con commitAllowingStateLoss se peta la app
        // https://www.google.es/search?q=android+create+list+inplace&ie=utf-8&oe=utf-8&gws_rd=cr&ei=1J20VrXjFIScUYuXt6gI
        transaction.commitAllowingStateLoss();

        return f;
    }

    public Fragment shareElementFragment(Fragment fragmentOne, ImageView startImage) {
        Fragment f;
        f = getProblemDetailFragment();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Inflate transitions to apply
            Transition changeTransform = TransitionInflater.from(this).
                    inflateTransition(R.transition.change_image_transform);
            Transition explodeTransform = TransitionInflater.from(this).
                    inflateTransition(android.R.transition.explode);

            // Setup exit transition on first fragment
            fragmentOne.setSharedElementReturnTransition(changeTransform);
            fragmentOne.setExitTransition(explodeTransform);

            // Setup enter transition on second fragment
            f.setSharedElementEnterTransition(changeTransform);
            f.setEnterTransition(explodeTransform);

            // Add second fragment by replacing first
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_fragment, f, PROBLEM_DETAIL_FRAGMENT_TAG)
                    .addToBackStack(PROBLEM_DETAIL_FRAGMENT_TAG)
                    .addSharedElement(startImage, getString(R.string.transition_problem_to_detail));
            // Apply the transaction
            ft.commitAllowingStateLoss();
        } else {
            Fragment fmt = getSupportFragmentManager().findFragmentByTag(PROBLEM_DETAIL_FRAGMENT_TAG);
            if (fmt != null && fmt.isVisible())
                return null;

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack if needed
            transaction.replace(R.id.container_fragment, f, PROBLEM_DETAIL_FRAGMENT_TAG);
            transaction.addToBackStack(PROBLEM_DETAIL_FRAGMENT_TAG);

            // Commit the transaction (Si no lo hago con commitAllowingStateLoss se peta la app
            // https://www.google.es/search?q=android+create+list+inplace&ie=utf-8&oe=utf-8&gws_rd=cr&ei=1J20VrXjFIScUYuXt6gI
            transaction.commitAllowingStateLoss();
        }

        return f;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        // Destroy the NativeExpressAdView.
        mNativeExpressAdView.destroy();

        super.onDestroy();

        if (bound && monitorShieldService != null) {
            unbindService(serviceConnection);
            bound = false;
        }
    }

    private void loadAds() {
        final InterstitialAd mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(ID.INTERSTITIAL_AD);
        AdRequest interstitialAdRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(interstitialAdRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                if (mInterstitialAd.isLoaded() && showAd) {
                    mInterstitialAd.show();
                }
            }
        });

        mNativeExpressAdView = new NativeExpressAdView(this);
        mNativeExpressAdView.setAdSize(new AdSize(AdSize.FULL_WIDTH, 150));
        mNativeExpressAdView.setAdUnitId(ID.NATIVE_AD_SCANED);
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
        ads_container.addView(mNativeExpressAdView);
        mNativeExpressAdView.loadAd(adRequestBuilder.build());
    }

    public void refresh() {
        if (isSafeSate()) {
            int rate = getSharedPreferences("Settings", 0).getInt("rate", 0);
            if (rate == 0) {
                getSharedPreferences("Settings", 0).edit().putInt("rate", 1).apply();
            }
            Intent intent = new Intent(this, SafeActivity.class);
            intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.you_are_safe));
            startActivity(intent);
            finish();
        } else {
            new Init().execute();
        }
    }
}
