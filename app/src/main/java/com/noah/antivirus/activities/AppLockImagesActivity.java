package com.noah.antivirus.activities;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.noah.antivirus.R;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.databases.ImagesDatabaseHelper;
import com.noah.antivirus.adapter.ImagesAdapter;
import com.noah.antivirus.model.Image;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;

public class AppLockImagesActivity extends BaseToolbarActivity {
    private ImagesDatabaseHelper imagesDatabaseHelper;
    private List<Image> images;

    @BindView(R.id.rv_images)
    RecyclerView rv_images;

    @Override
    public int getLayoutId() {
        return R.layout.activity_app_lock_images;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imagesDatabaseHelper = ImagesDatabaseHelper.getInstance(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    private void initView() {
        rv_images.setLayoutManager(new GridLayoutManager(this, 2));
        rv_images.setHasFixedSize(true);
        images = imagesDatabaseHelper.getAllImages();
        Collections.sort(images, Collections.reverseOrder());
        ImagesAdapter adapter = new ImagesAdapter(this, images);
        rv_images.setAdapter(adapter);
    }
}
