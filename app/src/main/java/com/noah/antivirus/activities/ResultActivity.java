package com.noah.antivirus.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.adapter.ResultAdapter;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.fragment.ResloveProblemDetailsFragment;
import com.noah.antivirus.iface.IProblem;
import com.noah.antivirus.iface.IResultItemSelectedListener;
import com.noah.antivirus.service.MonitorShieldService;
import com.noah.antivirus.util.TypeFaceUttils;

import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by HuyLV-CT on 7/2/2016.
 */
public class ResultActivity extends BaseToolbarActivity {
    public static final String PROBLEM_DETAIL_FRAGMENT_TAG = "PROBLEM_DETAIL";
    public static final int REQUEST_DETAIL_PROBLEM = 3;

    @BindView(R.id.rv_scan_result)
    RecyclerView rv_scan_result;

    ResultAdapter adapter;

    @BindView(R.id.tv_num_of_issues)
    TextView tv_num_of_issues;

    @BindView(R.id.framelayout_skip_all)
    View framelayout_skip_all;

    @BindView(R.id.tv_skip_all)
    TextView tv_skip_all;

    @BindView(R.id.result_layout)
    View result_layout;

    private void customFont() {
        TypeFaceUttils.setNomal(this, tv_num_of_issues);
        TypeFaceUttils.setNomal(this, tv_skip_all);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_scan_result;
    }

    private boolean bound;
    private MonitorShieldService monitorShieldService;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            bound = true;
            MonitorShieldService.MonitorShieldLocalBinder binder = (MonitorShieldService.MonitorShieldLocalBinder) service;
            monitorShieldService = binder.getServiceInstance();

            init();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bound = false;
            monitorShieldService = null;
        }
    };

    public MonitorShieldService getMonitorShieldService() {
        return monitorShieldService;
    }

    private IProblem comu;

    public IProblem getComu() {
        return comu;
    }

    public void setComu(IProblem comu) {
        this.comu = comu;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        bindService(new Intent(this, MonitorShieldService.class), serviceConnection, BIND_AUTO_CREATE);

        customFont();

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                    result_layout.setVisibility(View.VISIBLE);
                } else {
                    result_layout.setVisibility(View.GONE);
                }
            }
        });
    }

    private void init() {
        tv_num_of_issues.setText(getResources().getString(R.string.found) + " " + monitorShieldService.getMenacesCacheSet().getItemCount() + " " + getResources().getString(R.string.issues));

        adapter = new ResultAdapter(this, new ArrayList<>(monitorShieldService.getMenacesCacheSet().getSet()));
        rv_scan_result.setAdapter(adapter);
        rv_scan_result.setLayoutManager(new StickyHeaderLayoutManager());


        adapter.setResultItemSelectedStateChangedListener(new IResultItemSelectedListener() {
            @Override
            public void onItemSelected(IProblem bpdw, ImageView iv_icon_app, Context c) {
                setComu(bpdw);

                FragmentManager fm = getSupportFragmentManager();
                ResloveProblemDetailsFragment f = (ResloveProblemDetailsFragment) fm.findFragmentByTag(PROBLEM_DETAIL_FRAGMENT_TAG);
                if (f == null) f = new ResloveProblemDetailsFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                transaction.replace(R.id.container, f, PROBLEM_DETAIL_FRAGMENT_TAG);
                transaction.addToBackStack(PROBLEM_DETAIL_FRAGMENT_TAG);
                transaction.commitAllowingStateLoss();
            }
        });

        framelayout_skip_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, SafeActivity.class);
                intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.you_are_safe));
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (bound && monitorShieldService != null) {
            unbindService(serviceConnection);
            bound = false;
        }
        super.onDestroy();
    }

    public void refresh(IProblem iProblem) {
        adapter.remove(iProblem);
        tv_num_of_issues.setText(getResources().getString(R.string.found) + " " + monitorShieldService.getMenacesCacheSet().getItemCount() + " " + getResources().getString(R.string.issues));
        if (monitorShieldService.getMenacesCacheSet().getItemCount() == 0) {
            Intent intent = new Intent(this, SafeActivity.class);
            intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.you_are_safe));
            startActivity(intent);
            finish();
        }
    }
}
