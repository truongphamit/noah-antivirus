package com.noah.antivirus.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.widget.TextView;

import com.liulishuo.magicprogresswidget.MagicProgressBar;
import com.noah.antivirus.R;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.util.RootUtil;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.util.Utils;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;

public class PhoneInfoActivity extends BaseToolbarActivity {
    public static final int PHONE_INFO_REQUEST_CODE = 2345;

    @BindView(R.id.pb_cpu)
    MagicProgressBar pb_cpu;

    @BindView(R.id.pb_ram)
    MagicProgressBar pb_ram;

    @BindView(R.id.pb_storage)
    MagicProgressBar pb_storage;

    @BindView(R.id.tv_title_cpu)
    TextView tv_title_cpu;

    @BindView(R.id.tv_title_ram)
    TextView tv_title_ram;

    @BindView(R.id.tv_title_storage)
    TextView tv_title_storage;

    @BindView(R.id.tv_basic_information)
    TextView tv_basic_information;

    @BindView(R.id.tv_title_system_os_version)
    TextView tv_title_system_os_version;

    @BindView(R.id.tv_title_imei)
    TextView tv_title_imei;

    @BindView(R.id.tv_title_root_state)
    TextView tv_title_root_state;

    @BindView(R.id.tv_imei)
    TextView tv_imei;

    private TextView tvInforStorage;
    private TextView tvInforRam;
    private TextView tvInfoCPU;

    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();

    @Override
    public int getLayoutId() {
        return R.layout.activity_phone_info;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView tvTitle = (TextView) findViewById(R.id.toolbar_title);
        tvTitle.setText(getResources().getString(R.string.phone_info));

        initView();
        startTimer();
    }

    private void initView() {
        TextView tvSystOSVersion = (TextView) findViewById(R.id.tv_system_os_version);
        tvSystOSVersion.setText("Android " + String.valueOf(Build.VERSION.RELEASE));

        TextView tvRootState = (TextView) findViewById(R.id.tv_root_state);
        if (RootUtil.isDeviceRooted()) {
            tvRootState.setText("Rooted");
        } else {
            tvRootState.setText("Not Rooted");
        }

        TypeFaceUttils.setNomal(this, tv_imei);
        if (Build.VERSION.SDK_INT < 23) {
            tv_imei.setText(((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
        } else {
            boolean permission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;
            if (!permission) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
            } else {
                tv_imei.setText(((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
            }
        }

        tvInforStorage = (TextView) findViewById(R.id.tv_info_storage);

        tvInforRam = (TextView) findViewById(R.id.tv_info_ram);

        tvInfoCPU = (TextView) findViewById(R.id.tv_info_cpu);

        TypeFaceUttils.setNomal(this, tv_title_cpu);
        TypeFaceUttils.setNomal(this, tv_title_ram);
        TypeFaceUttils.setNomal(this, tv_title_storage);
        TypeFaceUttils.setNomal(this, tvInfoCPU);
        TypeFaceUttils.setNomal(this, tvInforRam);
        TypeFaceUttils.setNomal(this, tvInforStorage);
        TypeFaceUttils.setNomal(this, tv_basic_information);
        TypeFaceUttils.setNomal(this, tv_title_system_os_version);
        TypeFaceUttils.setNomal(this, tvSystOSVersion);
        TypeFaceUttils.setNomal(this, tv_title_imei);
        TypeFaceUttils.setNomal(this, tv_title_root_state);
        TypeFaceUttils.setNomal(this, tvRootState);

        long totalInternalMemorySize = Utils.getTotalInternalMemorySize();
        long availableInternalMemorySize = Utils.getAvailableInternalMemorySize();
        tvInforStorage.setText(Utils.formatSize(totalInternalMemorySize - availableInternalMemorySize));
        pb_storage.setSmoothPercent((float) (totalInternalMemorySize - Utils.getAvailableInternalMemorySize()) / totalInternalMemorySize);

        long freeRam = Utils.getFreeRAM(this);
        long totalRam = Utils.getTotalRAM(this);
        tvInforRam.setText(Utils.formatSize(totalRam - freeRam));
        pb_ram.setSmoothPercent(((float) (totalRam - freeRam) / totalRam));
    }


    public void startTimer() {
        //set a new Timer
        timer = new Timer();
        //initialize the TimerTask's job
        initializeTimerTask();
        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 500, 1000); //
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        int cpu = (int) (readUsage() * 100);
                        tvInfoCPU.setText(cpu + "%");
                        pb_cpu.setSmoothPercent((float) (cpu / 100.0));
                    }
                });
            }
        };
    }


    private float readUsage() {
        try {
            RandomAccessFile reader = new RandomAccessFile("/proc/stat", "r");
            String load = reader.readLine();

            String[] toks = load.split(" +");  // Split on one or more spaces

            long idle1 = Long.parseLong(toks[4]);
            long cpu1 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            try {
                Thread.sleep(360);
            } catch (Exception e) {
            }

            reader.seek(0);
            load = reader.readLine();
            reader.close();

            toks = load.split(" +");

            long idle2 = Long.parseLong(toks[4]);
            long cpu2 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            return (float) (cpu2 - cpu1) / ((cpu2 + idle2) - (cpu1 + idle1));

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                tv_imei.setText(((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                stoptimertask();
                finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stoptimertask();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stoptimertask();
    }
}
