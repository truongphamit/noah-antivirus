package com.noah.antivirus.activities;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.liulishuo.magicprogresswidget.MagicProgressBar;
import com.noah.antivirus.R;
import com.noah.antivirus.base.BaseNavigationDrawerActivity;
import com.noah.antivirus.model.AppData;
import com.noah.antivirus.util.ID;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.service.MonitorShieldService;
import com.noah.antivirus.util.Utils;
import com.noah.antivirus.wifisecurity.provider.WifiSecuritySharePreference;

import butterknife.BindView;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends BaseNavigationDrawerActivity {

    @BindView(R.id.iv_start_scan_anim)
    ImageView iv_start_scan;

    @BindView(R.id.tv_info_storage)
    TextView tv_info_storage;

    @BindView(R.id.tv_info_memory)
    TextView tv_info_memory;

    @BindView(R.id.tv_percent_memory)
    TextView tv_percent_memory;

    @BindView(R.id.tv_percent_storage)
    TextView tv_percent_storage;

    @BindView(R.id.pb_storage)
    MagicProgressBar pb_storage;

    @BindView(R.id.pb_memory)
    MagicProgressBar pb_memory;

    @BindView(R.id.notifi_safe)
    View notifi_safe;

    @BindView(R.id.noti_danger)
    View noti_danger;

    @BindView(R.id.tv_found_problem)
    TextView tv_found_problem;

    @BindView(R.id.img_resolvep_roblems)
    ImageView img_resolvep_roblems;

    @BindView(R.id.tv_first_run)
    TextView tv_first_run;

    @BindView(R.id.bg_animation_scan)
    ImageView bg_animation_scan;

    @BindView(R.id.tv_scan)
    TextView tv_scan;

    @BindView(R.id.tv_app_system)
    TextView tv_app_system;

    @BindView(R.id.tv_storage)
    TextView tv_storage;

    @BindView(R.id.tv_memory)
    TextView tv_memory;

    @BindView(R.id.tv_safe)
    TextView tv_safe;

    @BindView(R.id.tv_danger)
    TextView tv_danger;

    @BindView(R.id.ads_container)
    FrameLayout ads_container;

    @OnClick(R.id.iv_start_scan)
    void onStartScan() {
        Intent i = new Intent(this, ScanningActivity.class);
        startActivity(i);
    }

    private void customFont() {
        TypeFaceUttils.setNomal(this, tv_scan);
        TypeFaceUttils.setNomal(this, tv_app_system);
        TypeFaceUttils.setNomal(this, tv_storage);
        TypeFaceUttils.setNomal(this, tv_memory);
        TypeFaceUttils.setNomal(this, tv_percent_storage);
        TypeFaceUttils.setNomal(this, tv_percent_memory);
        TypeFaceUttils.setNomal(this, tv_info_storage);
        TypeFaceUttils.setNomal(this, tv_info_memory);
        TypeFaceUttils.setNomal(this, tv_first_run);
        TypeFaceUttils.setNomal(this, tv_safe);
        TypeFaceUttils.setNomal(this, tv_danger);
        TypeFaceUttils.setNomal(this, tv_found_problem);
    }

    private View view;

    private AdView adView;
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        loadAds();

        customFont();
        view = findViewById(R.id.background);

        iv_start_scan = (ImageView) view.findViewById(R.id.iv_start_scan_anim);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.animation);
        iv_start_scan.startAnimation(animation);

        Animation bgAnimationScan = AnimationUtils.loadAnimation(this, R.anim.bg_animation_scan);
        bg_animation_scan.setAnimation(bgAnimationScan);

        img_resolvep_roblems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ResultActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
//        Log.e("Scan", "CACHE SCANNING-------------------------------------");
        Utils.getCacheSize(this);

        getMemoryInfo();

        Intent i = new Intent(this, MonitorShieldService.class);
        //Start services
        if (!Utils.isServiceRunning(this, MonitorShieldService.class))
            startService(i);
        //Bind to service
        bindService(i, serviceConnection, Context.BIND_AUTO_CREATE);

        if (getSharedPreferences("Settings", 0).getInt("rate", 0) == 1) {
            showRateDialog();
        }

        adView.resume();
    }



    private void getMemoryInfo() {
        long totalInternalMemorySize = Utils.getTotalInternalMemorySize();
        long availableInternalMemorySize = Utils.getAvailableInternalMemorySize();
        tv_info_storage.setText(Utils.formatSize(totalInternalMemorySize - availableInternalMemorySize) + "/" + Utils.formatSize(totalInternalMemorySize));
        int percent_storage = (int) ((totalInternalMemorySize - availableInternalMemorySize) * 100 / totalInternalMemorySize);
        tv_percent_storage.setText(String.valueOf((percent_storage) + "%"));

        long freeRam = Utils.getFreeRAM(this);
        long totalRam = Utils.getTotalRAM(this);
        tv_info_memory.setText(Utils.formatSize(totalRam - freeRam) + "/" + Utils.formatSize(totalRam));
        int percent_memory = (int) ((totalRam - freeRam) * 100 / totalRam);
        tv_percent_memory.setText(String.valueOf(percent_memory) + "%");

        pb_storage.setSmoothPercent((float) (percent_storage / 100.0));
        pb_memory.setSmoothPercent((float) (percent_memory / 100.0));
    }

    private void showRateDialog() {
        getSharedPreferences("Settings", 0).edit().putInt("rate", 2).apply();
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_rate);
        dialog.show();

        ImageView btnClose = (ImageView) dialog.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ImageView btnRate = (ImageView) dialog.findViewById(R.id.btn_rate);
        btnRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=" + getPackageName()));
                startActivity(intent);
                dialog.dismiss();
            }
        });
    }

    public void setBackgroungDanger() {
        view.setBackgroundResource(R.drawable.background_danger);
    }

    public void setBackgroungSafe() {
        view.setBackgroundResource(R.drawable.settings_background);
    }

    boolean bound = false;
    private MonitorShieldService monitorShieldService;

    public MonitorShieldService getMonitorShieldService() {
        return monitorShieldService;
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            bound = true;
            MonitorShieldService.MonitorShieldLocalBinder binder = (MonitorShieldService.MonitorShieldLocalBinder) service;
            monitorShieldService = binder.getServiceInstance();

            initView();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            monitorShieldService = null;
            bound = false;
        }
    };

    private void initView() {
        if (!AppData.getInstance(this).getFirstScanDone()) {
            showFirstScan();
        } else {
            if (monitorShieldService.getMenacesCacheSet().getItemCount() == 0) {
                showSafe();
            } else {
                showDanger();
            }

        }
    }

    private void showFirstScan() {
        tv_first_run.setVisibility(View.VISIBLE);
        noti_danger.setVisibility(View.INVISIBLE);
        notifi_safe.setVisibility(View.INVISIBLE);
    }

    private void showSafe() {
        tv_first_run.setVisibility(View.INVISIBLE);
        noti_danger.setVisibility(View.INVISIBLE);
        notifi_safe.setVisibility(View.VISIBLE);
        setBackgroungSafe();
    }

    private void showDanger() {
        tv_first_run.setVisibility(View.INVISIBLE);
        notifi_safe.setVisibility(View.INVISIBLE);
        noti_danger.setVisibility(View.VISIBLE);
        setBackgroungDanger();
        tv_found_problem.setText(monitorShieldService.getMenacesCacheSet().getItemCount() + " " + getResources().getString(R.string.problem_found));
    }

    @Override
    protected void onPause() {
        super.onPause();
        adView.pause();
    }

    @Override
    public void onStop() {
        super.onStop();
        // Unbind from the service
        if (bound && monitorShieldService != null) {
            unbindService(serviceConnection);
            bound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppEventsLogger.deactivateApp(this);
        adView.destroy();
    }

    private void loadAds() {
        adView = new AdView(this);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(ID.BANNER_AD);
        AdRequest adRequest = new AdRequest.Builder().build();
        ads_container.addView(adView);
        adView.loadAd(adRequest);
    }
}
