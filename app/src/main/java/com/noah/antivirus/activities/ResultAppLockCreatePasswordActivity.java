package com.noah.antivirus.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.adapter.QuestionSpinnerAdapter;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.util.TypeFaceUttils;
import com.takwolf.android.lock9.Lock9View;

import butterknife.BindView;

public class ResultAppLockCreatePasswordActivity extends BaseToolbarActivity {

    @BindView(R.id.la_password)
    View la_password;

    @BindView(R.id.la_password_again)
    View la_password_again;

    @BindView(R.id.la_question)
    View la_question;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.lock_view)
    Lock9View lock_view;

    @BindView(R.id.title_again)
    TextView title_again;

    @BindView(R.id.lock_view_again)
    Lock9View lock_view_again;

    @BindView(R.id.spinner_question)
    Spinner spinner_question;

    @BindView(R.id.title_1)
    TextView title_1;

    @BindView(R.id.title_2)
    TextView title_2;

    @BindView(R.id.done)
    TextView done;

    @BindView(R.id.edt_answer)
    EditText edt_answer;

    private SharedPreferences sharedPreferences;

    private String password;
    private String passwordAgain;


    private void customFont() {
        TypeFaceUttils.setNomal(this, title);
        TypeFaceUttils.setNomal(this, title_again);
        TypeFaceUttils.setNomal(this, title_1);
        TypeFaceUttils.setNomal(this, title_2);
        TypeFaceUttils.setNomal(this, done);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_app_lock_create_password;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customFont();
        sharedPreferences = getSharedPreferences(AppLockCreatePasswordActivity.SHARED_PREFERENCES_NAME, 0);

        initView();
    }

    private void initView() {
        lock_view.setCallBack(new Lock9View.CallBack() {
            @Override
            public void onFinish(String password) {
                ResultAppLockCreatePasswordActivity.this.password = password;
                if (ResultAppLockCreatePasswordActivity.this.password != null) {
                    la_password.setVisibility(View.GONE);
                    la_password_again.setVisibility(View.VISIBLE);
                }
            }
        });

        lock_view_again.setCallBack(new Lock9View.CallBack() {
            @Override
            public void onFinish(String password) {
                passwordAgain = password;
                if (ResultAppLockCreatePasswordActivity.this.password.equals(passwordAgain)) {
                    la_password_again.setVisibility(View.GONE);
                    la_question.setVisibility(View.VISIBLE);
                } else {
                    Snackbar snackbar = Snackbar.make(la_password_again, R.string.patterns_do_not_match, Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });

        QuestionSpinnerAdapter spinnerAdapter = new QuestionSpinnerAdapter(this, getResources().getStringArray(R.array.question_arrays));
        spinner_question.setAdapter(spinnerAdapter);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionDone();
            }
        });

        edt_answer.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    actionDone();
                }
                return false;
            }
        });
    }

    private void actionDone() {
        if (edt_answer.getText().toString().length() == 0) {
            Snackbar snackbar = Snackbar.make(la_password_again, R.string.answer_is_not_empty, Snackbar.LENGTH_SHORT);
            snackbar.show();
        } else {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(AppLockCreatePasswordActivity.KEY_PASSWORD, password);
            editor.putString(AppLockCreatePasswordActivity.KEY_QUESTION, spinner_question.getSelectedItem().toString());
            editor.putString(AppLockCreatePasswordActivity.KEY_ANSWER, edt_answer.getText().toString());
            editor.apply();

            finish();
        }
    }
}
