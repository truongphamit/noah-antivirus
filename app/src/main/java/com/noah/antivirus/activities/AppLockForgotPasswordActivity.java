package com.noah.antivirus.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.adapter.QuestionSpinnerAdapter;
import com.noah.antivirus.util.TypeFaceUttils;
import com.takwolf.android.lock9.Lock9View;

import butterknife.BindView;

public class AppLockForgotPasswordActivity extends BaseToolbarActivity {

    @BindView(R.id.la_password)
    View la_password;

    @BindView(R.id.la_password_again)
    View la_password_again;

    @BindView(R.id.la_question)
    View la_question;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.lock_view)
    Lock9View lock_view;

    @BindView(R.id.title_again)
    TextView title_again;

    @BindView(R.id.lock_view_again)
    Lock9View lock_view_again;

    @BindView(R.id.spinner_question)
    Spinner spinner_question;

    @BindView(R.id.title_1)
    TextView title_1;

    @BindView(R.id.title_2)
    TextView title_2;

    @BindView(R.id.done)
    TextView done;

    @BindView(R.id.edt_answer)
    EditText edt_answer;

    private SharedPreferences sharedPreferences;

    private String password;

    private void customFont() {
        TypeFaceUttils.setNomal(this, title);
        TypeFaceUttils.setNomal(this, title_again);
        TypeFaceUttils.setNomal(this, title_1);
        TypeFaceUttils.setNomal(this, title_2);
        TypeFaceUttils.setNomal(this, done);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_app_lock_forgot_password;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customFont();
        sharedPreferences = getSharedPreferences(AppLockCreatePasswordActivity.SHARED_PREFERENCES_NAME, 0);

        initView();
    }

    private void initView() {
        QuestionSpinnerAdapter spinnerAdapter = new QuestionSpinnerAdapter(this, getResources().getStringArray(R.array.question_arrays));
        spinner_question.setAdapter(spinnerAdapter);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionDone();
            }
        });

        edt_answer.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    actionDone();
                }
                return false;
            }
        });

        lock_view.setCallBack(new Lock9View.CallBack() {
            @Override
            public void onFinish(String password) {
                AppLockForgotPasswordActivity.this.password = password;
                if (AppLockForgotPasswordActivity.this.password != null) {
                    la_password.setVisibility(View.GONE);
                    la_password_again.setVisibility(View.VISIBLE);
                }
            }
        });

        lock_view_again.setCallBack(new Lock9View.CallBack() {
            @Override
            public void onFinish(String password) {
                if (AppLockForgotPasswordActivity.this.password.equals(password)) {
                    sharedPreferences.edit().putString(AppLockCreatePasswordActivity.KEY_PASSWORD, password).apply();
                    finish();
                } else {
                    Snackbar snackbar = Snackbar.make(la_password_again, R.string.patterns_do_not_match, Snackbar.LENGTH_SHORT);
                    snackbar.setAction(R.string.reset, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            la_password.setVisibility(View.VISIBLE);
                            la_password_again.setVisibility(View.GONE);
                        }
                    });
                    snackbar.show();
                }
            }
        });
    }

    private void actionDone() {
        if (edt_answer.getText().toString().length() == 0) {
            Snackbar snackbar = Snackbar.make(la_password_again, R.string.answer_is_not_empty, Snackbar.LENGTH_SHORT);
            snackbar.show();
        } else {
            boolean checkQuestion = sharedPreferences.getString(AppLockCreatePasswordActivity.KEY_QUESTION, "").equals(spinner_question.getSelectedItem().toString());
            boolean checkAnswer = sharedPreferences.getString(AppLockCreatePasswordActivity.KEY_ANSWER, "").equals(edt_answer.getText().toString());
            if (checkQuestion && checkAnswer) {
                la_question.setVisibility(View.GONE);
                la_password.setVisibility(View.VISIBLE);
            } else {
                Snackbar snackbar = Snackbar.make(la_password_again, R.string.question_or_answer_do_not_match, Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        }
    }
}
