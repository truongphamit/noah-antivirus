package com.noah.antivirus.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.util.TypeFaceUttils;
import com.takwolf.android.lock9.Lock9View;

import butterknife.BindView;

public class AppLockEditPasswordActivity extends BaseToolbarActivity {
    @BindView(R.id.la_old_password)
    View la_old_password;

    @BindView(R.id.la_password)
    View la_password;

    @BindView(R.id.la_password_again)
    View la_password_again;

    @BindView(R.id.title_old)
    TextView title_old;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.title_again)
    TextView title_again;

    @BindView(R.id.lock_view_old)
    Lock9View lock_view_old;

    @BindView(R.id.lock_view)
    Lock9View lock_view;

    @BindView(R.id.lock_view_again)
    Lock9View lock_view_again;

    private SharedPreferences sharedPreferences;
    private String password;

    private void customFont() {
        TypeFaceUttils.setNomal(this, title_old);
        TypeFaceUttils.setNomal(this, title);
        TypeFaceUttils.setNomal(this, title_again);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_app_lock_edit_password;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customFont();
        sharedPreferences = getSharedPreferences(AppLockCreatePasswordActivity.SHARED_PREFERENCES_NAME, 0);

        initView();
    }

    private void initView() {
        lock_view_old.setCallBack(new Lock9View.CallBack() {
            @Override
            public void onFinish(String password) {
                if (password.equals(sharedPreferences.getString(AppLockCreatePasswordActivity.KEY_PASSWORD, null))) {
                    la_old_password.setVisibility(View.GONE);
                    la_password.setVisibility(View.VISIBLE);
                } else {
                    Snackbar snackbar = Snackbar.make(la_old_password, R.string.patterns_do_not_match, Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });

        lock_view.setCallBack(new Lock9View.CallBack() {
            @Override
            public void onFinish(String password) {
                AppLockEditPasswordActivity.this.password = password;
                la_password.setVisibility(View.GONE);
                la_password_again.setVisibility(View.VISIBLE);
            }
        });

        lock_view_again.setCallBack(new Lock9View.CallBack() {
            @Override
            public void onFinish(String password) {
                if (AppLockEditPasswordActivity.this.password.equals(password)) {
                    sharedPreferences.edit().putString(AppLockCreatePasswordActivity.KEY_PASSWORD, password).apply();
                    finish();
                } else {
                    Snackbar snackbar = Snackbar.make(la_password_again, R.string.patterns_do_not_match, Snackbar.LENGTH_SHORT);
                    snackbar.setAction(R.string.reset, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            la_password.setVisibility(View.VISIBLE);
                            la_password_again.setVisibility(View.GONE);
                        }
                    });
                    snackbar.show();
                }
            }
        });
    }
}
