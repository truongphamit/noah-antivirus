package com.noah.antivirus.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.noah.antivirus.adapter.AppLockAdapter;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.model.AppsLocked;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.util.Utils;
import com.noah.antivirus.R;
import com.noah.antivirus.model.AppLock;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class AppLockHomeActivity extends BaseToolbarActivity {
    @BindView(R.id.rv_app_lock)
    RecyclerView rv_app_lock;

    @BindView(R.id.title)
    TextView title;

    private AppsLocked appsLocked;
    private List<AppLock> appLocks;

    private AppLockAdapter adapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_app_lock_home;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();

        new LoadAppTask().execute();
    }

    private void initView() {
        TypeFaceUttils.setNomal(this, title);
        rv_app_lock.setLayoutManager(new LinearLayoutManager(this));
        rv_app_lock.setHasFixedSize(true);
        appLocks = new ArrayList<>();
        adapter = new AppLockAdapter(this, appLocks);
        rv_app_lock.setAdapter(adapter);


        adapter.setOnItemClickListener(new AppLockAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ToggleButton toggleButton, int position, int sectionIndex, int itemIndex) {
                if (toggleButton.isChecked()) {
                    appLocks.get(position).setLock(false);
                    appsLocked.remove(appLocks.get(position));
                } else {
                    appLocks.get(position).setLock(true);
                    appsLocked.add(appLocks.get(position));
                }

                adapter.notifySectionItemChanged(sectionIndex, itemIndex);
            }
        });
    }

    private class LoadAppTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            appsLocked = new AppsLocked(AppLockHomeActivity.this);
            appLocks.addAll(getApps());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapter.notifyAllSectionsDataSetChanged();
        }
    }

    private List<AppLock> getApps() {
        List<AppLock> apps = new ArrayList<>();
        PackageManager packageManager = getPackageManager();
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> appList = packageManager.queryIntentActivities(mainIntent, 0);
        for (ResolveInfo ri : appList) {
            AppLock appLock = new AppLock(Utils.getAppNameFromPackage(this, ri.activityInfo.packageName), ri.activityInfo.packageName, false);
            if (appsLocked.isAppLocked(appLock.getPackageName())) appLock.setLock(true);

            apps.add(appLock);
        }
        return apps;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_lock_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, AppLockSettingsActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}
