package com.noah.antivirus.activities;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.noah.antivirus.R;
import com.noah.antivirus.model.UserWhiteList;
import com.noah.antivirus.adapter.IgnoredAppsAdapter;
import com.noah.antivirus.base.BaseToolbarActivity;
import com.noah.antivirus.iface.IProblem;
import com.noah.antivirus.model.AppProblem;
import com.noah.antivirus.model.SystemProblem;
import com.noah.antivirus.service.MonitorShieldService;
import com.noah.antivirus.util.TypeFaceUttils;
import com.noah.antivirus.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class IgnoredListActivity extends BaseToolbarActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_ignored_list;
    }

    private boolean bound;
    private MonitorShieldService monitorShieldService;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            bound = true;
            MonitorShieldService.MonitorShieldLocalBinder binder = (MonitorShieldService.MonitorShieldLocalBinder) service;
            monitorShieldService = binder.getServiceInstance();
            init();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bound = false;
            monitorShieldService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.colorPrimaryDark)));

        bindService(new Intent(this, MonitorShieldService.class), serviceConnection, BIND_AUTO_CREATE);
    }

    private void init() {
        RecyclerView rvIgnoredApps = (RecyclerView) findViewById(R.id.rv_ignored_apps);
        rvIgnoredApps.setLayoutManager(new LinearLayoutManager(this));

        UserWhiteList userWhiteList = monitorShieldService.getUserWhiteList();
        final List<IProblem> iProblems = new ArrayList<>();
        iProblems.addAll(userWhiteList.getSet());
        final IgnoredAppsAdapter adapter = new IgnoredAppsAdapter(this, iProblems, monitorShieldService);
        rvIgnoredApps.setAdapter(adapter);

        final TextView tvCountApps = (TextView) findViewById(R.id.tv_count_apps);
        TypeFaceUttils.setNomal(this, tvCountApps);
        tvCountApps.setText(userWhiteList.getItemCount() + " " + getResources().getString(R.string.apps_ignored));

        adapter.setOnItemClickListener(new IgnoredAppsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, final int position) {

                IProblem iProblem = iProblems.get(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(IgnoredListActivity.this, R.style.MyAlertDialogStyle);
                builder.setTitle(getString(R.string.warning))
                        .setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adapter.removeItem(position);
                                tvCountApps.setText((new UserWhiteList(IgnoredListActivity.this)).getItemCount() + " " + getResources().getString(R.string.apps_ignored));
                            }
                        }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                String dialogMessage;

                if (iProblem.getType() == IProblem.ProblemType.AppProblem) {
                    AppProblem appProblem = (AppProblem) iProblem;
                    dialogMessage = getString(R.string.remove_ignored_app_message) + " " + Utils.getAppNameFromPackage(IgnoredListActivity.this, appProblem.getPackageName());
                } else {
                    SystemProblem sp = ((SystemProblem) iProblem);
                    dialogMessage = sp.getWhiteListOnRemoveDescription(IgnoredListActivity.this);
                }
                builder.setMessage(dialogMessage);
                builder.show();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (bound && monitorShieldService != null) {
            unbindService(serviceConnection);
            bound = false;
        }
    }
}
