package com.noah.antivirus.util;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Created by truongpq on 8/3/16.
 */
public class TypeFaceUttils {
    private static final String nomal = "utm_avo.ttf";
    private static final String bold = "utm_avo_bold.ttf";
    private static final String italic = "utm_avo_italic.ttf";
    private static final String bold_italic = "utm_avo_bold_italic.ttf";
    private static final String menu = "arial.ttf";

    public static void setNomal(Context context, TextView textView) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), menu);
        textView.setTypeface(typeface);
    }

    public static void setNomal(Context context, RadioButton radioButton) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), menu);
        radioButton.setTypeface(typeface);
    }

    public static void setBold(Context context, TextView textView) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), bold);
        textView.setTypeface(typeface);
    }

    public static void setItalic(Context context, TextView textView) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), italic);
        textView.setTypeface(typeface);
    }

    public static void setBoldItalic(Context context, TextView textView) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), bold_italic);
        textView.setTypeface(typeface);
    }

    public static void setMenu(Context context, TextView textView) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), menu);
        textView.setTypeface(typeface);
    }
}
