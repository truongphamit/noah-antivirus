package com.noah.antivirus.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import com.noah.antivirus.activities.AppLockSettingsActivity;
import com.noah.antivirus.iface.ActivityStartingListener;
import com.noah.antivirus.model.AppsLocked;
import com.noah.antivirus.activities.AppLockCreatePasswordActivity;
import com.noah.antivirus.service.LockService;

import java.util.Hashtable;

/**
 * Created by truongpq on 8/18/16.
 */
public class ActivityStartingHandler implements ActivityStartingListener {
    private Context context;
    public String lastRunningPackage = "";
    private Hashtable<String, Runnable> tempAllowedPackages;
    private SharedPreferences sharedPreferences;

    public class PhoneUnlockedReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
                Log.e("LockService", "Phone unlocked");
            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                Log.e("LockService", "Phone locked");
                tempAllowedPackages.clear();
                sharedPreferences.edit().putBoolean(LockService.KEY_UNLOCKED, false).apply();
            }
        }
    }

    private BroadcastReceiver receiver;

    public ActivityStartingHandler(Context context) {
        this.context = context;

        //Screen Lock listener
        receiver = new PhoneUnlockedReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_USER_PRESENT);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        context.registerReceiver(receiver, filter);

        tempAllowedPackages = new Hashtable<>();
        sharedPreferences = context.getSharedPreferences(AppLockCreatePasswordActivity.SHARED_PREFERENCES_NAME, 0);

        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                lastRunningPackage = intent.getStringExtra("packageName");
                if (!sharedPreferences.getBoolean(AppLockCreatePasswordActivity.KEY_RELOCK_POLICY, false)) {
                    Runnable runnable = new RemoveFromTempRunnable(lastRunningPackage);
                    switch (sharedPreferences.getInt(AppLockCreatePasswordActivity.KEY_RELOCK_TIMEOUT, 1)) {
                        case 1:
                            tempAllowedPackages.put(lastRunningPackage, runnable);
                            break;
                        case 2:
                            new Handler().postDelayed(runnable, 1000 * 60 * 3);
                            tempAllowedPackages.put(lastRunningPackage, runnable);
                            break;
                        case 3:
                            new Handler().postDelayed(runnable, 1000 * 60 * 5);
                            tempAllowedPackages.put(lastRunningPackage, runnable);
                            break;
                        case 4:
                            break;
                    }
                }

            }
        }, new IntentFilter(LockService.ACTION_APPLICATION_PASSED));
    }

    @Override
    public void onActivityStarting(String packageName) {
        synchronized (this) {
            if (sharedPreferences.getBoolean(LockService.KEY_UNLOCKED, false)) return;

            if (packageName.equals(lastRunningPackage)) return;

            if (packageName.equals(context.getPackageName())) {
                lastRunningPackage = packageName;
                return;
            }

            if (tempAllowedPackages.containsKey(packageName)) return;

            AppsLocked appsLocked = new AppsLocked(context);

            if (sharedPreferences.getString(AppLockCreatePasswordActivity.KEY_PASSWORD, null) != null) {
                if (packageName.equals("com.android.packageinstaller") && !Utils.isServiceRunning(context, LockService.class)) {
                    blockApp(packageName);
                    return;
                }

                if (appsLocked.isAppLocked(packageName) && !Utils.isServiceRunning(context, LockService.class)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (Settings.canDrawOverlays(context)) {
                            blockApp(packageName);
                            return;
                        }
                    }

                    blockApp(packageName);
                    return;
                }
            }

            lastRunningPackage = packageName;
        }
    }

    private void blockApp(String packageName) {
        Intent intent = new Intent(context, LockService.class);
        intent.putExtra("packageName", packageName);
        context.startService(intent);
    }

    private class RemoveFromTempRunnable implements Runnable {
        private String mPackageName;

        public RemoveFromTempRunnable(String pname) {
            mPackageName = pname;
        }

        public void run() {
            tempAllowedPackages.remove(mPackageName);
        }
    }

}
