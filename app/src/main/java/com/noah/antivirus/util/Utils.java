package com.noah.antivirus.util;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageDataObserver;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.RemoteException;
import android.os.StatFs;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.noah.antivirus.activities.DoneBoostActivity;
import com.noah.antivirus.model.Application;
import com.noah.antivirus.model.AppsLocked;
import com.noah.antivirus.service.MyAccessibilityService;
import com.noah.antivirus.R;
import com.noah.antivirus.iface.IProblem;
import com.noah.antivirus.model.AppLock;
import com.noah.antivirus.model.BrowserHistory;
import com.noah.antivirus.model.JunkOfApplication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.support.design.R.styleable.AlertDialog;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by hexdump on 16/02/16.
 */
public class Utils {

    public static String SHARE_PREFERENCE_APP_CHOSEN_ADVICE = "app_advide";

    public static int numOfPackages;
    public static IProblem selectedProblem;
    public static List<JunkOfApplication> junkOfApplications = new ArrayList<>();

    //Notifications
    public static void notificatePush(Context context, int notificationId, int iconDrawableId,
                                      String tickerText, String contentTitle, String contentText, Intent intent) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_small_notificiation)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                        iconDrawableId))
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setTicker(tickerText);

        // Because clicking the notification opens a new ("special") activity, there's no need to create an artificial back stack.
        if (intent != null) {
            PendingIntent resultPendingIntent = PendingIntent.getActivity(context, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
        }
        mBuilder.setAutoCancel(true);
        mBuilder.setOnlyAlertOnce(true);

        // Gets an instance of the NotificationManager service
        NotificationManager notifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Builds the notification and issues it.
        notifyMgr.notify(notificationId, mBuilder.build());
    }

    public static void notificateAppLock(Context context, int notificationId, int iconDrawableId, String tickerText, String contentTitle, String contentText, Intent intent) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_small_notificiation)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), iconDrawableId))
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setTicker(tickerText);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        mBuilder.setAutoCancel(true);
        mBuilder.setOnlyAlertOnce(true);

        // Gets an instance of the NotificationManager service
        NotificationManager notifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Builds the notification and issues it.
        notifyMgr.notify(notificationId, mBuilder.build());
    }

    public static void notificatePermanentPush(Context context, int notificationId, int iconDrawableId,
                                               String tickerText, String contentTitle, String contentText, Intent intent) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(iconDrawableId)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setTicker(tickerText)
                .setOngoing(true);

        // Because clicking the notification opens a new ("special") activity, there's no need to create an artificial back stack.
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setAutoCancel(true);
        mBuilder.setOnlyAlertOnce(true);

        // Gets an instance of the NotificationManager service
        NotificationManager notifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Builds the notification and issues it.
        notifyMgr.notify(notificationId, mBuilder.build());
    }

    //Media
    public static boolean isSDAvailable() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    public static String getSDPath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath();
    }

    public static String getInternalDataPath(Context c) {
        String path = c.getFilesDir().getPath();
        if (path.length() == 0) {
            path = "/data/data/" + c.getPackageName() + "/files";
            File filesDir = new File(path);
            filesDir.mkdirs();
        }
        return path;
//        return new ContextWrapper(c).getFilesDir().getPath();
    }

    public static boolean existsFile(String filePath) {
        File f = new File(filePath);
        if (f.exists() && !f.isDirectory())
            return true;
        else
            return false;
    }

    public static boolean existsFolder(String folderPath) {
        File f = new File(folderPath);
        if (f.exists() && f.isDirectory())
            return true;
        else
            return false;
    }

    public static boolean existsInternalStorageFile(Context ctx, String internalRelativePath) {
        String fullPath = Utils.getInternalDataPath(ctx) + internalRelativePath;
        return existsFile(fullPath);
    }

    public static boolean existsSDFile(Context ctx, String sdRelativePath) {
        String fullPath = Utils.getSDPath() + sdRelativePath;
        return existsFile(fullPath);
    }


    public static boolean deleteFile(String filePath) {
        File f = new File(filePath);
        if (f.exists() && !f.isDirectory())
            return f.delete();
        else {
            //Log.i("IO", "The file you want to delete does not exist or is a folder");
            return false;
        }
    }

    public static StatFs getSDData() {
        StatFs statFS = new StatFs(getSDPath());

        return statFS;
    }

    public static File createTempFile(Context context, String prefix, String extension) {
        File outputDir = context.getCacheDir();
        File outputFile = null;
        try {
            outputFile = File.createTempFile(prefix, extension, outputDir);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return outputFile;
    }


    public static void copyAssetFileToCacheFile(Context context, String assetFile, File cacheFile) {
        try {
            InputStream inStream = context.getAssets().open(assetFile);
            BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
            _copyAssetFile(br, cacheFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static boolean copyAssetFileToFileSystem(Context context, String assetFile, String dstFile) {
        File toFile = new File(dstFile);
        return copyAssetFileToFileSystem(context, assetFile, toFile);

    }

    public static boolean copyAssetFileToFileSystem(Context context, String assetFile, File dstFile) {
        boolean success = true;

        try {
            InputStream inStream = context.getAssets().open(assetFile);
            BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
            _copyAssetFile(br, dstFile);
        } catch (IOException e) {
            e.printStackTrace();
            success = false;
        }

        return success;

    }


    private static void _copyAssetFile(BufferedReader br, File toFile) throws IOException {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(toFile));

            int in;
            while ((in = br.read()) != -1) {
                bw.write(in);
            }
        } finally {
            if (bw != null) {
                bw.close();
            }
            br.close();
        }
    }

    public static String loadJSONFromAsset(Context context, String file) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String loadJSONFromFile(Context context, String filePath) {
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return text.toString();
    }

    static public String getFileExtension(String fileName) {
        try {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }

    static public String loadTextFile(String filePath) throws IOException {
        //Get the text file
        File file = new File(filePath);

        //Read text from file
        StringBuilder text = new StringBuilder();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } finally {
            br.close();
        }

        return text.toString();
    }

    static public void writeTextFile(String filePath, String text) throws IOException {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(filePath));
            bw.write(text);
        } finally {
            if (bw != null) {
                bw.close();
            }
        }
    }


    static public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    //Activitiets
    public static String getPackageName(final Context context) {
        return context.getPackageName();
    }

    /*public static boolean isPackageInstalled(final Context context, final String packageName)
    {
        List<ApplicationInfo> packages;
        PackageManager pm;
        pm = context.getPackageManager();
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages)
        {
            if (packageInfo.packageName.equals(packageName))
                return true;
        }

        return false;
    }*/

    public static boolean isPackageInstalled(Context context, String targetPackage) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(targetPackage, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    public static List<PackageInfo> getSystemApps(final Context context, List<PackageInfo> appsToFilter) {

        List<PackageInfo> filteredPackgeInfo = new ArrayList<PackageInfo>();

        PackageInfo packInfo = null;

        int mask = ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP;

        for (int i = 0; i < appsToFilter.size(); i++) {
            packInfo = appsToFilter.get(i);
            if ((packInfo.applicationInfo.flags & mask) != 0) {
                filteredPackgeInfo.add(packInfo);
            }
        }

        return filteredPackgeInfo;
    }

    public static List<PackageInfo> getNonSystemApps(final Context context, List<PackageInfo> appsToFilter) {

        List<PackageInfo> filteredPackgeInfo = new ArrayList<PackageInfo>();

        PackageInfo packInfo = null;

        int mask = ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP;

        for (int i = 0; i < appsToFilter.size(); i++) {
            packInfo = appsToFilter.get(i);
            if ((packInfo.applicationInfo.flags & mask) == 0) {
                filteredPackgeInfo.add(packInfo);
            }
        }

        return filteredPackgeInfo;
    }

    public static void logPackageNames(List<PackageInfo> packages) {
        PackageInfo pi = null;
        for (int i = 0; i < packages.size(); ++i) {
            pi = packages.get(i);
            Log.d("Package", pi.packageName);
        }
    }

    public static List<PackageInfo> getApps(final Context context, int packageManagerPermissions) {
        List<PackageInfo> packageInfos = null;
        try {
            packageInfos = context.getPackageManager().getInstalledPackages(packageManagerPermissions);
        } catch (Exception e) {
            packageInfos = getInstalledPackages(context, packageManagerPermissions);
        }
        return packageInfos;
//        return context.getPackageManager().getInstalledPackages(packageManagerPermissions);
    }


    public static List<PackageInfo> getInstalledPackages(Context context, int flags) {
        final PackageManager pm = context.getPackageManager();
        try {
            return pm.getInstalledPackages(flags);
        } catch (Exception ignored) {
            //we don't care why it didn't succeed. We'll do it using an alternative way instead
        }
        // use fallback:
        Process process;
        List<PackageInfo> result = new ArrayList<>();
        BufferedReader bufferedReader = null;
        try {
            process = Runtime.getRuntime().exec("pm list packages");
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                final String packageName = line.substring(line.indexOf(':') + 1);
                final PackageInfo packageInfo = pm.getPackageInfo(packageName, flags);
                result.add(packageInfo);
            }
            process.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null)
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return result;
    }

    public static String getAppNameFromPackage(final Context context, final String packageName) {
        PackageManager packageManager = context.getApplicationContext().getPackageManager();
        String appName = "";
        try {
            appName = (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA));
        } catch (PackageManager.NameNotFoundException ex) {
            appName = "Unkown app";
        }

        return appName;
    }


    public static Drawable getIconFromPackage(String packageName, Context context) {

        final PackageManager pm = context.getPackageManager();
        Drawable icon = null;

        try {
            icon = pm.getApplicationIcon(packageName);

        } catch (PackageManager.NameNotFoundException e) {

        }

        return icon;

    }


    public static boolean checkIfAppWasInstalledThroughGooglePlay(Context context, String packageName) {

        final PackageManager packageManager = context.getPackageManager();

        try {
            final ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageName, 0);
            if ("com.android.vending".equals(packageManager.getInstallerPackageName(applicationInfo.packageName))) {
                // App was installed by Play Store
                //Sacar error similar a este: Title: apk desconocida MSG: La instalacion de apk desde fuentes desconocidas puede ser peligroso

                return true;
            }
        } catch (final PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        return false;

    }


    public static boolean checkIfUSBDebugIsEnabled(Context context) {

        if (Settings.Secure.getInt(context.getContentResolver(), Settings.Global.ADB_ENABLED, 0) == 1) {
            // debugging enabled
            //Sacar error similar a este: Title: Depuracion USB MSG: Cuando la depuracion de USB esta activada, los intrusos pueden acceder a sus datos privados.
            return true;
        } else {
            //debugging does not enabled
            return false;
        }
    }


    public static ActivityInfo[] getActivitiesInPackage(Context context, String packageName, int packageManagerPermissions) throws PackageManager.NameNotFoundException {
        PackageInfo pi = getPackageInfo(context, packageName, packageManagerPermissions);
        return pi.activities;
    }

    public static PackageInfo getPackageInfo(Context context, String packageName, int packageManagerPermissions) throws PackageManager.NameNotFoundException {
        return context.getPackageManager().getPackageInfo(packageName, packageManagerPermissions);
    }

    public static boolean packageInfoHasPermission(PackageInfo packageInfo, String permissionName) {
        if (packageInfo.requestedPermissions == null)
            return false;

        for (String permInfo : packageInfo.requestedPermissions) {
            if (permInfo.equals(permissionName))
                return true;
        }

        return false;
    }

    public static boolean checkIfUnknownAppIsEnabled(Context context) {

        if (Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.INSTALL_NON_MARKET_APPS, 0) == 1) {

            return true;
        } else {

            return false;
        }

    }

    public static void openSecuritySettings(Context context) {
        context.startActivity(new Intent(Settings.ACTION_SECURITY_SETTINGS));
    }

    public static void openDeveloperSettings(Context context) {

        context.startActivity(new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS));
    }

    //Example fillParams("My name is #1 and I have #2 years", "#", John, 12)
    public static String fillParams(String data, String paramStr, String... args) {
        int total = 0;

        for (int i = 0; i < args.length; i++)
            data = data.replace(paramStr + (i + 1), args[i]);

        return data;
    }

    public static String capitalize(String source) {
        return source.substring(0, 1).toUpperCase() + source.substring(1);
    }

    public static String hexStrToStr(String hex) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < hex.length(); i += 2) {
            String str = hex.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
        }

        return output.toString();
    }

    public static long getFreeRAM(Context context) {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);

        return mi.availMem;
    }

    public static long getTotalRAM(Context context) {
        if (Build.VERSION.SDK_INT >= 16) {
            ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            activityManager.getMemoryInfo(mi);
            return mi.totalMem;
        } else {
            RandomAccessFile reader = null;
            String load = null;
            long total = 0;
            try {
                reader = new RandomAccessFile("/proc/meminfo", "r");
                load = reader.readLine().replaceAll("\\D+", "");
                total = Integer.parseInt(load) / 1024;
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            //total is MB
            return total * 1024;
        }
    }

    //Encode
    public static String mixUp(String str, int interleaveRange) {

        StringBuffer sb = new StringBuffer(str);

        for (int i = 0; i < str.length() - interleaveRange; ++i) {
            char c = sb.charAt(i);
            sb.setCharAt(i, sb.charAt(i + interleaveRange));
            sb.setCharAt(i + interleaveRange, c);
        }

        return sb.toString();
    }

    //Decode
    public static String unMixUp(String str, int interleaveRange) {

        StringBuffer sb = new StringBuffer(str);

        for (int i = str.length() - 1; i >= interleaveRange; --i) {
            char c = sb.charAt(i);
            sb.setCharAt(i, sb.charAt(i - interleaveRange));
            sb.setCharAt(i - interleaveRange, c);
        }

        return sb.toString();
    }

    public static String padRight(String s, int n, char paddingChar) {
        return String.format("%1$-" + n + "s", s).replace(' ', paddingChar);
    }

    public static String padLeft(String s, int n, char paddingChar) {
        return String.format("%1$" + n + "s", s).replace(' ', paddingChar);
    }

    public static void convertFileSizeToString(long size, String[] outputParts) {
        if (outputParts.length != 2)
            throw new IllegalArgumentException("output parts must be an array of length 2");

        final String[] units = new String[]{"b", "Kb", "Mb", "Gb", "Tb", "Pb"};

        if (size <= 0) {
            outputParts[0] = "0";
            outputParts[1] = "Kb";
        } else {
            int digitGroups = (int) (Math.log10(size) / Math.log10(1024));

            if (digitGroups > units.length - 1) {
                outputParts[0] = "Too big";
                outputParts[1] = "";
            } else {
                outputParts[0] = new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups));
                outputParts[1] = units[digitGroups];
            }
        }
    }

    public static String convertFileSizeToString(long size) {
        final String[] units = new String[]{"b", "Kb", "Mb", "Gb", "Tb", "Pb"};

        if (size <= 0) {
            return "0Kb";
        } else {
            int digitGroups = (int) (Math.log10(size) / Math.log10(1024));

            if (digitGroups > units.length - 1) {
                return "Too big";
            } else {
                return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + units[digitGroups];
            }
        }
    }

    static public boolean stringMatchesMask(String packageName, String mask) {
        boolean wildcard = false;

        if (mask.charAt(mask.length() - 1) == '*') {
            wildcard = true;
            mask = mask.substring(0, mask.length() - 2);
        } else
            wildcard = false;

        if (wildcard == true) {
            if (packageName.startsWith(mask))
                return true;
            else
                return false;
        } else {
            if (packageName.equals(mask))
                return true;
            else
                return false;
        }

    }

    //Views
    @SuppressLint("NewApi")
    static public void setViewBackgroundDrawable(View view, Drawable drawable) {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(drawable);
        } else {
            view.setBackground(drawable);
        }
    }

    //Service
    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);

        for (ActivityManager.RunningServiceInfo runningServiceInfo : services) {
            if (runningServiceInfo.service.getClassName().equals(serviceClass.getName())) {
                return true;
            }
        }
        return false;
    }

    public static void openMarketURL(Context context, String marketUrl, String webUrl) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(marketUrl)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(webUrl)));
        }
    }

    @SuppressWarnings("unchecked")
    static public <T> T deserializeFromFile(String fileName) throws FileNotFoundException, IOException {
        T data = null;
        try {
            FileInputStream fis = new FileInputStream(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            data = (T) ois.readObject();
            ois.close();
            fis.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return data;
    }

    public static <T> T deserializeFromDataFolder(Context ctx, String rootRelativePath) {
        T obj = null;

        try {

            //Internal
            String path = Utils.getInternalDataPath(ctx) + File.separatorChar + rootRelativePath;
            if (Utils.existsFile(path)) {
                obj = Utils.deserializeFromFile(path);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return obj;
    }

    static public void serializeToFile(String fileName, Serializable obj) throws IOException {
        File file = new File(fileName);
        String fileParentFolder = file.getParent();
        File parentPath = new File(fileParentFolder);

        if (fileParentFolder != null) {
            if (!Utils.existsFolder(fileParentFolder))
                parentPath.mkdirs();
        }

        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

		/*try
        {*/
        fos = new FileOutputStream(fileName);
        oos = new ObjectOutputStream(fos);
        oos.writeObject(obj);
        oos.close();
        fos.close();
        /*}
        catch(Exception e)
		{
			e.printStackTrace();
		}*/
    }

    public static void serializeToDataFolder(Context ctx, Serializable obj, String rootRelativePath) throws IOException {

        String internalPath = Utils.getInternalDataPath(ctx);
        String finalPath = internalPath + File.separatorChar + rootRelativePath;
        serializeToFile(finalPath, obj);
    }


    public static boolean externalMemoryAvailable() {
        return android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED);
    }

    public static long getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return availableBlocks * blockSize;
    }

    public static long getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return totalBlocks * blockSize;
    }

    public static long getAvailableExternalMemorySize() {
        if (externalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long availableBlocks = stat.getAvailableBlocks();
            return availableBlocks * blockSize;
        } else {
            return Constants.ERROR;
        }
    }

    public static long getTotalExternalMemorySize() {
        if (externalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long totalBlocks = stat.getBlockCount();
            return totalBlocks * blockSize;
        } else {
            return Constants.ERROR;
        }
    }

    public static String formatSize(long size) {
        String suffix = null;

//        if (size >= 1024) {
//            suffix = "KB";
//            size /= 1024;
//            if (size >= 1024) {
//                suffix = "MB";
//                size /= 1024;
//            }
//        }
        suffix = "GB";
        double gbSize = (size / (1024.0 * 1024 * 1024));
        gbSize = Math.round(gbSize * 100) / 100.0d;

        //StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

//        int commaOffset = resultBuffer.length() - 3;
//        while (commaOffset > 0) {
//            resultBuffer.insert(commaOffset, '.');
//            commaOffset -= 3;
//        }

        //if (suffix != null) resultBuffer.append(suffix);
//        return resultBuffer.toString();
        return gbSize + suffix;
    }

    public long getAvalableMemory(Context c) {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) c.getSystemService(c.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.availMem / 1048576L;

        //Percentage can be calculated for API 16+
        //long percentAvail = mi.availMem / mi.totalMem;
        return availableMegs;
    }

    public static void showAlertDialog(Context c, String message, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(c, R.style.MyAlertDialogStyle);
        alertDialogBuilder.setTitle(message);
        alertDialogBuilder.setPositiveButton("OK", listener);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public static void showConfirmDialog(Context c, String title, DialogInterface.OnClickListener positiveListener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(c, R.style.MyAlertDialogStyle);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setPositiveButton(c.getString(R.string.ok), positiveListener);
        alertDialogBuilder.setNegativeButton(c.getString(R.string.cancel), null);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public static long getMemoryOfService(Context context, int pid) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        manager.getMemoryInfo(memoryInfo);
        return manager.getProcessMemoryInfo(new int[]{pid})[0].getTotalPss();
    }

    public static List<Application> getRunningApplications(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        SharedPreferences sharedPreferences = null;
        try {
            sharedPreferences = getApplicationContext().getSharedPreferences(SHARE_PREFERENCE_APP_CHOSEN_ADVICE, Context.MODE_PRIVATE);
        } catch (Exception ignored) {
        }

        List<ActivityManager.RunningServiceInfo> runningServiceInfos = manager.getRunningServices(Integer.MAX_VALUE);
        List<Application> applications = new ArrayList<>();
        for (ActivityManager.RunningServiceInfo serviceInfo : runningServiceInfos) {
            String packageName = serviceInfo.service.getPackageName();
            //Check System Process
            try {
                if ((context.getPackageManager().getApplicationInfo(packageName, 0).flags & ApplicationInfo.FLAG_SYSTEM) == 1)
                    continue;
            } catch (PackageManager.NameNotFoundException e) {
                //e.printStackTrace();
            }

            // Check process of this App
            if (serviceInfo.process.equals(context.getPackageName())) {
                continue;
            }

            try {
                if (packageName.equals(getApplicationContext().getPackageName())) {
                    continue;
                }
            } catch (Exception ignored) {
            }

            Application application = new Application(serviceInfo.pid, packageName);

            application.setChoose(true);

            if (sharedPreferences != null)
                if (sharedPreferences.contains(packageName)) {
                    if (sharedPreferences.getInt(packageName, 0) >= 3) {
                        Log.d("get apps", "set app advice " + packageName);
                        application.setChoose(false);
                    }
                }

            if (packageName.contains("facebook")) {
                application.setChoose(false);
            }

            if (packageName.contains("labankey")) {
                application.setChoose(false);
            }

            if (!applications.contains(application)) {
                application.setName(Utils.getAppNameFromPackage(context, packageName));
                application.setIcon(Utils.getIconFromPackage(packageName, context));
//                if (application.getIcon() == null) continue;
                application.setSize(Utils.getMemoryOfService(context, serviceInfo.pid));
                applications.add(application);
            } else {
                int index = applications.indexOf(application);
                Application app = applications.get(index);
                app.setSize(app.getSize() + Utils.getMemoryOfService(context, serviceInfo.pid));
            }
        }

        return applications;
    }

    public static int getTotalMemoryBoost(Context context) {
        List<Application> applications = getRunningApplications(context);
        int total = 0;
        for (Application application : applications) {
            total += application.getSize() / 1024;
        }
        return total;
    }

    public static void killBackgroundProcesses(Context context, String packageName) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        manager.killBackgroundProcesses(packageName);
    }

    public static void getCacheSize(final Context context) {
        junkOfApplications.clear();
        List<PackageInfo> packList = context.getPackageManager().getInstalledPackages(0);
        for (final PackageInfo packageInfo : packList) {
            try {
                Method getPackageSizeInfo = context.getPackageManager().getClass().getMethod(
                        "getPackageSizeInfo", String.class,
                        IPackageStatsObserver.class);
                try {
                    getPackageSizeInfo.invoke(context.getPackageManager(), packageInfo.packageName, new IPackageStatsObserver.Stub() {

                        @Override
                        public void onGetStatsCompleted(PackageStats pStats, boolean succeeded) throws RemoteException {
//                            cacheSize += pStats.cacheSize;
                            if (pStats.cacheSize != 0) {
                                if (!context.getPackageName().equals(packageInfo.packageName) && pStats.cacheSize >= 100 * 1024) {
                                    JunkOfApplication junkOfApplication = new JunkOfApplication(packageInfo.packageName, pStats.cacheSize);
                                    if (!junkOfApplications.contains(junkOfApplication))
                                        junkOfApplications.add(junkOfApplication);
                                }
                            }
                        }
                    });
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
    }

    private static final long CACHE_APP = Long.MAX_VALUE;

    public static void clearCache(Context context) {
        junkOfApplications.clear();
        CachePackageDataObserver mClearCacheObserver = new CachePackageDataObserver();
        PackageManager mPM = context.getPackageManager();
        @SuppressWarnings("rawtypes")
        final Class[] classes = {Long.TYPE, IPackageDataObserver.class};
        Long localLong = CACHE_APP;
        try {
            Method localMethod = mPM.getClass().getMethod("freeStorageAndNotify", classes);
            try {
                localMethod.invoke(mPM, localLong, mClearCacheObserver);
            } catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (NoSuchMethodException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    public static boolean isRecommendAppLock(String packageName) {
        String[] recommendApps = {"com.android.settings", "com.android.email", "com.android.chrome", "com.google.android.youtube", "com.facebook.katana",
                "com.facebook.orca", "com.google.android.gm", "com.instagram.android", "com.twitter.android", "com.snapchat.android", "com.whatsapp", "com.zing.zalo",
                "com.facebook.lite", "com.viber.voip", "com.skype.raider", "com.dropbox.android", "com.google.android.apps.docs", "com.google.android.apps.photos"};

        for (String recommendApp : recommendApps) {
            if (recommendApp.equals(packageName)) return true;
        }
        return false;
    }

    public static List<AppLock> getAppLock(Context context) {
        AppsLocked appsLocked = new AppsLocked(context);
        List<AppLock> apps = new ArrayList<>();
        PackageManager packageManager = context.getPackageManager();
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> appList = packageManager.queryIntentActivities(mainIntent, 0);
        for (ResolveInfo ri : appList) {
            if (!appsLocked.isAppLocked(ri.activityInfo.packageName)) {
                AppLock appLock = new AppLock(Utils.getAppNameFromPackage(context, ri.activityInfo.packageName), ri.activityInfo.packageName, false);
                if (isRecommendAppLock(appLock.getPackageName())) appLock.setRecommend(true);
                apps.add(appLock);
            }

        }
        return apps;
    }

    public static String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static boolean isUsageAccessEnabled(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            int mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, applicationInfo.uid, applicationInfo.packageName);
            return (mode == AppOpsManager.MODE_ALLOWED);

        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void openUsageAccessSetings(Context context) {
        if (!isUsageAccessEnabled(context)) {
            Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    public static Intent AppDetailsIntent(String packageName) {
        Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.setData(Uri.parse("package:" + packageName));
        return intent;
    }

    // To check if service is enabled
    public static boolean isAccessibilitySettingsOn(Context mContext) {
        int accessibilityEnabled = 0;
        final String appName = mContext.getPackageName();
        final String serviceName = MyAccessibilityService.class.getSimpleName();

        try {
            accessibilityEnabled = Settings.Secure.getInt(
                    mContext.getApplicationContext().getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
        } catch (Settings.SettingNotFoundException e) {
        }
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            String settingValue = Settings.Secure.getString(
                    mContext.getApplicationContext().getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext()) {
                    String accessibilityService = mStringColonSplitter.next();

                    if (accessibilityService.contains(appName) && accessibilityService.contains(serviceName)) {
                        return true;
                    }
                }
            }
        } else {
        }

        return false;
    }

    public static Intent getHomeIntent() {
        Intent startHomescreen = new Intent(Intent.ACTION_MAIN);
        startHomescreen.addCategory(Intent.CATEGORY_HOME);
        startHomescreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return startHomescreen;
    }

    public static void hideKeyBoard(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    public static long getCurrentTime() {
        return System.currentTimeMillis();
    }

    public static void rateUs(Activity activity) {
        Uri uri = Uri.parse("market://details?id=" + activity.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            SharedPreferences sharedPreferences = activity.getSharedPreferences(DoneBoostActivity.sharePreference_key_boosting, activity.MODE_PRIVATE);
            sharedPreferences.edit().putInt(DoneBoostActivity.sharePreference_key_rate_us, 3).commit();

            activity.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + activity.getPackageName())));
        }
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * return the unit of file size
     */
    public static String computeFileSizeUnit(long size) {
        String unit = null;

        float f = (float) size;

        if (f >= 1024.0F) {
            unit = "KB";
            f = (float) (f / 1024.0D);
            if (f >= 1024.0F) {
                unit = "MB";
                f = (float) (f / 1024.0D);
                if (f >= 1024.0F) {
                    unit = "GB";
                }
            }
        }

        if (unit == null) return "B";
        return unit;
    }

    /**
     * return the string size round with perfect unit (only return number)
     */
    public static String roundFileSize(long size) {
        String unit = null;

        float f = (float) size;

        if (f >= 1024.0F) {
            unit = "KB";
            f = (float) (f / 1024.0D);
            if (f >= 1024.0F) {
                unit = "MB";
                f = (float) (f / 1024.0D);
                if (f >= 1024.0F) {
                    unit = "GB";
                    f = (float) (f / 1024.0D);
                }
            }
        }

        /**
         * round number
         */
        if (unit == null) return size + "";
        if (unit.equals("KB") || unit.equals("MB")) {
            f = (float) Math.ceil(f);
        }
        if (unit.equals("GB")) {
            f *= 100;
            f = (float) Math.ceil(f);
            f /= 100;
        }

        return (int) f + "";
    }


}
